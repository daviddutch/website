module.exports = ({ env }) => ({
  "duplicate-button": true,
  "icon-picker": {
    enabled: true,
    resolve: "./src/plugins/icon-picker",
  },
  "vercel-deploy": {
    enabled: true,
    config: {
      deployHook: process.env.VERCEL_DEPLOY_PLUGIN_HOOK,
      apiToken: process.env.VERCEL_DEPLOY_PLUGIN_API_TOKEN,
      roles: ["strapi-super-admin"],
    },
  },
  // deepl: {
  //   enabled: true,
  //   config: {
  //     // your DeepL API key
  //     apiKey: "key",
  //     // whether to use the free or paid api, default true
  //     freeApi: true,
  //     // Which field types are translated (default string, text, richtext, components and dynamiczones)
  //     translatedFieldTypes: [
  //       "string",
  //       "text",
  //       "richtext",
  //       "component",
  //       "dynamiczone",
  //     ],
  //     // If relations should be translated (default true)
  //     translateRelations: true,
  //     // You can define a custom glossary to be used here (see https://www.deepl.com/docs-api/managing-glossaries/)
  //     glossaryId: "customGlossary",
  //   },
  // },
  upload: {
    config: {
      provider: "aws-s3",
      providerOptions: {
        accessKeyId: env("AWS_ACCESS_KEY_ID"),
        secretAccessKey: env("AWS_ACCESS_SECRET"),
        endpoint: env("AWS_ENDPOINT"),
        region: env("AWS_REGION"),
        params: {
          Bucket: env("AWS_BUCKET"),
        },
      },
      actionOptions: {
        upload: {},
        uploadStream: {},
        delete: {},
      },
    },
  },
});
