module.exports = ({ env }) => ({
  auth: {
    secret: env('ADMIN_JWT_SECRET', '70fec9a672e47dc3b9462af21090e2b5'),
  },
});
