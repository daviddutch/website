"use strict";

const fs = require("fs");
const path = require("path");
const mime = require("mime-types");
const set = require("lodash.set");
const tags = require("../data/tags.json");
const categories = require("../data/categories.json");
const { data: dataEn } = require("../data/postsEn.json");
const { data: dataFr } = require("../data/postsFrDe.json");

const BLOG_FOLDER_ID = 35;

const download = require("image-downloader");

const downloadImage = async (url, filepath) => {
  if (fs.existsSync(filepath)) {
    return filepath;
  }
  return download.image({
    url,
    dest: filepath,
  });
};

function getFileSizeInBytes(filePath) {
  const stats = fs.statSync(filePath);
  return stats["size"];
}

async function uploadImage(file) {
  const [fileName] = file.name.split(".");

  const files = await strapi.plugins["upload"].services.upload.findMany({
    filters: {
      name: fileName,
    },
  });
  if (files && files.length > 0) {
    return files[0];
  }
  // Upload each individual file
  let uploaded = await strapi
    .plugin("upload")
    .service("upload")
    .upload({
      files: file,
      data: {
        fileInfo: {
          alternativeText: fileName,
          caption: fileName,
          name: fileName,
          folder: BLOG_FOLDER_ID,
        },
      },
    });
  return uploaded[0];
}

// Create an entry and attach files if there are any
async function createEntry({ model, entry, files }) {
  try {
    if (files) {
      for (const [key, file] of Object.entries(files)) {
        // Get file name without the extension
        const uploadedFile = await uploadImage(file);

        // Attach each file to its entry
        set(entry, key, uploadedFile.id);
      }
    }

    // Actually create the entry in Strapi
    return await strapi.entityService.create(`api::${model}.${model}`, {
      data: entry,
    });
  } catch (e) {
    console.log("model", e);
  }
}

function getFileData(filename) {
  const filePath = path.join(__dirname, "..", ".tmp/images", filename);

  // Parse the file metadata
  const size = getFileSizeInBytes(filePath);
  const ext = filename.split(".").pop();
  const mimeType = mime.lookup(ext);

  return {
    path: filePath,
    name: filename,
    size,
    type: mimeType,
  };
}

function getFilename(link) {
  if (link.startsWith("https://www.wedo.swiss/wp-content/uploads")) {
    link = link.replace("https://www.wedo.swiss/wp-content/uploads/", "");
  } else if (link.startsWith("https://wedo.swiss/wp-content/uploads")) {
    link = link.replace("https://wedo.swiss/wp-content/uploads/", "");
  } else {
    const url = new URL(link);
    link = url.pathname.substring(1);
  }

  return link.replaceAll("/", "-").toLocaleLowerCase();
}

async function getTagBySlug(slug, locale) {
  let newVar = await strapi.entityService.findMany("api::tag.tag", {
    filters: { slug: slug },
    locale: locale,
  });
  return newVar[0];
}

async function importTags() {
  const imported = [];
  console.log("Processing tags import");
  await Promise.all(
    tags.map(async (item) => {
      let exist = await getTagBySlug(item.en.slug, "en");
      if (exist) {
        imported.push(exist);
        exist = await getTagBySlug(item.fr.slug, "fr");
        imported.push(exist);
        exist = await getTagBySlug(item.de.slug, "de");
        imported.push(exist);
        return;
      }
      const fr = await createEntry({ model: "tag", entry: item.fr });
      const de = await createEntry({ model: "tag", entry: item.de });
      const en = await createEntry({
        model: "tag",
        entry: {
          ...item.en,
          localizations: [fr.id, de.id],
        },
      });
      imported.push(en);
      imported.push(fr);
      imported.push(de);
    })
  );
  fs.writeFileSync("./data/importedTags.json", JSON.stringify(imported));
}

async function getCategoryBySlug(slug, locale) {
  let newVar = await strapi.entityService.findMany("api::category.category", {
    filters: { slug: slug },
    locale: locale,
  });
  return newVar[0];
}

async function importCategories() {
  const imported = [];
  console.log("Processing categories import");
  await Promise.all(
    categories.map(async (item) => {
      let exist = await getCategoryBySlug(item.en.slug, "en");
      if (exist) {
        imported.push(exist);
        exist = await getCategoryBySlug(item.fr.slug, "fr");
        imported.push(exist);
        exist = await getCategoryBySlug(item.de.slug, "de");
        imported.push(exist);
        return;
      }
      const fr = await createEntry({ model: "category", entry: item.fr });
      const de = await createEntry({ model: "category", entry: item.de });
      const en = await createEntry({
        model: "category",
        entry: {
          ...item.en,
          localizations: [fr.id, de.id],
        },
      });
      imported.push(en);
      imported.push(fr);
      imported.push(de);
    })
  );
  fs.writeFileSync("./data/importedCategories.json", JSON.stringify(imported));
}

const linkLocalizations = async (model) => {
  const items = await strapi.entityService.findMany(`api::${model}.${model}`, {
    filters: {
      locale: "en",
    },
    populate: ["localizations"],
  });
  for (const englishItem of items) {
    await strapi.db.query(`api::${model}.${model}`).update({
      where: { id: englishItem.localizations[0].id },
      data: {
        localizations: [
          {
            id: englishItem.id,
            locale: "en",
          },
          {
            id: englishItem.localizations[1].id,
            locale: englishItem.localizations[1].locale,
          },
        ],
      },
    });
    await strapi.db.query(`api::${model}.${model}`).update({
      where: { id: englishItem.localizations[1].id },
      data: {
        localizations: [
          {
            id: englishItem.id,
            locale: "en",
          },
          {
            id: englishItem.localizations[0].id,
            locale: englishItem.localizations[0].locale,
          },
        ],
      },
    });
  }
};

const importArticles = async (data) => {
  const articles = data["api::article.article"];
  for (const id in articles) {
    const article = articles[id];
    const exist = await strapi.entityService.findOne(
      "api::article.article",
      id
    );
    if (exist) {
      continue;
    }
    article.title = article.title.replaceAll("WeDo", "WEDO");
    article.content = article.content.replaceAll("WeDo", "WEDO");
    article.description = article.description.replaceAll("WeDo", "WEDO");
    article.seo.metaTitle = article.seo.metaTitle.replaceAll("WeDo", "WEDO");
    article.seo.metaDescription = article.seo.metaDescription.replaceAll(
      "WeDo",
      "WEDO"
    );
    console.log("Processing post", article.title);

    let filename = getFilename(article.image);
    await downloadImage(
      article.image,
      path.join(__dirname, "..", ".tmp/images", filename)
    );
    const files = {
      image: getFileData(filename),
    };

    article.content = article.content.replaceAll(
      "https://mlyjlrnhequ7.i.optimole.com/w:auto/h:auto/q:auto/",
      ""
    );
    let allImages = article.content.matchAll(/!\[(.*?)\]\((.*?)\)/g);
    const extractedImages = Array.from(allImages);
    for (const image of extractedImages) {
      const url = image[2];
      let filename = getFilename(url);
      await downloadImage(
        url,
        path.join(__dirname, "..", ".tmp/images", filename)
      );
      const fileData = getFileData(filename);
      const uploadedFile = await uploadImage(fileData);
      article.content = article.content.replaceAll(url, uploadedFile.url);
    }
    let allLinks = article.content.matchAll(/!\[(.*?)\]\((.*?)\)/g);
    const extractedLinks = Array.from(allLinks);
    for (const link of extractedLinks) {
      const url = new URL(link[2]);
      if (url.hostname.indexOf("wedo.swiss") >= 0) {
        const pathname = url.pathname.substring(1);
        if (pathname.startsWith("mailing/")) {
          // Keep
          console.log("Mailing", link[2]);
        } else if (pathname.startsWith("wp-content/")) {
          // Upload
          let filename = getFilename(url);
          await downloadImage(
            url,
            path.join(__dirname, "..", ".tmp/uploads", filename)
          );
          const fileData = getFileData(filename);
          const uploadedFile = await uploadImage(fileData);
          article.content = article.content.replaceAll(
            link[2],
            uploadedFile.url
          );
        } else {
          console.log("WTF?", link[2]);
          continue;
        }
      }
    }

    await createEntry({
      model: "article",
      entry: {
        ...article,
        createdBy: 1,
        updatedBy: 1,
      },
      files,
    });

    if (article.localizations) {
      await strapi.db.query("api::article.article").update({
        where: { id: article.localizations[0] },
        data: {
          localizations: [
            {
              id: article.id,
              locale: "en",
            },
            {
              id: article.localizations[1],
              locale: "de",
            },
          ],
        },
      });
      await strapi.db.query("api::article.article").update({
        where: { id: article.localizations[1] },
        data: {
          localizations: [
            {
              id: article.id,
              locale: "en",
            },
            {
              id: article.localizations[0],
              locale: "fr",
            },
          ],
        },
      });
    }
  }
};

const resetMedia = async () => {
  const files = await strapi.plugins["upload"].services.upload.findMany({
    filters: {
      folderPath: `/${BLOG_FOLDER_ID}`,
    },
  });

  // console.log(files[3]);
  // await strapi.plugins["upload"].services.upload.updateFileInfo(files[3].id, {
  //   folder: 26,
  // });
  // return;
  for (const file of files) {
    console.log(file);
    await strapi.plugins["upload"].services.upload.updateFileInfo(file.id, {
      folder: BLOG_FOLDER_ID,
    });
    // await strapi.entityService.update("plugin::upload.file", file.id, {
    //   data: {
    //     folderPath: `/${BLOG_FOLDER_ID}`,
    //   },
    // });
  }
};

module.exports = async () => {
  const shouldImportSeedData = true;

  if (shouldImportSeedData) {
    try {
      console.log("Setting up the template...");
      // await linkLocalizations("tag");
      // await linkLocalizations("category");
      // await resetMedia();
      // await importTags();
      // await importCategories();
      // await importArticles(dataFr);
      // await importArticles(dataEn);
      console.log("Ready to go");
    } catch (error) {
      console.log("Could not import seed data");
      console.error(error);
    }
  }
};
