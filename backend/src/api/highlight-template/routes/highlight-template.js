'use strict';

/**
 * highlight-template router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::highlight-template.highlight-template');
