'use strict';

/**
 * highlight-template controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::highlight-template.highlight-template');
