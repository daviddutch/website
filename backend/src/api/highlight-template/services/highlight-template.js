'use strict';

/**
 * highlight-template service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::highlight-template.highlight-template');
