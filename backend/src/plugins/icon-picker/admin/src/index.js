import { prefixPluginTranslations } from "@strapi/helper-plugin";
import pluginId from "./pluginId";
import IconPickerIcon from "./components/IconPickerIcon";
import getTrad from "./utils/getTrad";

export default {
  register(app) {
    app.customFields.register({
      name: "icon",
      pluginId: "icon-picker",
      type: "string",
      icon: IconPickerIcon,
      intlLabel: {
        id: getTrad("icon-picker.label"),
        defaultMessage: "Icon",
      },
      intlDescription: {
        id: getTrad("icon-picker.description"),
        defaultMessage: "Select any icon",
      },
      components: {
        Input: async () =>
          import(
            /* webpackChunkName: "icon-picker-input-component" */ "./components/IconPickerInput"
          ),
      },
      options: {
        advanced: [
          {
            sectionTitle: {
              id: "global.settings",
              defaultMessage: "Settings",
            },
            items: [
              {
                name: "required",
                type: "checkbox",
                intlLabel: {
                  id: getTrad("icon-picker.options.advanced.requiredField"),
                  defaultMessage: "Required field",
                },
                description: {
                  id: getTrad(
                    "icon-picker.options.advanced.requiredField.description"
                  ),
                  defaultMessage:
                    "You won't be able to create an entry if this field is empty",
                },
              },
            ],
          },
        ],
      },
    });
  },
  async registerTrads({ locales }) {
    const importedTrads = await Promise.all(
      locales.map((locale) => {
        return import(`./translations/${locale}.json`)
          .then(({ default: data }) => {
            return {
              data: prefixPluginTranslations(data, pluginId),
              locale,
            };
          })
          .catch(() => {
            return {
              data: {},
              locale,
            };
          });
      })
    );

    return Promise.resolve(importedTrads);
  },
};
