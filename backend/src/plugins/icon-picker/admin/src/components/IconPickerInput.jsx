import React from "react";
import PropTypes from "prop-types";
import { Stack } from "@strapi/design-system/Stack";
import { Flex } from "@strapi/design-system/Flex";
import { Box } from "@strapi/design-system/Box";
import { Link } from "@strapi/design-system/Link";
import {
  Field,
  FieldError,
  FieldHint,
  FieldLabel,
} from "@strapi/design-system/Field";
import { useIntl } from "react-intl";
import { Option, Select } from "@strapi/design-system/Select";

const icons = [
  "arrowsToCircle",
  "ballotCheck",
  "barsProgress",
  "briefcase",
  "buildingUser",
  "calendarStar",
  "check",
  "checkDouble",
  "clipboardList",
  "clipboardListCheck",
  "fileExport",
  "headSideBrain",
  "hospitalUser",
  "hourglassStart",
  "keyboard",
  "objectsColumn",
  "peopleGroup",
  "rectangleHistory",
  "repeat",
  "school",
  "shieldHalved",
  "spinnerThird",
  "user",
  "userCheck",
  "userGroup",
  "users",
  "usersBetweenLines",
  "usersLine",
  "usersMedical",
  "usersRays",
  "usersRectangle",
  "userTie",
  "userTieHair",
];

const IconPickerInput = ({
  attribute,
  description,
  disabled,
  error,
  intlLabel,
  labelAction,
  name,
  onChange,
  required,
  value,
}) => {
  const { formatMessage } = useIntl();

  return (
    <Field
      name={name}
      id={name}
      // GenericInput calls formatMessage and returns a string for the error
      error={error}
      hint={description && formatMessage(description)}
    >
      <Stack spacing={1}>
        <FieldLabel action={labelAction} required={required}>
          {formatMessage(intlLabel)}
        </FieldLabel>

        <Flex>
          <Box padding={1} flex={1}>
            <Select
              id="select1"
              required={required}
              placeholder={formatMessage(intlLabel)}
              hint={description && formatMessage(description)}
              error={error}
              value={value}
              onChange={(newValue) => {
                console.log(newValue);
                return onChange({
                  target: { name, value: newValue, type: attribute.type },
                });
              }}
              disabled={disabled}
            >
              {icons.map((icon) => (
                <Option value={icon}>{icon}</Option>
              ))}
            </Select>
          </Box>
          <Box padding={1}>
            {value && (
              <Link href={"https://fontawesome.com/icons/" + value} isExternal>
                View icon
              </Link>
            )}
          </Box>
        </Flex>

        <FieldHint />
        <FieldError />
      </Stack>
    </Field>
  );
};

IconPickerInput.defaultProps = {
  description: null,
  disabled: false,
  error: null,
  labelAction: null,
  required: false,
  value: "",
};

IconPickerInput.propTypes = {
  intlLabel: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
  attribute: PropTypes.object.isRequired,
  name: PropTypes.string.isRequired,
  description: PropTypes.object,
  disabled: PropTypes.bool,
  error: PropTypes.string,
  labelAction: PropTypes.object,
  required: PropTypes.bool,
  value: PropTypes.string,
};

export default IconPickerInput;
