const fs = require("fs");
const xml2js = require("xml2js");
const WPAPI = require("wpapi");
const Excel = require("exceljs");

const importTags = async () => {
  try {
    var wp = new WPAPI({ endpoint: "https://www.wedo.swiss/wp-json" });
    const tags = await wp.tags().param("lang", "de").perPage(100).get();
    const data = tags.map((tag) => ({
      name: tag.name,
      slug: tag.slug,
      locale: "de",
    }));
    console.log(data);

    const workbook = new Excel.Workbook();
    await workbook.xlsx.readFile("./src/tags.xlsx");

    const worksheet = workbook.getWorksheet("Sheet1");

    const data2 = [];
    worksheet.eachRow(async function (row, rowNumber) {
      if (rowNumber === 1) {
        return;
      }
      const item = {
        en: {
          name: row.getCell(1).value,
          slug: row.getCell(2).value,
          locale: "en",
          seo: {
            metaTitle: row.getCell(1).value,
            metaDescription: row.getCell(1).value,
          },
        },
        fr: {
          name: row.getCell(3).value,
          slug: row.getCell(4).value,
          locale: "fr",
          seo: {
            metaTitle: row.getCell(3).value,
            metaDescription: row.getCell(3).value,
          },
        },
        de: {
          name: row.getCell(5).value,
          slug: row.getCell(6).value,
          locale: "de",
          seo: {
            metaTitle: row.getCell(5).value,
            metaDescription: row.getCell(5).value,
          },
        },
      };
      data2.push(item);
    });
    fs.writeFileSync("../backend/data/tags.json", JSON.stringify(data2));
  } catch (e) {
    console.log(e);
  }
};

importTags().then(() => {
  console.log("I'm done!");
});
