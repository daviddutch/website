const fs = require("fs");
const xml2js = require("xml2js");
const { NodeHtmlMarkdown } = require("node-html-markdown");

const authorMap = {
  chris: 4,
  david: 6,
  Gilles: 1,
  helena: 3,
  Mathilde: 2,
  Sabrina: 8,
  sugadmin: 5,
};
const categoryMap = {
  "customer-testimonials": 6,
  wedo: 4,
  press: 2,
  productivity: 5,
  updates: 1,
  "use-cases": 3,
  "mise-a-jour": 7,
  "mise-a-jour": 7,
};

function convertPost(post, nhm) {
  const link = post["link"][0].substring(19);
  const linkParts = link.split("/");
  let lang = "en";
  let slug = linkParts[0];
  if (linkParts.length > 2) {
    lang = linkParts[0];
    slug = linkParts[1];
  }
  const categories = post["category"].map((c) => ({
    domain: c.$.domain,
    name: c.$.nicename,
  }));
  const newPost = {
    id: post["wp:post_id"][0],
    title: post["title"][0],
    description: nhm.translate(post["excerpt:encoded"][0]),
    content: nhm.translate(post["content:encoded"][0]),
    slug: slug,
    publicationDate: post["wp:post_date_gmt"][0],
    createdAt: post["wp:post_date_gmt"][0],
    updatedAt: post["wp:post_modified"][0],
    publishedAt: post["wp:post_date_gmt"][0],
    locale: lang,
    categories: categories
      .filter((c) => c.domain === "category")
      .map((c) => categoryMap[c.name]),
    image: null,
    author: authorMap[post["dc:creator"][0]],
    seo: null,
    tags: categories.filter((c) => c.domain === "post_tag").map((c) => c.name),
    createdBy: 1,
    updatedBy: 1,
  };
  return newPost;
}

const importPosts = async () => {
  const nhm = new NodeHtmlMarkdown();

  try {
    const xml = fs.readFileSync("./src/posts.xml", "utf8");
    xml2js.parseString(xml, (err, result) => {
      if (err) {
        throw err;
      }
      const data = {
        version: 2,
        data: {
          "api::article.article": {},
        },
      };
      const wpPosts = result.rss.channel[0].item;
      const posts = {
        en: [],
        fr: {},
        de: {},
      };
      let i = 0;
      for (const post of wpPosts) {
        const newPost = convertPost(post, nhm);
        if (newPost.locale === "en") {
          posts.en.push(newPost);
        } else {
          posts[newPost.locale][newPost.slug] = newPost;
        }
      }
      for (const post in posts.en) {
        data.data["api::article.article"][post.id] = post;
        i++;
        if (i > 2) break;
      }
      console.log(data);
    });
  } catch (e) {
    console.log(e);
  }
};

importPosts().then(() => {
  console.log("I'm done!");
});
