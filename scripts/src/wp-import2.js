const fs = require("fs");
const xml2js = require("xml2js");
const { NodeHtmlMarkdown } = require("node-html-markdown");
const categories = require("../../backend/data/importedCategories.json");
const tags = require("../../backend/data/importedTags.json");

const authorMap = {
  chris: 4,
  david: 6,
  Gilles: 1,
  helena: 3,
  Mathilde: 2,
  Sabrina: 8,
  sugadmin: 5,
};

const htmlEntities = (str) => String(str).replace(/'/g, "&#039;");

const getCategory = (name, locale) => {
  if (!name) {
    return "";
  }
  let find = categories.find(
    (c) => htmlEntities(c.name) === name && c.locale === locale
  );
  if (!find) {
    console.log(name, locale);
  }
  return find.id;
};
const getTag = (name, locale) => {
  if (!name) {
    return "";
  }
  let find = tags.find((c) => c.name === name && c.locale === locale);
  if (!find) {
    console.log(name, locale);
  }
  return find.id;
};

const convertPost = (post, nhm) => ({
  id: post["id"][0],
  title: post["Title"][0],
  description: nhm.translate(post["Excerpt"][0]),
  content: nhm.translate(post["Content"][0]),
  slug: post["Slug"][0],
  publicationDate: post["Date"][0],
  createdAt: post["Date"][0],
  updatedAt: post["Date"][0],
  publishedAt: post["Date"][0],
  locale: post["WPMLLanguageCode"][0],
  translationId: post["WPMLTranslationID"][0],
  categories: post["Categories"][0]
    .split("|")
    .map((c) => getCategory(c, post["WPMLLanguageCode"][0]))
    .filter((v) => v !== ""),
  image: post["ImageURL"][0].split("|")[0],
  author: authorMap[post["AuthorUsername"][0]],
  seo: {
    metaTitle: post["Title"][0],
    metaDescription: nhm.translate(post["Excerpt"][0]),
  },
  tags: post["Tags"][0]
    .split("|")
    .map((c) => getTag(c, post["WPMLLanguageCode"][0]))
    .filter((v) => v !== ""),
  createdBy: authorMap[post["AuthorUsername"][0]],
  updatedBy: authorMap[post["AuthorUsername"][0]],
});

const importPosts = async () => {
  const nhm = new NodeHtmlMarkdown();

  try {
    const xml = fs.readFileSync("./src/posts4.xml", "utf8");
    xml2js.parseString(xml, (err, result) => {
      if (err) {
        throw err;
      }
      const dataEN = {
        version: 2,
        data: {
          "api::article.article": {},
        },
      };
      const dataFRDE = {
        version: 2,
        data: {
          "api::article.article": {},
        },
      };
      const wpPosts = result.data.post;
      const posts = {};
      for (const post of wpPosts) {
        const newPost = convertPost(post, nhm);
        if (!posts[newPost.translationId]) {
          posts[newPost.translationId] = {
            en: null,
            de: null,
            fr: null,
          };
        }
        posts[newPost.translationId][newPost.locale] = newPost;
      }
      let i = 0;
      for (const postTransId in posts) {
        i++;
        const post = posts[postTransId];
        if (!post.en) {
          console.log("Missing translation for ", post.fr.title);
          continue;
        }
        dataEN.data["api::article.article"][post.en.id] = Object.assign(
          post.en,
          {
            localizations: [post.fr.id, post.de.id],
          }
        );
        post.fr.slug = `fr-${post.fr.slug}`;
        post.de.slug = `de-${post.de.slug}`;
        dataFRDE.data["api::article.article"][post.fr.id] = post.fr;
        dataFRDE.data["api::article.article"][post.de.id] = post.de;
      }
      fs.writeFileSync("../backend/data/postsEn.json", JSON.stringify(dataEN));
      fs.writeFileSync(
        "../backend/data/postsFrDe.json",
        JSON.stringify(dataFRDE)
      );
      console.log(dataEN);
    });
  } catch (e) {
    console.log(e);
  }
};

importPosts().then(() => {
  console.log("I'm done!");
});
