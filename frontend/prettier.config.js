module.exports = {
    plugins: [require('prettier-plugin-tailwindcss')],
    printWidth: 120,
    tabWidth: 4,
    singleQuote: true,
    semi: true,
    importOrder: ['react', '<THIRD_PARTY_MODULES>', '^@/(.*)$', '^[./]'],
};
