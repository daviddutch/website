/** @type {import('tailwindcss').Config} */
module.exports = {
    content: ['./src/**/*.{js,ts,jsx,tsx}'],
    theme: {
        fontFamily: {
            sans: [
                'Inter',
                'ui-sans-serif',
                'system-ui',
                '-apple-system',
                'BlinkMacSystemFont',
                '"Segoe UI"',
                'Roboto',
                '"Helvetica Neue"',
                'Arial',
                '"Noto Sans"',
                'sans-serif',
                '"Apple Color Emoji"',
                '"Segoe UI Emoji"',
                '"Segoe UI Symbol"',
                '"Noto Color Emoji"',
            ],
            serif: ['ui-serif', 'Georgia', 'Cambria', '"Times New Roman"', 'Times', 'serif'],
            mono: [
                'ui-monospace',
                'SFMono-Regular',
                'Menlo',
                'Monaco',
                'Consolas',
                '"Liberation Mono"',
                '"Courier New"',
                'monospace',
            ],
        },
        extend: {
            animation: {
                marquee: 'marquee 60s linear infinite',
                marquee2: 'marquee2 60s linear infinite',
            },
            keyframes: {
                marquee: {
                    '0%': { transform: 'translateX(0%)' },
                    '100%': { transform: 'translateX(-100%)' },
                },
                marquee2: {
                    '0%': { transform: 'translateX(100%)' },
                    '100%': { transform: 'translateX(0%)' },
                },
            },
            backgroundImage: {
                'play-blue': "url('/images/play-blue.svg')",
                'play-green': "url('/images/play-green.svg')",
                'play-orange': "url('/images/play-orange.svg')",
                'play-purple': "url('/images/play-purple.svg')",
                'play-yellow': "url('/images/play-yellow.svg')",
                'play-red': "url('/images/play-red.svg')",
                dotted: 'radial-gradient(#e1e7ec 20%, transparent 20%)',
                'dotted-light': 'radial-gradient(#f0f2f5 20%, transparent 20%)',
                'light-blue-gradient': 'linear-gradient(to right, #ECF7FF 0%, #C1E3FF 100%)',
                'blue-gradient': 'linear-gradient(to right, #2F94E7 0%, #0072CE 100%)',
                'purple-blue-gradient': 'linear-gradient(to right, #a855f7 0%, #0760A7 100%)',
            },
            backgroundPosition: {
                'dotted-10': '0 0, 10px 10px',
            },
            backgroundSize: {
                'dotted-20': '20px 20px',
            },
            boxShadow: {
                'dotted-fade': 'inset 0 0 200px #fff, inset 0 0 200px #fff',
            },
            gridTemplateColumns: {
                'auto-1fr': 'auto 1fr',
            },
            colors: {
                blue: {
                    900: '#00263A',
                    800: '#023454',
                    700: '#03477C',
                    600: '#0760A7',
                    500: '#0072CE',
                    400: '#2F94E7',
                    300: '#67BCFF',
                    200: '#C1E3FF',
                    100: '#ECF7FF',
                    50: '#EFF6FF',
                },
                // gray: {
                //   900: '#212934',
                //   800: '#414953',
                //   700: '#5f6b7a',
                //   600: '#8895a7',
                //   500: '#b8c4ce',
                //   400: '#cfd6de',
                //   300: '#e1e7ec',
                //   200: '#f0f2f5',
                //   100: '#f8f9fa',
                //   50: '#F8FAFC',
                // }
            },
        },
    },
    plugins: [require('@tailwindcss/forms'), require('@tailwindcss/typography'), require('@tailwindcss/aspect-ratio')],
};
