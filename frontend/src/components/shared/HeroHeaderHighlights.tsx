import { StrapiImage } from '@/components/shared/StrapiImage';
import { StrapiMedia } from '@/utils/media';
import { Highlight, HighlightTemplate } from '@/types/LandingPage';
import { StrapiIcon } from '@/components/shared/StrapiIcon';

export function HeroHeaderHighlights({
    title,
    text,
    image,
    highlights,
}: {
    title: string;
    text: string;
    image: StrapiMedia;
    highlights: (Highlight | HighlightTemplate)[];
}) {
    return (
        <div className="bg-white">
            <div className="relative pb-32">
                <div className="absolute inset-0">
                    <StrapiImage media={image} className="h-full w-full object-cover" />
                    <div className="absolute inset-0 bg-blue-800 mix-blend-multiply" aria-hidden="true" />
                </div>
                <div className="relative mx-auto max-w-7xl py-24 px-4 sm:py-32 sm:px-6 lg:px-8">
                    <h1 className="text-4xl font-bold tracking-tight text-white md:text-5xl lg:text-6xl">{title}</h1>
                    <p className="mt-6 max-w-3xl text-xl text-gray-300">{text}</p>
                </div>
            </div>

            <section
                className="relative z-10 mx-auto -mt-32 max-w-7xl px-4 sm:px-6 lg:px-8"
                aria-labelledby="contact-heading"
            >
                <div className="grid grid-cols-1 gap-y-20 lg:grid-cols-3 lg:gap-y-0 lg:gap-x-8">
                    {highlights.map((highlight, index) => {
                        const attributes = 'attributes' in highlight ? highlight.attributes : highlight;
                        return (
                            <div key={index} className="flex flex-col rounded-2xl bg-white shadow-xl">
                                <div className="relative flex-1 px-6 pt-16 pb-8 md:px-8">
                                    <div className="absolute top-0 inline-block -translate-y-1/2 transform rounded-xl bg-blue-600 p-5 shadow-lg">
                                        <StrapiIcon
                                            icon={attributes.icon}
                                            className="h-6 w-6 text-center text-2xl text-white"
                                        />
                                    </div>
                                    <h3 className="text-xl font-medium text-gray-900">{attributes.title}</h3>
                                    <p className="mt-4 text-base text-gray-500">{attributes.description}</p>
                                </div>
                            </div>
                        );
                    })}
                </div>
            </section>
        </div>
    );
}
