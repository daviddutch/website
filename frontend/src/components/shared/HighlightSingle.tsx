import { StrapiImage } from '@/components/shared/StrapiImage';
import { StrapiMedia } from '@/utils/media';

export const HighlightSingle = ({
    title,
    description,
    image,
}: {
    title: string;
    description: string;
    image: StrapiMedia;
}) => {
    return (
        <>
            <div className="relative overflow-hidden bg-gray-50 pt-16 sm:pt-24 lg:pt-32">
                <div className="mx-auto max-w-md px-4 text-center sm:max-w-3xl sm:px-6 lg:max-w-7xl lg:px-8">
                    <div>
                        <p className="mt-2 bg-gradient-to-r from-blue-400 to-blue-700 bg-clip-text pb-3 text-3xl font-bold tracking-tight text-gray-900 text-transparent sm:text-4xl">
                            {title}
                        </p>
                        <p className="mx-auto mt-5 max-w-prose text-xl text-gray-500">{description}</p>
                    </div>
                    <div className="mt-12">
                        <StrapiImage
                            media={image}
                            className="rounded-t-lg shadow-lg ring-1 ring-black ring-opacity-5"
                        />
                    </div>
                </div>
            </div>
        </>
    );
};
