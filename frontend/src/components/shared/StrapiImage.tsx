import { getStrapiMedia, StrapiMedia } from '@/utils/media';
import React, { ComponentProps } from 'react';
import Image from 'next/image';

type Props = {
    media: StrapiMedia;
    className?: string;
    alt?: string;
    width?: number;
    height?: number;
} & Omit<ComponentProps<typeof Image>, 'src' | 'width' | 'height' | 'alt'>;

export function StrapiImage({ media, className, alt, width, height, ...props }: Props) {
    if (!media?.data) {
        return <></>;
    }
    return (
        <Image
            className={className}
            width={width || media.data.attributes.width || 1000}
            height={height || media.data.attributes.height || 1000}
            src={getStrapiMedia(media)}
            alt={alt || media.data.attributes.alternativeText || ''}
            {...props}
        />
    );
}
