import Head from 'next/head';
import { SeoMeta } from '@/types/SeoMeta';
import { t } from '@lingui/macro';
import { Post } from '@/types/Post';
import { getStrapiMedia } from '@/utils/media';

const getUrl = (path = '') => {
    return `${process.env.NEXT_PUBLIC_SITE_URL}${path}`;
};

export const SeoHeaders = ({ seo, post }: { seo: SeoMeta; post?: Post }) => {
    const defaultSeo = {
        metaTitle: t`Meetings, Tasks & Checklists for Teams`,
        metaDescription: t`With the collaborative platform WEDO, prepare your meetings in teams, write the minutes in real-time, and follow the tasks assigned to your colleagues.`,
    };

    const seoWithDefaults = {
        ...defaultSeo,
        ...seo,
    };
    const ogImageData = {
        url: post ? getStrapiMedia(post.attributes.image) : null,
        title: seoWithDefaults.metaTitle,
        description: seoWithDefaults.metaDescription,
    };
    const fullSeo = {
        ...seoWithDefaults,
        metaTitle: `WEDO | ${seoWithDefaults.metaTitle}`,
        shareImage: getUrl('/api/og?data=' + btoa(unescape(encodeURIComponent(JSON.stringify(ogImageData))))),
    };

    return (
        <Head>
            {fullSeo.metaTitle && (
                <>
                    <title>{fullSeo.metaTitle}</title>
                    <meta property="og:title" content={fullSeo.metaTitle} />
                    <meta name="twitter:title" content={fullSeo.metaTitle} />
                </>
            )}
            {fullSeo.metaDescription && (
                <>
                    <meta name="description" content={fullSeo.metaDescription} />
                    <meta property="og:description" content={fullSeo.metaDescription} />
                    <meta name="twitter:description" content={fullSeo.metaDescription} />
                </>
            )}
            {fullSeo.shareImage && (
                <>
                    <meta property="og:image" content={fullSeo.shareImage} />
                    <meta name="twitter:image" content={fullSeo.shareImage} />
                    <meta name="image" content={fullSeo.shareImage} />
                </>
            )}
            {fullSeo.article && <meta property="og:type" content="article" />}
            <meta name="twitter:card" content="summary_large_image" />
        </Head>
    );
};
