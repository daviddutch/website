import { Fragment, useState } from 'react';
import { Disclosure, Popover, Transition } from '@headlessui/react';
import Image from 'next/image';
import { t, Trans } from '@lingui/macro';
import clsx from 'clsx';
import {
    faBank,
    faBarsProgress,
    faBriefcase,
    faBuilding,
    faCalendarStar,
    faCheck,
    faChevronDown,
    faClipboardList,
    faDesktop,
    faHandHoldingHeart,
    faHospital,
    faHouse,
    faInfoCircle,
    faLifeRing,
    faMailbox,
    faNotebook,
    faPhone,
    faPlay,
    faSchool,
    faShield,
    faUser,
    faUsers,
    faUserTie,
} from '@fortawesome/pro-duotone-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars, faCommentLines, faXmark } from '@fortawesome/pro-solid-svg-icons';
import Link from 'next/link';
import { Button } from '@/components/shared/Button';
import { useScrollPosition } from '@/hooks/useScrollPosition';

const features = [
    {
        name: t`Meetings`,
        description: t`Prepare your meetings in teams, write the minutes in real time and follow the tasks assigned to your colleagues.`,
        href: '/features/meetings',
        icon: faCalendarStar,
    },
    {
        name: t`Tasks`,
        description: t`Create workspaces for your projects, departments, and teams, and then share tasks with your teammates.`,
        href: '/features/tasks',
        icon: faCheck,
    },
    {
        name: t`Checklists`,
        description: t`Save templates for your proceedings then follow the progress of your checklists in real time.`,
        href: '/features/checklists',
        icon: faClipboardList,
    },
    {
        name: t`Project Management`,
        description: t`Create project workspaces, schedule meetings, assign tasks and share files with your project members.`,
        href: '/features/project-management',
        icon: faBarsProgress,
    },
];
const callsToAction = [
    { name: t`Watch Demo`, href: '/demo', icon: faPlay },
    { name: t`Contact Sales`, href: '/contact', icon: faPhone },
];
const company = [
    { name: t`About Us`, href: '/about', icon: faInfoCircle },
    { name: t`Jobs`, href: '/jobs', icon: faBuilding },
    { name: t`Events`, href: '/events', icon: faDesktop },
    { name: t`Contact`, href: '/contact', icon: faMailbox },
];
const resources = [
    { name: t`Blog`, href: '/blog', icon: faCommentLines },
    { name: t`Help Center`, href: 'https://help.wedo.swiss', icon: faLifeRing },
    { name: t`Security`, href: '/security', icon: faShield },
    { name: t`Request demo`, href: '/demo', icon: faPlay },
];
const useCases = [
    { name: t`Team Meetings`, href: '/use-cases/team-meetings', icon: faUsers },
    { name: t`One-on-Ones`, href: '/use-cases/one-on-ones', icon: faUser },
    {
        name: t`Executive Committees`,
        href: '/use-cases/executive-committees',
        icon: faBriefcase,
    },
    { name: t`Board Meetings`, href: '/use-cases/board-meetings', icon: faUserTie },
    { name: t`Remote Teams`, href: '/use-cases/remote-teams', icon: faDesktop },
    {
        name: t`Executive Assistant`,
        href: '/use-cases/executive-assistant',
        icon: faNotebook,
    },
];
const industries = [
    { name: t`SMEs`, href: '/industries/sme', icon: faHouse },
    { name: t`Banks & Insurances`, href: '/industries/banks-insurances', icon: faBank },
    {
        name: t`Municipal Administrations`,
        href: '/industries/municipal-administrations',
        icon: faBuilding,
    },
    { name: t`Nursing Homes`, href: '/industries/nursing-homes', icon: faHospital },
    {
        name: t`Foundations & Associations`,
        href: '/industries/foundations-associations',
        icon: faHandHoldingHeart,
    },
    {
        name: t`Schools & Faculties`,
        href: '/industries/schools-faculties',
        icon: faSchool,
    },
];
const blogPosts = [
    {
        id: 1,
        name: t`Improve decision-making with voting`,
        href: '/blog/improve-decision-making-with-voting',
        preview: t`To facilitate the collection of votes and the adoption of decisions, we have integrated a new feature that allows you to vote before or during meetings.`,
        imageUrl: 'https://wedo-assets.sos-ch-gva-2.exo.io/VOTING_improve_decision_making_with_voting_00e53dd840.png',
    },
    {
        id: 2,
        name: t`Organize your files with WEDO`,
        href: '/blog/organize-your-files-with-wedo',
        preview: t`It is now possible to create folders to organize your files in WEDO. This improvement makes it easier for you to find and share documents with other users.`,
        imageUrl: 'https://wedo-assets.sos-ch-gva-2.exo.io/FILE_organize_your_files_with_WEDO_25e8d36bfd.png',
    },
];

function MobileMenuGroup({ title, links }: { title: string; links: any[] }) {
    return (
        <Disclosure as="div" key={title} className="pt-6">
            {({ open, close }) => (
                <>
                    <dt className="text-lg">
                        <Disclosure.Button className="flex w-full items-start justify-between text-left text-gray-400">
                            <span className="font-medium text-gray-900">
                                <Trans id={title} />
                            </span>
                            <span className="ml-6 flex h-7 items-center">
                                <FontAwesomeIcon
                                    className={clsx(open ? '-rotate-180' : 'rotate-0', 'h-6 w-6 transform')}
                                    aria-hidden="true"
                                    icon={faChevronDown}
                                />
                            </span>
                        </Disclosure.Button>
                    </dt>
                    <Disclosure.Panel as="dd" className="mt-2 pr-12">
                        <ul role="list" className="mt-5 space-y-6">
                            {links.map((item) => (
                                <li key={item.name} className="flow-root">
                                    <Link
                                        href={item.href}
                                        onClick={close}
                                        className="-m-3 flex items-center rounded-md p-3 text-base font-medium text-gray-900 hover:bg-gray-50"
                                    >
                                        <FontAwesomeIcon
                                            icon={item.icon}
                                            className="h-6 w-6 flex-shrink-0 text-gray-400"
                                            aria-hidden="true"
                                        />
                                        <span className="ml-4">
                                            <Trans id={item.name} />
                                        </span>
                                    </Link>
                                </li>
                            ))}
                        </ul>
                    </Disclosure.Panel>
                </>
            )}
        </Disclosure>
    );
}

export function Header() {
    const [hideOnScroll, setHideOnScroll] = useState(true);

    useScrollPosition(
        ({ prevPos, currPos }) => {
            const isShow = currPos.y > prevPos.y;
            if (isShow !== hideOnScroll) setHideOnScroll(isShow);
        },
        [hideOnScroll]
    );

    return (
        <Popover className="relative top-0 z-50 bg-white md:sticky">
            <div
                className={clsx('pointer-events-none absolute inset-0 z-30', hideOnScroll ? '' : 'shadow')}
                aria-hidden="true"
            />
            <div className="relative z-20">
                <div className="mx-auto flex max-w-7xl items-center justify-between px-4 py-5 sm:px-6 sm:py-4 md:justify-start md:space-x-10 lg:px-8">
                    <div>
                        <Link href="/" className="flex">
                            <span className="sr-only">WEDO</span>
                            <Image
                                className="h-8 w-auto sm:h-10"
                                width={20}
                                height={50}
                                src={'/icon.svg'}
                                alt={'Company logo'}
                                unoptimized
                            />
                        </Link>
                    </div>
                    <div className="-my-2 -mr-2 md:hidden">
                        <Popover.Button className="inline-flex items-center justify-center rounded-md bg-white p-2 text-gray-400 hover:bg-gray-100 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-blue-500">
                            <span className="sr-only">
                                <Trans>Open menu</Trans>
                            </span>
                            <FontAwesomeIcon className="h-6 w-6" aria-hidden="true" icon={faBars} />
                        </Popover.Button>
                    </div>
                    <div className="hidden md:flex md:flex-1 md:items-center md:justify-between">
                        <Popover.Group as="nav" className="flex space-x-10">
                            <Popover>
                                {({ open, close }) => (
                                    <>
                                        <Popover.Button
                                            className={clsx(
                                                open ? 'text-gray-900' : 'text-gray-500',
                                                'group inline-flex items-center rounded-md bg-white text-base font-medium hover:text-gray-900 focus:outline-none'
                                            )}
                                        >
                                            <span>
                                                <Trans>Product</Trans>
                                            </span>
                                            <FontAwesomeIcon
                                                icon={faChevronDown}
                                                className={clsx(
                                                    open ? 'text-gray-600' : 'text-gray-400',
                                                    'ml-2 h-5 w-5 group-hover:text-gray-500'
                                                )}
                                                aria-hidden="true"
                                            />
                                        </Popover.Button>

                                        <Transition
                                            as={Fragment}
                                            enter="transition ease-out duration-200"
                                            enterFrom="opacity-0 -translate-y-1"
                                            enterTo="opacity-100 translate-y-0"
                                            leave="transition ease-in duration-150"
                                            leaveFrom="opacity-100 translate-y-0"
                                            leaveTo="opacity-0 -translate-y-1"
                                        >
                                            <Popover.Panel className="absolute inset-x-0 top-full z-10 hidden transform bg-white shadow-lg md:block">
                                                <div className="mx-auto grid max-w-7xl gap-y-6 px-4 py-6 sm:grid-cols-2 sm:gap-8 sm:px-6 sm:py-8 lg:grid-cols-4 lg:px-8 lg:py-12 xl:py-16">
                                                    {features.map((item) => (
                                                        <Link
                                                            key={item.name}
                                                            href={item.href}
                                                            onClick={close}
                                                            className="-m-3 flex flex-col justify-between rounded-lg p-3 hover:bg-gray-50"
                                                        >
                                                            <div className="flex md:h-full lg:flex-col">
                                                                <div className="flex-shrink-0">
                                                                    <span className="inline-flex h-10 w-10 items-center justify-center rounded-md bg-gradient-to-r from-blue-400 to-blue-500 text-white sm:h-12 sm:w-12">
                                                                        <FontAwesomeIcon
                                                                            icon={item.icon}
                                                                            className="h-6 w-6 text-2xl"
                                                                            aria-hidden="true"
                                                                        />
                                                                    </span>
                                                                </div>
                                                                <div className="ml-4 md:flex md:flex-1 md:flex-col md:justify-between lg:ml-0 lg:mt-4">
                                                                    <div>
                                                                        <p className="text-base font-medium text-gray-900">
                                                                            <Trans id={item.name} />
                                                                        </p>
                                                                        <p className="mt-1 text-sm text-gray-500">
                                                                            <Trans id={item.description} />
                                                                        </p>
                                                                    </div>
                                                                    <p className="mt-2 text-sm font-medium text-blue-500 lg:mt-4">
                                                                        <Trans>Learn more</Trans>
                                                                        <span aria-hidden="true"> &rarr;</span>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </Link>
                                                    ))}
                                                </div>
                                                <div className="bg-gray-50">
                                                    <div className="mx-auto max-w-7xl space-y-6 px-4 py-5 sm:flex sm:space-y-0 sm:space-x-10 sm:px-6 lg:px-8">
                                                        {callsToAction.map((item) => (
                                                            <div key={item.name} className="flow-root">
                                                                <Link
                                                                    href={item.href}
                                                                    onClick={close}
                                                                    className="-m-3 flex items-center rounded-md p-3 text-base font-medium text-gray-900 hover:bg-gray-100"
                                                                >
                                                                    <FontAwesomeIcon
                                                                        icon={item.icon}
                                                                        className="h-6 w-6 flex-shrink-0 text-gray-400"
                                                                        aria-hidden="true"
                                                                    />
                                                                    <span className="ml-3">
                                                                        <Trans id={item.name} />
                                                                    </span>
                                                                </Link>
                                                            </div>
                                                        ))}
                                                    </div>
                                                </div>
                                            </Popover.Panel>
                                        </Transition>
                                    </>
                                )}
                            </Popover>
                            <Popover>
                                {({ open, close }) => (
                                    <>
                                        <Popover.Button
                                            className={clsx(
                                                open ? 'text-gray-900' : 'text-gray-500',
                                                'group inline-flex items-center rounded-md bg-white text-base font-medium hover:text-gray-900 focus:outline-none'
                                            )}
                                        >
                                            <span>
                                                <Trans>Solutions</Trans>
                                            </span>
                                            <FontAwesomeIcon
                                                icon={faChevronDown}
                                                className={clsx(
                                                    open ? 'text-gray-600' : 'text-gray-400',
                                                    'ml-2 h-5 w-5 group-hover:text-gray-500'
                                                )}
                                                aria-hidden="true"
                                            />
                                        </Popover.Button>

                                        <Transition
                                            as={Fragment}
                                            enter="transition ease-out duration-200"
                                            enterFrom="opacity-0 -translate-y-1"
                                            enterTo="opacity-100 translate-y-0"
                                            leave="transition ease-in duration-150"
                                            leaveFrom="opacity-100 translate-y-0"
                                            leaveTo="opacity-0 -translate-y-1"
                                        >
                                            <Popover.Panel className="absolute inset-x-0 top-full z-10 hidden transform shadow-lg md:block">
                                                <div className="absolute inset-0 flex">
                                                    <div className="w-1/2 bg-white" />
                                                    <div className="w-1/2 bg-gray-50" />
                                                </div>
                                                <div className="relative mx-auto grid max-w-7xl grid-cols-1 lg:grid-cols-2">
                                                    <nav className="grid gap-y-10 bg-white px-4 py-8 sm:grid-cols-2 sm:gap-x-8 sm:py-12 sm:px-6 lg:px-8 xl:pr-12">
                                                        <div>
                                                            <h3 className="text-base font-medium text-gray-500">
                                                                <Trans>Use Cases</Trans>
                                                            </h3>
                                                            <ul role="list" className="mt-5 space-y-6">
                                                                {useCases.map((item) => (
                                                                    <li key={item.name} className="flow-root">
                                                                        <Link
                                                                            href={item.href}
                                                                            onClick={close}
                                                                            className="-m-3 flex items-center rounded-md p-3 text-base font-medium text-gray-900 hover:bg-gray-50"
                                                                        >
                                                                            <FontAwesomeIcon
                                                                                icon={item.icon}
                                                                                className="h-6 w-6 flex-shrink-0 text-gray-400"
                                                                                aria-hidden="true"
                                                                            />
                                                                            <span className="ml-4">
                                                                                <Trans id={item.name} />
                                                                            </span>
                                                                        </Link>
                                                                    </li>
                                                                ))}
                                                            </ul>
                                                        </div>
                                                        <div>
                                                            <h3 className="text-base font-medium text-gray-500">
                                                                <Trans>Industries</Trans>
                                                            </h3>
                                                            <ul role="list" className="mt-5 space-y-6">
                                                                {industries.map((item) => (
                                                                    <li key={item.name} className="flow-root">
                                                                        <Link
                                                                            href={item.href}
                                                                            onClick={close}
                                                                            className="-m-3 flex items-center rounded-md p-3 text-base font-medium text-gray-900 hover:bg-gray-50"
                                                                        >
                                                                            <FontAwesomeIcon
                                                                                icon={item.icon}
                                                                                className="h-6 w-6 flex-shrink-0 text-gray-400"
                                                                                aria-hidden="true"
                                                                            />
                                                                            <span className="ml-4">
                                                                                <Trans id={item.name} />
                                                                            </span>
                                                                        </Link>
                                                                    </li>
                                                                ))}
                                                            </ul>
                                                        </div>
                                                    </nav>
                                                    <div className="bg-gray-50 px-4 py-8 sm:py-12 sm:px-6 lg:px-8 xl:pl-12">
                                                        <div>
                                                            <h3 className="text-base font-medium text-gray-500">
                                                                <Trans>From the blog</Trans>
                                                            </h3>
                                                            <ul role="list" className="mt-6 space-y-6">
                                                                {blogPosts.map((post) => (
                                                                    <li key={post.id} className="flow-root">
                                                                        <Link
                                                                            href={post.href}
                                                                            onClick={close}
                                                                            className="-m-3 flex rounded-lg p-3 hover:bg-gray-100"
                                                                        >
                                                                            <div className="hidden flex-shrink-0 sm:block">
                                                                                <Image
                                                                                    className="h-20 w-32 rounded-md object-cover"
                                                                                    height={20}
                                                                                    width={32}
                                                                                    src={post.imageUrl}
                                                                                    alt=""
                                                                                />
                                                                            </div>
                                                                            <div className="w-0 flex-1 sm:ml-8">
                                                                                <h4 className="truncate text-base font-medium text-gray-900">
                                                                                    {post.name}
                                                                                </h4>
                                                                                <p className="mt-1 text-sm text-gray-500">
                                                                                    {post.preview}
                                                                                </p>
                                                                            </div>
                                                                        </Link>
                                                                    </li>
                                                                ))}
                                                            </ul>
                                                        </div>
                                                        <div className="mt-6 text-sm font-medium">
                                                            <Link
                                                                href="/[page]"
                                                                onClick={close}
                                                                className="text-blue-500 hover:text-blue-400"
                                                            >
                                                                <Trans>View all posts</Trans>
                                                                <span aria-hidden="true"> &rarr;</span>
                                                            </Link>
                                                        </div>
                                                    </div>
                                                </div>
                                            </Popover.Panel>
                                        </Transition>
                                    </>
                                )}
                            </Popover>
                            <Link href="/pricing" className="text-base font-medium text-gray-500 hover:text-gray-900">
                                <Trans>Pricing</Trans>
                            </Link>
                            <Popover>
                                {({ open, close }) => (
                                    <>
                                        <Popover.Button
                                            className={clsx(
                                                open ? 'text-gray-900' : 'text-gray-500',
                                                'group inline-flex items-center rounded-md bg-white text-base font-medium hover:text-gray-900 focus:outline-none'
                                            )}
                                        >
                                            <span>
                                                <Trans>Resources</Trans>
                                            </span>
                                            <FontAwesomeIcon
                                                icon={faChevronDown}
                                                className={clsx(
                                                    open ? 'text-gray-600' : 'text-gray-400',
                                                    'ml-2 h-5 w-5 group-hover:text-gray-500'
                                                )}
                                                aria-hidden="true"
                                            />
                                        </Popover.Button>

                                        <Transition
                                            as={Fragment}
                                            enter="transition ease-out duration-200"
                                            enterFrom="opacity-0 -translate-y-1"
                                            enterTo="opacity-100 translate-y-0"
                                            leave="transition ease-in duration-150"
                                            leaveFrom="opacity-100 translate-y-0"
                                            leaveTo="opacity-0 -translate-y-1"
                                        >
                                            <Popover.Panel className="absolute inset-x-0 top-full z-10 hidden transform shadow-lg md:block">
                                                <div className="absolute inset-0 flex">
                                                    <div className="w-1/2 bg-white" />
                                                    <div className="w-1/2 bg-gray-50" />
                                                </div>
                                                <div className="relative mx-auto grid max-w-7xl grid-cols-1 lg:grid-cols-2">
                                                    <nav className="grid gap-y-10 bg-white px-4 py-8 sm:grid-cols-2 sm:gap-x-8 sm:py-12 sm:px-6 lg:px-8 xl:pr-12">
                                                        <div>
                                                            <h3 className="text-base font-medium text-gray-500">
                                                                <Trans>Company</Trans>
                                                            </h3>
                                                            <ul role="list" className="mt-5 space-y-6">
                                                                {company.map((item) => (
                                                                    <li key={item.name} className="flow-root">
                                                                        <Link
                                                                            href={item.href}
                                                                            onClick={close}
                                                                            className="-m-3 flex items-center rounded-md p-3 text-base font-medium text-gray-900 hover:bg-gray-50"
                                                                        >
                                                                            <FontAwesomeIcon
                                                                                icon={item.icon}
                                                                                className="h-6 w-6 flex-shrink-0 text-gray-400"
                                                                                aria-hidden="true"
                                                                            />
                                                                            <span className="ml-4">
                                                                                <Trans id={item.name} />
                                                                            </span>
                                                                        </Link>
                                                                    </li>
                                                                ))}
                                                            </ul>
                                                        </div>
                                                        <div>
                                                            <h3 className="text-base font-medium text-gray-500">
                                                                <Trans>Resources</Trans>
                                                            </h3>
                                                            <ul role="list" className="mt-5 space-y-6">
                                                                {resources.map((item) => (
                                                                    <li key={item.name} className="flow-root">
                                                                        <Link
                                                                            href={item.href}
                                                                            onClick={close}
                                                                            className="-m-3 flex items-center rounded-md p-3 text-base font-medium text-gray-900 hover:bg-gray-50"
                                                                        >
                                                                            <FontAwesomeIcon
                                                                                icon={item.icon}
                                                                                className="h-6 w-6 flex-shrink-0 text-gray-400"
                                                                                aria-hidden="true"
                                                                            />
                                                                            <span className="ml-4">
                                                                                <Trans id={item.name} />
                                                                            </span>
                                                                        </Link>
                                                                    </li>
                                                                ))}
                                                            </ul>
                                                        </div>
                                                    </nav>
                                                    <div className="bg-gray-50 px-4 py-8 sm:py-12 sm:px-6 lg:px-8 xl:pl-12">
                                                        <div>
                                                            <h3 className="text-base font-medium text-gray-500">
                                                                <Trans>From the blog</Trans>
                                                            </h3>
                                                            <ul role="list" className="mt-6 space-y-6">
                                                                {blogPosts.map((post) => (
                                                                    <li key={post.id} className="flow-root">
                                                                        <Link
                                                                            href={post.href}
                                                                            onClick={close}
                                                                            className="-m-3 flex rounded-lg p-3 hover:bg-gray-100"
                                                                        >
                                                                            <div className="hidden flex-shrink-0 sm:block">
                                                                                <Image
                                                                                    className="h-20 w-32 rounded-md object-cover"
                                                                                    height={20}
                                                                                    width={32}
                                                                                    src={post.imageUrl}
                                                                                    alt=""
                                                                                />
                                                                            </div>
                                                                            <div className="w-0 flex-1 sm:ml-8">
                                                                                <h4 className="truncate text-base font-medium text-gray-900">
                                                                                    {post.name}
                                                                                </h4>
                                                                                <p className="mt-1 text-sm text-gray-500">
                                                                                    {post.preview}
                                                                                </p>
                                                                            </div>
                                                                        </Link>
                                                                    </li>
                                                                ))}
                                                            </ul>
                                                        </div>
                                                        <div className="mt-6 text-sm font-medium">
                                                            <Link
                                                                href="/[page]"
                                                                onClick={close}
                                                                className="text-blue-500 hover:text-blue-400"
                                                            >
                                                                <Trans>View all posts</Trans>
                                                                <span aria-hidden="true"> &rarr;</span>
                                                            </Link>
                                                        </div>
                                                    </div>
                                                </div>
                                            </Popover.Panel>
                                        </Transition>
                                    </>
                                )}
                            </Popover>
                        </Popover.Group>
                        <div className="flex items-center md:ml-12">
                            <Link href={'https://login.wedo.swiss'} passHref legacyBehavior>
                                <Button withShadow={false} className="whitespace-nowrap" variant={'link'}>
                                    <Trans>Sign in</Trans>
                                </Button>
                            </Link>
                            <Link href={'/signup'} passHref legacyBehavior>
                                <Button withShadow={false} className="ml-3" variant={'secondary'}>
                                    <Trans>Free trial</Trans>
                                </Button>
                            </Link>
                            <Link href={'/demo'} passHref legacyBehavior>
                                <Button withShadow={false} className="ml-3">
                                    <Trans>Live demo</Trans>
                                </Button>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>

            <Transition
                as={Fragment}
                enter="duration-200 ease-out"
                enterFrom="opacity-0 scale-95"
                enterTo="opacity-100 scale-100"
                leave="duration-100 ease-in"
                leaveFrom="opacity-100 scale-100"
                leaveTo="opacity-0 scale-95"
            >
                <Popover.Panel
                    focus
                    className="absolute inset-x-0 top-0 z-30 origin-top-right transform p-2 transition md:hidden"
                >
                    <div className="divide-y-2 divide-gray-50 rounded-lg bg-white shadow-lg ring-1 ring-black ring-opacity-5">
                        <div className="px-5 pt-5 pb-6 sm:pb-8">
                            <div className="flex items-center justify-between">
                                <div>
                                    <Image
                                        className="h-8 w-auto"
                                        width={8}
                                        height={32}
                                        src={'/icon.svg'}
                                        alt={'WEDO logo'}
                                        unoptimized
                                    />
                                </div>
                                <div className="-mr-2">
                                    <Popover.Button className="inline-flex items-center justify-center rounded-md bg-white p-2 text-gray-400 hover:bg-gray-100 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-blue-500">
                                        <span className="sr-only">
                                            <Trans>Close menu</Trans>
                                        </span>
                                        <FontAwesomeIcon icon={faXmark} className="h-6 w-6" aria-hidden="true" />
                                    </Popover.Button>
                                </div>
                            </div>
                            <div className="mt-6 sm:mt-8">
                                <div className="text-lg">
                                    <Link
                                        href={'/'}
                                        className="flex items-center font-medium text-gray-900 hover:bg-gray-50"
                                    >
                                        <Trans>Home</Trans>
                                    </Link>
                                </div>
                                <MobileMenuGroup title={t`Product`} links={features} />
                                <MobileMenuGroup title={t`Use case`} links={useCases} />
                                <MobileMenuGroup title={t`Industries`} links={industries} />
                                <MobileMenuGroup title={t`Company`} links={company} />
                                <MobileMenuGroup title={t`Resources`} links={resources} />
                                <div className="mt-3 text-lg">
                                    <Link
                                        href={'/pricing'}
                                        className="flex items-center py-3 font-medium text-gray-900 hover:bg-gray-50"
                                    >
                                        <Trans>Pricing</Trans>
                                    </Link>
                                </div>
                            </div>
                            <div className="mt-6 flex items-center justify-center">
                                <Link href={'https://login.wedo.swiss'} passHref legacyBehavior>
                                    <Button className="whitespace-nowrap" variant={'secondary'}>
                                        <Trans>Sign in</Trans>
                                    </Button>
                                </Link>
                                <Link href={'/signup'} passHref legacyBehavior>
                                    <Button className="ml-3 whitespace-nowrap shadow-sm">
                                        <Trans>Free trial</Trans>
                                    </Button>
                                </Link>
                                <Link href={'/demo'} passHref legacyBehavior>
                                    <Button className="ml-3 whitespace-nowrap bg-blue-700 shadow-sm hover:bg-blue-500">
                                        <Trans>Live demo</Trans>
                                    </Button>
                                </Link>
                            </div>
                        </div>
                    </div>
                </Popover.Panel>
            </Transition>
        </Popover>
    );
}
