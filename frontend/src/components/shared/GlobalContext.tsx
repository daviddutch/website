import { createContext } from 'react';
import { fr, enUS, de, enGB, frCH } from 'date-fns/locale';

export const getDateFnsLocale = (locale: string): Locale => {
    switch (locale) {
        case 'fr':
        case 'fr-fr':
            return fr;
        case 'fr-ch':
            return frCH;
        case 'en':
        case 'en-gb':
            return enGB;
        case 'en-us':
            return enUS;
        case 'de':
        case 'de-de':
        case 'de-ch':
            return de;
        default:
            return enUS;
    }
};

export const GlobalContext = createContext<{
    locale?: Locale;
    translatedPaths?: { [key: string]: string };
}>({});

export const GlobalProvider = ({
    locale,
    translatedPaths,
    children,
}: {
    locale: Locale;
    translatedPaths: { [key: string]: string };
    children: React.ReactNode;
}): JSX.Element => <GlobalContext.Provider value={{ locale, translatedPaths }}>{children}</GlobalContext.Provider>;
