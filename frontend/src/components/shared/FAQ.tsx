import { Disclosure } from '@headlessui/react';
import clsx from 'clsx';
import { Trans } from '@lingui/macro';
import { MarkdownContent } from '@/components/shared/MarkdownContent';
import { i18n } from '@lingui/core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCircleChevronRight } from '@fortawesome/pro-solid-svg-icons';

export interface FaqEntry {
    question: string;
    answer: string;
}

export function FAQ({ entries }: { entries: FaqEntry[] }) {
    return (
        <div className="bg-white">
            <div className="mx-auto max-w-7xl py-12 px-4 sm:py-16 sm:px-6 lg:px-8">
                <div className="mx-auto max-w-3xl">
                    <h2 className="text-center text-3xl font-bold tracking-tight text-gray-900 sm:text-4xl">
                        <Trans>Frequently asked questions</Trans>
                    </h2>
                    <dl className="mt-12 space-y-3">
                        {entries.map((faq) => (
                            <Disclosure as="div" key={faq.question}>
                                {({ open }) => (
                                    <div className="rounded-xl bg-gray-100 hover:bg-gray-200">
                                        <dt className="text-lg">
                                            <Disclosure.Button className="flex w-full items-start justify-between p-6 text-left text-gray-400">
                                                <span className="font-medium text-gray-900">
                                                    <Trans id={faq.question} />
                                                </span>
                                                <span className="ml-6 flex h-7 items-center">
                                                    <FontAwesomeIcon
                                                        className={clsx(
                                                            open ? 'text-blue-500' : 'text-gray-300',
                                                            open ? 'rotate-90' : 'rotate-0',
                                                            'h-10 w-10 transform'
                                                        )}
                                                        aria-hidden="true"
                                                        icon={faCircleChevronRight}
                                                    />
                                                </span>
                                            </Disclosure.Button>
                                        </dt>
                                        <Disclosure.Panel as="dd" className="mt-2 pl-6 pr-6 pb-6">
                                            <div className="text-justify text-base text-gray-500">
                                                <MarkdownContent>{i18n._(faq.answer)}</MarkdownContent>
                                            </div>
                                        </Disclosure.Panel>
                                    </div>
                                )}
                            </Disclosure>
                        ))}
                    </dl>
                </div>
            </div>
        </div>
    );
}
