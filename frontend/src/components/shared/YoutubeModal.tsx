import React from 'react';
import { Dialog } from '@headlessui/react';
import { YoutubePlayer } from '@/components/shared/YoutubePlayer';

export const YoutubeModal = ({ isOpen, onClose }: { isOpen: boolean; onClose: () => void }) => (
    <Dialog open={isOpen} onClose={() => onClose()} className="relative z-50">
        <div className="fixed inset-0 bg-black/40" aria-hidden="true" />

        <div className="fixed inset-0 flex items-center justify-center p-4">
            <Dialog.Panel className="mx-auto h-[664px] w-[1180px] rounded bg-white">
                <YoutubePlayer />
            </Dialog.Panel>
        </div>
    </Dialog>
);
