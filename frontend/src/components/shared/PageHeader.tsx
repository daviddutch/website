export function PageHeader({
    title,
    description,
    sectionName,
}: {
    title: string;
    description: string;
    sectionName?: string;
}) {
    return (
        <div className="bg-white">
            <div className="mx-auto max-w-7xl py-10 px-4 sm:py-16 sm:px-6 lg:px-8">
                <div className="text-center">
                    {sectionName && <p className="text-lg font-semibold text-blue-600">{sectionName}</p>}
                    {title && (
                        <h1 className="mt-1 bg-gradient-to-r from-blue-400 to-blue-700 bg-clip-text bg-clip-text py-3 text-4xl font-bold font-extrabold tracking-tight text-gray-900 text-transparent sm:text-5xl lg:text-6xl">
                            {title}
                        </h1>
                    )}
                    {description && <p className="mx-auto mt-5 max-w-xl text-xl text-gray-500">{description}</p>}
                </div>
            </div>
        </div>
    );
}
