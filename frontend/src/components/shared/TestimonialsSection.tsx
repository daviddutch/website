import { Testimonial } from '@/types/LandingPage';
import { StrapiImage } from '@/components/shared/StrapiImage';
import { Container } from '@/components/shared/Container';

function TestimonialsDuo({ testimonials }: { testimonials: Testimonial[] }) {
    return (
        <section className="bg-gray-50">
            <div className="mx-auto max-w-7xl md:grid md:grid-cols-2 md:px-6 lg:px-8">
                {testimonials.map((testimonial, index) => (
                    <div
                        key={index}
                        className="py-12 px-4 sm:px-6 md:flex md:flex-col md:py-16 md:pl-0 md:pr-10 lg:pr-16"
                    >
                        <div className="flex-shrink-0">
                            {testimonial.attributes.logo.data.attributes.logoColor.data && (
                                <StrapiImage
                                    media={testimonial.attributes.logo.data.attributes.logoColor}
                                    className="h-12 max-w-min"
                                />
                            )}
                        </div>
                        <blockquote className="mt-6 md:flex md:flex-grow md:flex-col">
                            <div className="relative text-lg font-medium text-blue-800 md:flex-grow">
                                <svg
                                    className="absolute top-0 left-0 h-20 w-20 -translate-x-5 -translate-y-4 transform text-gray-200"
                                    fill="currentColor"
                                    viewBox="0 0 32 32"
                                    aria-hidden="true"
                                >
                                    <path d="M9.352 4C4.456 7.456 1 13.12 1 19.36c0 5.088 3.072 8.064 6.624 8.064 3.36 0 5.856-2.688 5.856-5.856 0-3.168-2.208-5.472-5.088-5.472-.576 0-1.344.096-1.536.192.48-3.264 3.552-7.104 6.624-9.024L9.352 4zm16.512 0c-4.8 3.456-8.256 9.12-8.256 15.36 0 5.088 3.072 8.064 6.624 8.064 3.264 0 5.856-2.688 5.856-5.856 0-3.168-2.304-5.472-5.184-5.472-.576 0-1.248.096-1.44.192.48-3.264 3.456-7.104 6.528-9.024L25.864 4z" />
                                </svg>
                                <p className="relative">{testimonial.attributes.content}</p>
                            </div>
                            <footer className="mt-8">
                                <div className="flex items-start">
                                    <div className="inline-flex flex-shrink-0 rounded-full border-2 border-white">
                                        <StrapiImage
                                            media={testimonial.attributes.photo}
                                            className="h-12 w-12 rounded-full"
                                        />
                                    </div>
                                    <div className="ml-4">
                                        <div className="text-base font-medium text-blue-800">
                                            {testimonial.attributes.author}
                                        </div>
                                        <div className="text-base font-medium text-gray-500">
                                            {testimonial.attributes.title}, {testimonial.attributes.company}
                                        </div>
                                    </div>
                                </div>
                            </footer>
                        </blockquote>
                    </div>
                ))}
            </div>
        </section>
    );
}

function QuoteIcon(props: any) {
    return (
        <svg aria-hidden="true" width={105} height={78} {...props}>
            <path d="M25.086 77.292c-4.821 0-9.115-1.205-12.882-3.616-3.767-2.561-6.78-6.102-9.04-10.622C1.054 58.534 0 53.411 0 47.686c0-5.273.904-10.396 2.712-15.368 1.959-4.972 4.746-9.567 8.362-13.786a59.042 59.042 0 0 1 12.43-11.3C28.325 3.917 33.599 1.507 39.324 0l11.074 13.786c-6.479 2.561-11.677 5.951-15.594 10.17-3.767 4.219-5.65 7.835-5.65 10.848 0 1.356.377 2.863 1.13 4.52.904 1.507 2.637 3.089 5.198 4.746 3.767 2.41 6.328 4.972 7.684 7.684 1.507 2.561 2.26 5.5 2.26 8.814 0 5.123-1.959 9.19-5.876 12.204-3.767 3.013-8.588 4.52-14.464 4.52Zm54.24 0c-4.821 0-9.115-1.205-12.882-3.616-3.767-2.561-6.78-6.102-9.04-10.622-2.11-4.52-3.164-9.643-3.164-15.368 0-5.273.904-10.396 2.712-15.368 1.959-4.972 4.746-9.567 8.362-13.786a59.042 59.042 0 0 1 12.43-11.3C82.565 3.917 87.839 1.507 93.564 0l11.074 13.786c-6.479 2.561-11.677 5.951-15.594 10.17-3.767 4.219-5.65 7.835-5.65 10.848 0 1.356.377 2.863 1.13 4.52.904 1.507 2.637 3.089 5.198 4.746 3.767 2.41 6.328 4.972 7.684 7.684 1.507 2.561 2.26 5.5 2.26 8.814 0 5.123-1.959 9.19-5.876 12.204-3.767 3.013-8.588 4.52-14.464 4.52Z" />
        </svg>
    );
}

function TestimonialCard({ testimonial }: { testimonial: Testimonial }) {
    return (
        <li key={testimonial.id}>
            <figure className="relative rounded-2xl bg-white p-6 shadow-xl shadow-slate-900/10">
                <QuoteIcon className="absolute top-6 left-6 fill-slate-100" />
                <blockquote className="relative">
                    <p className="text-lg tracking-tight text-slate-900">{testimonial.attributes.content}</p>
                </blockquote>
                <figcaption className="relative mt-6 flex items-center justify-between border-t border-slate-100 pt-6">
                    <div>
                        <div className="font-display text-base text-slate-900">{testimonial.attributes.author}</div>
                        <div className="mt-1 text-sm text-slate-500">
                            {testimonial.attributes.title} | {testimonial.attributes.company}
                        </div>
                    </div>
                    <div className="ml-2 -mr-2 flex-shrink-0 overflow-hidden rounded-full bg-slate-50">
                        <StrapiImage media={testimonial.attributes.photo} className="h-14 w-14 object-cover" />
                    </div>
                </figcaption>
            </figure>
        </li>
    );
}

function sliceIntoChunks(arr: Testimonial[], chunkSize: number) {
    const res = [];
    for (let i = 0; i < arr.length; i += chunkSize) {
        const chunk = arr.slice(i, i + chunkSize);
        res.push(chunk);
    }
    return res;
}

export function TestimonialCards({ testimonials }: { testimonials: Testimonial[] }) {
    const rows = sliceIntoChunks(testimonials, 2);
    return (
        <section id="testimonials" aria-label="What our customers are saying" className="bg-slate-50 py-20 sm:py-32">
            <Container>
                <ul
                    role="list"
                    className="mx-auto grid max-w-2xl grid-cols-1 gap-6 sm:gap-8 lg:max-w-none lg:grid-cols-3"
                >
                    {rows.map((row, rowIndex) => (
                        <li key={rowIndex}>
                            <ul role="list" className="flex flex-col gap-y-6 sm:gap-y-8">
                                {row.map((testimonial: Testimonial) => (
                                    <TestimonialCard key={testimonial.id} testimonial={testimonial} />
                                ))}
                            </ul>
                        </li>
                    ))}
                </ul>
            </Container>
        </section>
    );
}

export function Testimonials({ testimonials }: { testimonials: Testimonial[] }) {
    if (testimonials.length === 2) {
        return <TestimonialsDuo testimonials={testimonials} />;
    }
    return <TestimonialCards testimonials={testimonials} />;
}
