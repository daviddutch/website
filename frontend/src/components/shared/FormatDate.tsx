import { useContext } from 'react';
import { format } from 'date-fns';
import { GlobalContext } from '@/components/shared/GlobalContext';

export type DateLike = Date | number | string;

export const useFormatDate = (date: DateLike, dateFormat: string, locale?: Locale): string => {
    const context = useContext(GlobalContext);
    const formatLocale = locale || context.locale;
    return format(new Date(date), dateFormat, { locale: formatLocale });
};

export type FormatDateProps = {
    format: string;
    date: DateLike;
    locale?: Locale;
    className?: string;
};

export const FormatDate = ({ date, format, locale, className }: FormatDateProps): JSX.Element => {
    const text = useFormatDate(date, format, locale);
    return (
        <time className={className} dateTime={new Date(date).toISOString()}>
            {text}
        </time>
    );
};
