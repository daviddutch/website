import { StrapiImage } from '@/components/shared/StrapiImage';
import { Logo } from '@/types/Logo';
import { LogosSectionType } from '@/types/LandingPage';
import clsx from 'clsx';

interface Props {
    logos: Logo[];
    title?: string;
    type: LogosSectionType;
    dark?: boolean;
}

const animations = {
    0: 'animate-[marquee_15s_linear_infinite]',
    10: 'animate-[marquee_25s_linear_infinite]',
    20: 'animate-[marquee_60s_linear_infinite]',
};
const animations2 = {
    0: 'animate-[marquee2_15s_linear_infinite]',
    10: 'animate-[marquee2_25s_linear_infinite]',
    20: 'animate-[marquee2_60s_linear_infinite]',
};

export function CustomerLogos({ logos, type, title, dark = false }: Props) {
    if (type === LogosSectionType.Grid) {
        return (
            <div className="bg-white">
                <div className="mx-auto max-w-7xl py-12 px-4 sm:px-6 lg:py-16 lg:px-8">
                    {title && <p className="text-center text-lg font-semibold text-gray-600">{title}</p>}
                    <div className="mt-6 grid grid-cols-2 gap-0.5 md:grid-cols-4 lg:mt-8">
                        {logos.map((logo) => (
                            <div key={logo.id} className="col-span-1 flex justify-center bg-gray-50 py-8 px-8">
                                <StrapiImage
                                    className="max-h-12"
                                    media={logo.attributes.logoColor}
                                    width={120}
                                    height={48}
                                />
                            </div>
                        ))}
                    </div>
                </div>
            </div>
        );
    }

    return (
        <div className={clsx('mb-20 py-10', dark ? 'bg-blue-900' : 'bg-white')}>
            <div className="mx-auto max-w-7xl px-4 sm:px-6 lg:px-8">
                {title && <p className="pb-4 pt-6 text-center text-lg font-semibold text-gray-400">{title}</p>}
            </div>
            <div className={clsx('relative flex overflow-x-hidden', dark ? 'text-white' : '')}>
                <div
                    className={clsx(
                        animations[Math.min(logos.length - (logos.length % 10), 20) as keyof typeof animations],
                        `whitespace-nowrap py-12`
                    )}
                >
                    {logos.map((logo) => (
                        <div key={logo.id} className="mx-12 inline-block w-32 text-4xl">
                            <StrapiImage
                                className="h-20"
                                media={dark ? logo.attributes.logoDark : logo.attributes.logoLight}
                                width={120}
                                height={48}
                                loading={'eager'}
                            />
                        </div>
                    ))}
                </div>

                <div
                    className={clsx(
                        animations2[Math.min(logos.length - (logos.length % 10), 20) as keyof typeof animations2],
                        `absolute top-0 whitespace-nowrap py-12`,
                        dark ? 'text-white' : ''
                    )}
                >
                    {logos.map((logo) => (
                        <div key={logo.id} className="mx-12 inline-block w-32 text-4xl text-red-300">
                            <StrapiImage
                                className="h-20"
                                media={dark ? logo.attributes.logoDark : logo.attributes.logoLight}
                                width={120}
                                height={48}
                                loading={'eager'}
                            />
                        </div>
                    ))}
                </div>
            </div>
        </div>
    );
}
