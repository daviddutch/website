import { StrapiMedia } from '@/utils/media';
import { Trans } from '@lingui/macro';
import { StrapiImage } from '@/components/shared/StrapiImage';
import Link from 'next/link';
import { Button } from '@/components/shared/Button';
import React from 'react';

export const HeroHeaderAngled = ({
    preTitle,
    title,
    text,
    image,
}: {
    preTitle: string;
    title: string;
    text: string;
    image: StrapiMedia;
}) => (
    <div className="relative overflow-hidden bg-gray-50">
        <div className="mx-auto max-w-7xl">
            <div className="relative z-10 bg-gray-50 pb-8 sm:pb-16 md:pb-20 lg:w-full lg:max-w-2xl lg:pb-28 xl:pb-32">
                <svg
                    className="absolute inset-y-0 right-0 hidden h-full w-48 translate-x-1/2 transform text-gray-50 lg:block"
                    fill="currentColor"
                    viewBox="0 0 100 100"
                    preserveAspectRatio="none"
                    aria-hidden="true"
                >
                    <polygon points="50,0 100,0 50,100 0,100" />
                </svg>

                <main className="mx-auto max-w-7xl px-4 pt-10 sm:px-6 sm:pt-12 md:pt-16 lg:px-8 lg:pt-20 xl:pt-28">
                    <div className="sm:text-center lg:text-left">
                        <h1 className="text-4xl font-bold tracking-tight text-gray-900 sm:text-5xl md:text-6xl">
                            <span className="block text-blue-800 xl:inline">
                                <Trans id={preTitle} />
                            </span>{' '}
                            <span className="block bg-gradient-to-r from-blue-500 to-blue-600 bg-clip-text pb-3 text-gray-900 text-transparent xl:inline">
                                {title}
                            </span>
                        </h1>
                        <p className="mt-3 text-base text-gray-500 sm:mx-auto sm:mt-5 sm:max-w-xl sm:text-lg md:mt-5 md:text-xl lg:mx-0">
                            {text}
                        </p>
                        <div className="mt-5 flex items-center justify-center gap-5 sm:mt-8 sm:inline-flex lg:justify-start">
                            <Link href={'/demo'} passHref legacyBehavior>
                                <Button className="w-full" size={'large'}>
                                    <Trans>Live demo</Trans>
                                </Button>
                            </Link>
                            <Link href={'/signup'} passHref legacyBehavior>
                                <Button className="w-full" variant={'secondary'} size={'large'}>
                                    <Trans>Get started</Trans>
                                </Button>
                            </Link>
                        </div>
                    </div>
                </main>
            </div>
        </div>
        <div className="lg:absolute lg:inset-y-0 lg:right-0 lg:w-1/2">
            <StrapiImage media={image} className="h-56 w-full object-cover sm:h-72 md:h-96 lg:h-full lg:w-full" />
            <div className="absolute inset-0 bg-gray-300 mix-blend-multiply" />
        </div>
    </div>
);
