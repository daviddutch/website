import {
    BlogSection,
    CardSection,
    CtaSection,
    GridSection,
    LandingPageSection,
    LeftRightSection,
    LogosSection,
    SectionComponentType,
    SingleSection,
    TestimonialsSection,
} from '@/types/LandingPage';
import { CustomerLogos } from '@/components/shared/CustomerLogos';
import { BlogHighlightSection } from '@/components/shared/BlogHighlightSection';
import { CallToAction } from '@/components/cta/CallToAction';
import { Highlight } from '@/components/shared/HighlightsLeftRight';
import { HighlightSingle } from '@/components/shared/HighlightSingle';
import { HighlightsGrid } from '@/components/shared/HighlightsGrid';
import { HighlightsCard } from '@/components/shared/HighlightsCard';
import { Testimonials } from '@/components/shared/TestimonialsSection';

export const LandingPageContent = ({
    content,
    isHome = false,
}: {
    content: LandingPageSection[];
    isHome?: boolean;
}) => {
    let hlIndex = 0;
    return (
        <>
            {content.map((content, index) => {
                if (content.__component === SectionComponentType.LogosSection) {
                    const section = content as LogosSection;
                    return (
                        <CustomerLogos
                            key={index}
                            logos={section.logos.data}
                            title={section.title}
                            type={section.type}
                            dark={isHome}
                        />
                    );
                }
                if (content.__component === SectionComponentType.BlogSection) {
                    const section = content as BlogSection;
                    return (
                        <BlogHighlightSection
                            key={index}
                            title={section.title}
                            description={section.description}
                            posts={section.articles.data}
                        />
                    );
                }
                if (content.__component === SectionComponentType.CtaSection) {
                    const section = content as CtaSection;
                    return <CallToAction key={index} type={section.type} />;
                }
                if (content.__component === SectionComponentType.LeftRight) {
                    const section = content as LeftRightSection;
                    return <Highlight key={index} index={hlIndex++} {...section} />;
                }
                if (content.__component === SectionComponentType.Single) {
                    const section = content as SingleSection;
                    return (
                        <HighlightSingle
                            key={index}
                            title={section.title}
                            description={section.description}
                            image={section.image}
                        />
                    );
                }
                if (content.__component === SectionComponentType.Grid) {
                    const section = content as GridSection;
                    return (
                        <HighlightsGrid
                            key={index}
                            title={section.title}
                            description={section.description}
                            highlights={section.highlights.data}
                        />
                    );
                }
                if (content.__component === SectionComponentType.Card) {
                    const section = content as CardSection;
                    return <HighlightsCard key={index} highlights={section.highlights.data} />;
                }
                if (content.__component === SectionComponentType.TestimonialsSection) {
                    const section = content as TestimonialsSection;
                    return <Testimonials key={index} testimonials={section.testimonials.data} />;
                }
                return (
                    <div key={index}>
                        {content.__component}
                        <pre className="mx-auto w-3/5 whitespace-pre-line bg-gray-200">{JSON.stringify(content)}</pre>
                    </div>
                );
            })}
        </>
    );
};
