import { StrapiImage } from '@/components/shared/StrapiImage';
import { StrapiMedia } from '@/utils/media';
import clsx from 'clsx';

const plays = [
    'lg:before:bg-play-blue',
    'lg:before:bg-play-purple',
    'lg:before:bg-play-green',
    'lg:before:bg-play-yellow',
    // 'lg:before:bg-play-orange',
    'lg:before:bg-play-red',
];

export const Highlight = ({
    title,
    description,
    image,
    position,
    index,
}: {
    title: string;
    description: string;
    image: StrapiMedia;
    position: 'left' | 'right';
    index: number;
}) => {
    return (
        <div className="overflow-hidden bg-white">
            <div className="relative mx-auto max-w-7xl py-12 px-4 sm:px-6 lg:px-8">
                <div
                    className={clsx(
                        'flex gap-5 lg:gap-20',
                        "before:z-10 lg:before:absolute lg:before:top-[10%] lg:before:h-[400px] lg:before:w-[400px] lg:before:bg-contain lg:before:bg-left-top lg:before:bg-no-repeat lg:before:content-['']",
                        position === 'right' ? 'lg:flex-row-reverse' : 'lg:flex-row',
                        position === 'right'
                            ? 'lg:before:right-[75%] lg:before:rotate-[10deg]'
                            : 'lg:before:left-[75%] lg:before:rotate-[170deg]',
                        plays[index % plays.length],
                        'flex-col'
                    )}
                >
                    <div className="flex-[45] px-4 sm:px-6 lg:py-16 lg:px-0">
                        <div>
                            <div>
                                <h2 className="font-serif text-6xl font-bold tracking-tight text-blue-800">{title}</h2>
                                <p className="mt-6 text-2xl text-gray-500">{description}</p>
                            </div>
                        </div>
                        {/*<div className="mt-4 border-t border-gray-200 pt-6">*/}
                        {/*    <blockquote>*/}
                        {/*        <div>*/}
                        {/*            <p className="text-base italic text-gray-500">*/}
                        {/*                &ldquo;Cras velit quis eros eget rhoncus lacus ultrices sed diam. Sit orci risus*/}
                        {/*                aenean curabitur donec aliquet. Mi venenatis in euismod ut.&rdquo;*/}
                        {/*            </p>*/}
                        {/*        </div>*/}
                        {/*        <footer className="mt-3">*/}
                        {/*            <div className="flex items-center space-x-3">*/}
                        {/*                <div className="flex-shrink-0">*/}
                        {/*                    <img*/}
                        {/*                        className="h-6 w-6 rounded-full"*/}
                        {/*                        src="https://images.unsplash.com/photo-1509783236416-c9ad59bae472?ixlib=rb-=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=8&w=1024&h=1024&q=80"*/}
                        {/*                        alt=""*/}
                        {/*                    />*/}
                        {/*                </div>*/}
                        {/*                <div className="text-base font-medium text-gray-500">*/}
                        {/*                    Marcia Hill, Digital Marketing Manager*/}
                        {/*                </div>*/}
                        {/*            </div>*/}
                        {/*        </footer>*/}
                        {/*    </blockquote>*/}
                        {/*</div>*/}
                    </div>
                    <div
                        className={clsx(
                            'relative flex flex-[55] items-center',
                            position === 'right' ? 'lg:ml-[-12px]' : 'lg:mr-[-12px]',
                            "lg:before:absolute lg:before:z-0 lg:before:ml-[-50px] lg:before:h-full lg:before:w-full lg:before:bg-dotted lg:before:bg-dotted-20 lg:before:bg-dotted-10 lg:before:content-['']",
                            'lg:after:absolute lg:after:inset-0 lg:after:ml-[-50px] lg:after:h-full lg:after:w-full lg:after:shadow-dotted-fade'
                        )}
                    >
                        <StrapiImage media={image} className="relative z-20 w-full" alt={title} />
                        {/*<object*/}
                        {/*    type={'image/svg+xml'}*/}
                        {/*    data={image.data.attributes.url}*/}
                        {/*    className="relative z-20 w-full"*/}
                        {/*    alt={title}*/}
                        {/*/>*/}
                    </div>
                </div>
            </div>
        </div>
    );
};
