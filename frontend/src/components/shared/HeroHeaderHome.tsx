import { Trans } from '@lingui/macro';
import { Button } from '@/components/shared/Button';
import { YoutubeModal } from '@/components/shared/YoutubeModal';
import React, { useState } from 'react';
import { StrapiMedia } from '@/utils/media';
import { StrapiImage } from '@/components/shared/StrapiImage';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCirclePlay } from '@fortawesome/pro-solid-svg-icons';

export function HeroHeaderHome({ title, text, image }: { title: string; text: string; image: StrapiMedia }) {
    let [isOpen, setIsOpen] = useState(false);
    return (
        <>
            <div className="relative overflow-hidden bg-white">
                <div
                    className="h-full w-1/3 shadow-dotted-fade lg:absolute lg:right-0 lg:h-1/3 lg:bg-dotted lg:bg-dotted-20 lg:bg-dotted-10"
                    aria-hidden="true"
                ></div>
                <div
                    className="h-full w-1/3 shadow-dotted-fade lg:absolute lg:top-1/3 lg:left-0 lg:bg-dotted lg:bg-dotted-20 lg:bg-dotted-10"
                    aria-hidden="true"
                ></div>
                <div className="relative pt-6">
                    <main className="mx-auto mt-16 px-4 sm:mt-24 sm:px-6">
                        <div className="mx-auto max-w-3xl">
                            <div className="sm:text-center md:mx-auto md:max-w-7xl lg:col-span-6">
                                <h1>
                                    <span className="mt-1 block text-4xl font-extrabold tracking-tight sm:text-5xl xl:text-7xl">
                                        <span className="block bg-gradient-to-r from-blue-400 to-blue-700 bg-clip-text pb-3 text-center text-gray-900 text-transparent">
                                            {title}
                                        </span>
                                    </span>
                                </h1>
                                <p className="mt-3 text-base text-gray-500 sm:mt-5 sm:text-xl lg:text-lg xl:text-xl">
                                    {text}
                                </p>
                            </div>
                            <div className="mt-12 flex items-center justify-center text-center">
                                <Button onClick={() => setIsOpen(true)} variant={'white'} size={'large'}>
                                    <FontAwesomeIcon icon={faCirclePlay} className="mr-2 text-gray-600" />
                                    <Trans>Discover WEDO in 1 minute</Trans>
                                </Button>
                            </div>
                        </div>
                        <div className="absolute top-[65%] left-0 h-1/2 w-full bg-blue-900"></div>
                        <div className="relative overflow-hidden pt-10">
                            <div className="mx-auto max-w-md px-4 text-center sm:max-w-3xl sm:px-6 lg:max-w-7xl lg:px-8">
                                <div className="mt-12">
                                    <StrapiImage
                                        media={image}
                                        className="rounded-lg shadow-xl ring-1 ring-black ring-opacity-5"
                                        loading={'eager'}
                                    />
                                </div>
                            </div>
                        </div>
                    </main>
                </div>
            </div>
            <YoutubeModal isOpen={isOpen} onClose={() => setIsOpen(false)} />
        </>
    );
}
