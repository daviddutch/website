import React from 'react';
import clsx from 'clsx';

const classes = {
    base: 'whitespace-nowrap inline-flex items-center justify-center rounded-md focus:outline-none transition ease-in-out duration-300',
    withShadow: 'shadow-md',
    disabled: 'opacity-50 cursor-not-allowed',
    pill: 'rounded-full',
    size: {
        small: 'px-2 py-2 text-sm',
        normal: 'px-4 py-2 text-base font-medium',
        large: 'px-8 py-3 text-xl font-medium',
    },
    variant: {
        primary:
            'no-underline border border-blue-500 bg-blue-gradient text-white hover:transition-colors hover:border-blue-500 hover:bg-purple-blue-gradient',
        secondary:
            'no-underline border border-blue-100 bg-blue-100 text-blue-700 hover:bg-blue-200 hover:border-blue-200',
        white: 'border border-gray-200 bg-white text-gray-700 hover:text-gray-800 hover:bg-gradient-to-r hover:from-gray-50 hover:to-gray-200',
        dark: 'border border-blue-800 bg-blue-800 text-white hover:text-gray-200 hover:bg-blue-700',
        link: 'bg-white text-blue-700 hover:bg-blue-100',
        danger: 'bg-red-500 hover:bg-red-800 focus:ring-2 focus:ring-red-500 focus:ring-opacity-50 text-white',
        ghost: '',
    },
};

type Props = {
    children: React.ReactNode;
    type?: 'submit' | 'button';
    className?: string;
    href?: string;
    pill?: boolean;
    disabled?: boolean;
    withShadow?: boolean;
    variant?: 'primary' | 'secondary' | 'danger' | 'white' | 'dark' | 'link' | 'ghost';
    size?: 'small' | 'normal' | 'large';
    onClick?: () => void;
};

const ButtonTag = `button`;
const ATag = `a`;
export const Button = React.forwardRef<HTMLButtonElement & HTMLAnchorElement, Props>(
    (
        {
            children,
            type = 'button',
            className,
            variant = 'primary',
            size = 'normal',
            pill,
            disabled = false,
            withShadow = true,
            href,
            ...props
        }: Props,
        ref
    ) => {
        const Tag = href ? ATag : ButtonTag;
        return (
            <Tag
                ref={ref}
                disabled={disabled}
                type={!href ? type : undefined}
                href={href}
                className={clsx(
                    classes.base,
                    classes.size[size],
                    classes.variant[variant],
                    withShadow && classes.withShadow,
                    pill && classes.pill,
                    disabled && classes.disabled,
                    className
                )}
                {...props}
            >
                {children}
            </Tag>
        );
    }
);
Button.displayName = 'Button';
