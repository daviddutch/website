import ReactMarkdown from 'react-markdown';
import { getStrapiMedia } from '@/utils/media';
import React from 'react';
import Image from 'next/image';
import remarkGfm from 'remark-gfm';
import remarkDirective from 'remark-directive';
import remarkDirectiveRehype from 'remark-directive-rehype';
import { Button } from '@/components/shared/Button';
import { YoutubePlayer } from '@/components/shared/YoutubePlayer';
import Link from 'next/link';

const ButtonPrimary = ({ link, children }: { link: string; children: React.ReactNode }) => {
    return (
        <div className="my-4 text-center">
            <Button href={link}>{children}</Button>
        </div>
    );
};
const ButtonSecondary = ({ link, children }: { link: string; children: React.ReactNode }) => (
    <div className="my-4 text-center">
        <Button href={link} variant="secondary">
            {children}
        </Button>
    </div>
);
const YouTubeVideo = ({ id }: { id: string }) => <YoutubePlayer videoId={id} />;

export function MarkdownContent({ children }: { children: string }) {
    return (
        <ReactMarkdown
            className={'prose prose-lg prose-blue text-gray-600'}
            remarkPlugins={[remarkDirective, remarkDirectiveRehype, remarkGfm]}
            components={{
                // @ts-ignore
                'youtube-video': YouTubeVideo,
                'button-primary': ButtonPrimary,
                'button-secondary': ButtonSecondary,
                p: ({ node, ...props }: { node: any }) => {
                    return <p className="whitespace-pre-line" {...props} />;
                },
                img: ({ node, ...props }: { node: any }) => {
                    const { alt, src } = props as { alt: string; src: string };
                    return (
                        <Image
                            alt={alt}
                            src={src ? getStrapiMedia({ data: { attributes: { url: src } } }) : ''}
                            width={800}
                            height={800}
                            {...props}
                        />
                    );
                },
                ul: ({ node, ...props }: { node: any }) => {
                    return <ul className={'list-disc whitespace-normal'} {...props} />;
                },
                ol: ({ node, ...props }: { node: any }) => {
                    return <ul className={'list-decimal whitespace-normal'} {...props} />;
                },
                u: ({ node, ...props }: { node: any }) => {
                    return <u className={'underline'} {...props} />;
                },
                a: ({ node, ...props }: { node: any }) => {
                    // @ts-ignore
                    return <Link {...props} />;
                },
            }}
        >
            {children}
        </ReactMarkdown>
    );
}
