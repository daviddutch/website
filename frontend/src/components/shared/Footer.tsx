import Image from 'next/image';
import { I18nSwitcher } from '@/components/shared/I18nSwitcher';
import { t, Trans } from '@lingui/macro';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFacebook, faInstagram, faLinkedin, faTwitter, faYoutube } from '@fortawesome/free-brands-svg-icons';
import Link from 'next/link';
import { i18n } from '@lingui/core';

const navigation = {
    product: [
        { name: t`Security`, href: '/security' },
        { name: t`Meetings`, href: '/features/meetings' },
        { name: t`Tasks`, href: '/features/tasks' },
        { name: t`Checklists`, href: '/features/checklists' },
        { name: t`Project Management`, href: '/features/project-management' },
        { name: t`Updates`, href: t`/category/updates` },
    ],
    discover: [
        { name: t`Team Meetings`, href: '/use-cases/team-meetings' },
        { name: t`One-on-Ones`, href: '/use-cases/one-on-ones' },
        { name: t`Executive Committees`, href: '/use-cases/executive-committees' },
        { name: t`Board Meetings`, href: '/use-cases/board-meetings' },
        { name: t`Remote Meetings`, href: '/use-cases/board-meetings' },
        { name: t`Your company`, href: '/industries/sme' },
    ],
    getStarted: [
        { name: t`Schedule a demo`, href: '/demo' },
        { name: t`Free trial`, href: '/signup' },
        { name: t`Onboarding`, href: '/onboarding' },
        { name: t`Help center`, href: 'https://help.wedo.swiss/' },
        { name: t`Blog`, href: '/blog' },
        { name: t`Events`, href: '/events' },
    ],
    company: [
        { name: t`About Us`, href: '/about' },
        { name: t`Jobs`, href: '/jobs' },
        { name: t`Press`, href: t`/category/press` },
        { name: t`Privacy Policy`, href: '/privacy' },
        { name: t`News`, href: t`/category/news` },
        { name: t`Contact`, href: '/contact' },
    ],
    legal: [
        { name: t`Terms of Service`, href: '/terms' },
        { name: t`Privacy Policy`, href: '/privacy' },
        { name: t`SLA`, href: '/sla' },
        { name: t`DPA`, href: '/dpa' },
        { name: t`Status page`, href: 'https://wedo.statuspage.io/' },
    ],
    social: [
        {
            name: 'LinkedIn',
            href: 'https://www.linkedin.com/company/wedoswiss',
            icon: faLinkedin,
        },
        {
            name: 'Facebook',
            href: 'https://www.facebook.com/wedoswiss/',
            icon: faFacebook,
        },
        {
            name: 'Instagram',
            href: 'https://www.instagram.com/wedoswiss/',
            icon: faInstagram,
        },
        {
            name: 'Twitter',
            href: 'https://twitter.com/wedoswiss',
            icon: faTwitter,
        },
    ],
};

export function Footer() {
    return (
        <footer className="bg-blue-900" aria-labelledby="footer-heading">
            <div className="mx-auto max-w-7xl py-12 px-4 sm:px-6 lg:py-16 lg:px-8">
                <div className="mb-10 xl:grid xl:grid-cols-3 xl:gap-8">
                    <div className="space-y-8 xl:col-span-1">
                        <Image src={'/logo.svg'} alt={'Company logo'} height={168} width={168} />
                        <div className="text-base text-gray-400">
                            <p>
                                Boulevard de Pérolles 3<br />
                                1700 Fribourg
                                <br />
                                Switzerland
                            </p>
                            <br />
                            <p>
                                <Trans>WEDO is a registred trademark of WEDO Sàrl</Trans>
                            </p>
                            <p>Reg/VAT CHE-267.068.989</p>
                            <br />
                            <p>
                                <Trans>Developed & hosted in Switzerland</Trans>
                            </p>
                        </div>
                    </div>
                    <div className="mt-12 grid grid-cols-2 gap-8 xl:col-span-2 xl:mt-0">
                        <div className="md:grid md:grid-cols-2 md:gap-8">
                            <div>
                                <h3 className="text-base font-medium text-white">
                                    <Trans>Product</Trans>
                                </h3>
                                <ul role="list" className="mt-4 space-y-4">
                                    {navigation.product.map((item) => (
                                        <li key={item.name}>
                                            <Link href={item.href} className="text-base text-gray-400 hover:text-white">
                                                <Trans id={item.name} />
                                            </Link>
                                        </li>
                                    ))}
                                </ul>
                            </div>
                            <div className="mt-12 md:mt-0">
                                <h3 className="text-base font-medium text-white">
                                    <Trans>Discover</Trans>
                                </h3>
                                <ul role="list" className="mt-4 space-y-4">
                                    {navigation.discover.map((item) => (
                                        <li key={item.name}>
                                            <Link href={item.href} className="text-base text-gray-400 hover:text-white">
                                                <Trans id={item.name} />
                                            </Link>
                                        </li>
                                    ))}
                                </ul>
                            </div>
                        </div>
                        <div className="md:grid md:grid-cols-2 md:gap-8">
                            <div>
                                <h3 className="text-base font-medium text-white">
                                    <Trans>Get Started</Trans>
                                </h3>
                                <ul role="list" className="mt-4 space-y-4">
                                    {navigation.getStarted.map((item) => (
                                        <li key={item.name}>
                                            <Link href={item.href} className="text-base text-gray-400 hover:text-white">
                                                <Trans id={item.name} />
                                            </Link>
                                        </li>
                                    ))}
                                </ul>
                            </div>
                            <div className="mt-12 md:mt-0">
                                <h3 className="text-base font-medium text-white">
                                    <Trans>Company</Trans>
                                </h3>
                                <ul role="list" className="mt-4 space-y-4">
                                    {navigation.company.map((item) => (
                                        <li key={item.name}>
                                            <Link
                                                href={i18n._(item.href)}
                                                className="text-base text-gray-400 hover:text-white"
                                            >
                                                <Trans id={item.name} />
                                            </Link>
                                        </li>
                                    ))}
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="mt-8 flex flex-col items-center gap-8 border-t border-gray-700 pt-8 md:flex-row md:justify-between">
                    <div className="w-full max-w-xs">
                        <label htmlFor="language" className="sr-only">
                            <Trans>Language</Trans>
                        </label>
                        <I18nSwitcher />
                    </div>
                    <div className="flex space-x-6">
                        {navigation.social.map((item) => (
                            <Link
                                href={item.href}
                                key={item.href}
                                rel="external"
                                target="_blank"
                                className="text-gray-400 hover:text-gray-300"
                            >
                                <span className="sr-only">{item.name}</span>
                                <FontAwesomeIcon icon={item.icon} className="h-6 w-6" aria-hidden="true" />
                            </Link>
                        ))}
                        <Link
                            href={t`https://www.youtube.com/channel/UCUL9bTsch6P0XdpMAf8IUpQ`}
                            key={t`https://www.youtube.com/channel/UCUL9bTsch6P0XdpMAf8IUpQ`}
                            rel="external"
                            target="_blank"
                            className="text-gray-400 hover:text-gray-300"
                        >
                            <span className="sr-only">Youtube</span>
                            <FontAwesomeIcon icon={faYoutube} className="h-6 w-6" aria-hidden="true" />
                        </Link>
                    </div>
                </div>
                <div className="mt-8 flex flex-col justify-between gap-8 md:flex-row">
                    <p className="order-1 text-center text-base text-gray-400 md:order-2">
                        &copy; {new Date().getFullYear()} WEDO Sàrl. All rights reserved.
                    </p>
                    <nav className="flex flex-wrap justify-center gap-x-8 gap-y-2 " aria-label="Footer">
                        {navigation.legal.map((item) => (
                            <div key={item.name}>
                                <Link href={item.href} className="text-base text-gray-400 hover:text-white">
                                    <Trans id={item.name} />
                                </Link>
                            </div>
                        ))}
                    </nav>
                </div>
            </div>
        </footer>
    );
}
