import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
    faCheck,
    faClipboardList,
    faKeyboard,
    faQuestion,
    faRepeat,
    faUser,
    faUserGroup,
    faUsers,
    faArrowsToCircle,
    faBallotCheck,
    faBarsProgress,
    faBriefcase,
    faBuildingUser,
    faCalendarStar,
    faCheckDouble,
    faClipboardListCheck,
    faFileExport,
    faHeadSideBrain,
    faHospitalUser,
    faHourglassStart,
    faObjectsColumn,
    faPeopleGroup,
    faRectangleHistory,
    faSchool,
    faShieldHalved,
    faSpinnerThird,
    faUserCheck,
    faUsersBetweenLines,
    faUsersLine,
    faUsersMedical,
    faUsersRays,
    faUsersRectangle,
    faUserTie,
    faUserTieHair,
} from '@fortawesome/pro-duotone-svg-icons';
import { IconProp } from '@fortawesome/fontawesome-svg-core';
import { Icon } from '@/types/Icon';

const getIconMapping = (icon: Icon): IconProp => {
    switch (icon) {
        case Icon.arrowsToCircle:
            return faArrowsToCircle;
        case Icon.ballotCheck:
            return faBallotCheck;
        case Icon.barsProgress:
            return faBarsProgress;
        case Icon.briefcase:
            return faBriefcase;
        case Icon.buildingUser:
            return faBuildingUser;
        case Icon.calendarStar:
            return faCalendarStar;
        case Icon.checkDouble:
            return faCheckDouble;
        case Icon.clipboardListCheck:
            return faClipboardListCheck;
        case Icon.fileExport:
            return faFileExport;
        case Icon.headSideBrain:
            return faHeadSideBrain;
        case Icon.hospitalUser:
            return faHospitalUser;
        case Icon.hourglassStart:
            return faHourglassStart;
        case Icon.objectsColumn:
            return faObjectsColumn;
        case Icon.peopleGroup:
            return faPeopleGroup;
        case Icon.rectangleHistory:
            return faRectangleHistory;
        case Icon.school:
            return faSchool;
        case Icon.shieldHalved:
            return faShieldHalved;
        case Icon.spinnerThird:
            return faSpinnerThird;
        case Icon.userCheck:
            return faUserCheck;
        case Icon.usersBetweenLines:
            return faUsersBetweenLines;
        case Icon.usersLine:
            return faUsersLine;
        case Icon.usersMedical:
            return faUsersMedical;
        case Icon.usersRays:
            return faUsersRays;
        case Icon.usersRectangle:
            return faUsersRectangle;
        case Icon.userTie:
            return faUserTie;
        case Icon.userTieHair:
            return faUserTieHair;
        case Icon.check:
            return faCheck;
        case Icon.clipboardList:
            return faClipboardList;
        case Icon.keyboard:
            return faKeyboard;
        case Icon.repeat:
            return faRepeat;
        case Icon.user:
            return faUser;
        case Icon.users:
            return faUsers;
        case Icon.userGroup:
            return faUserGroup;
        default:
            return faQuestion;
    }
};

type Props = {
    icon: Icon;
    className: string;
};

export function StrapiIcon({ icon, className }: Props) {
    return <FontAwesomeIcon icon={getIconMapping(icon)} className={className} aria-hidden="true" />;
}
