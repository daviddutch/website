import { StrapiImage } from '@/components/shared/StrapiImage';
import { StrapiMedia } from '@/utils/media';
import { Trans } from '@lingui/macro';
import Link from 'next/link';
import { Button } from '@/components/shared/Button';
import React from 'react';

export function HeroHeaderRounded({ title, text, image }: { title: string; text: string; image: StrapiMedia }) {
    return (
        <div className="bg-white">
            {/* Hero card */}
            <div className="relative">
                <div className="absolute inset-x-0 bottom-0 h-1/2 bg-gray-200" />
                <div className="mx-auto sm:px-6 lg:px-8">
                    <div className="relative shadow-xl sm:overflow-hidden sm:rounded-2xl">
                        <div className="absolute inset-0">
                            <StrapiImage media={image} className="h-full w-full object-cover" />
                            <div className="absolute inset-0 bg-blue-700 mix-blend-multiply" />
                        </div>
                        <div className="relative px-4 py-16 sm:px-6 sm:py-24 lg:py-32 lg:px-8">
                            <h1 className="text-center text-4xl font-bold tracking-tight sm:text-5xl lg:text-6xl">
                                <span className="block text-white">
                                    <Trans>WEDO for</Trans>
                                </span>
                                <span className="mt-4 inline-block rounded-md bg-white px-4 py-2 text-blue-900">
                                    {title}
                                </span>
                            </h1>
                            <p className="mx-auto mt-6 max-w-lg text-center text-2xl text-white sm:max-w-3xl">{text}</p>
                            <div className="mx-auto mt-10 max-w-sm sm:flex sm:max-w-none sm:justify-center">
                                <div className="space-y-4 sm:mx-auto sm:inline-grid sm:grid-cols-2 sm:gap-5 sm:space-y-0">
                                    <Link href={'/signup'} passHref legacyBehavior>
                                        <Button variant={'white'} size={'large'}>
                                            <Trans>Get started</Trans>
                                        </Button>
                                    </Link>
                                    <Link href={'/demo'} passHref legacyBehavior>
                                        <Button variant={'primary'} size={'large'}>
                                            <Trans>Live demo</Trans>
                                        </Button>
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
