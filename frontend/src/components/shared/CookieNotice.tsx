import Image from 'next/image';
import cookieIcon from '@/assets/cookie.svg';
import { t, Trans } from '@lingui/macro';
import Link from 'next/link';
import { Button } from '@/components/shared/Button';
import React, { useEffect, useState } from 'react';
import { getCookie, setCookie } from 'cookies-next';
import { Dialog, Switch } from '@headlessui/react';
import clsx from 'clsx';

type CookieSwitchProps = {
    label: string;
    checked: boolean;
    disabled?: boolean;
    onChange: (checked: boolean) => void;
    children: React.ReactNode;
};
const CookieSwitch = ({ label, checked, onChange, children, disabled }: CookieSwitchProps) => {
    return (
        <Switch.Group as="div" className="flex items-center justify-between">
            <span className="flex flex-grow flex-col">
                <Switch.Label as="span" className="cursor-pointer text-sm font-medium text-gray-900">
                    {label}
                </Switch.Label>
                <Switch.Description as="span" className="text-sm text-gray-500">
                    {children}
                </Switch.Description>
            </span>
            <Switch
                checked={checked}
                onChange={onChange}
                disabled={disabled}
                className={clsx(
                    checked ? (disabled ? 'bg-blue-200' : 'bg-blue-500') : 'bg-gray-200',
                    'relative inline-flex h-6 w-11 flex-shrink-0 cursor-pointer rounded-full border-2 border-transparent transition-colors duration-200 ease-in-out focus:outline-none focus:ring-2 focus:ring-blue-500 focus:ring-offset-2 disabled:cursor-not-allowed'
                )}
            >
                <span
                    aria-hidden="true"
                    className={clsx(
                        checked ? 'translate-x-5' : 'translate-x-0',
                        'pointer-events-none inline-block h-5 w-5 transform rounded-full bg-white shadow ring-0 transition duration-200 ease-in-out'
                    )}
                />
            </Switch>
        </Switch.Group>
    );
};

export const CookieSettingsModal = ({
    isOpen,
    onClose,
}: {
    isOpen: boolean;
    onClose: ({ necessary, usage, marketing }: { necessary: boolean; usage: boolean; marketing: boolean }) => void;
}) => {
    const [necessary, setNecessary] = useState(true);
    const [usage, setUsage] = useState(false);
    const [marketing, setMarketing] = useState(false);

    if (!isOpen) {
        return null;
    }
    return (
        <Dialog open={isOpen} static onClose={() => {}} className="relative z-50">
            <div className="fixed inset-0 bg-black/40" aria-hidden="true" />

            <div className="fixed inset-0 flex items-center justify-center p-4">
                <Dialog.Panel className="w-full max-w-md rounded bg-white">
                    <Dialog.Title
                        as="h3"
                        className="border-b border-gray-200 p-6 pb-4 text-lg font-medium leading-6 text-gray-900"
                    >
                        <Trans>Cookie settings</Trans>
                    </Dialog.Title>
                    <div className="mt-2 space-y-4 p-6">
                        <CookieSwitch
                            checked={necessary}
                            label={t`Strictly necessary cookies`}
                            disabled={true}
                            onChange={setNecessary}
                        >
                            <Trans>Cookies to remember your cookie notice answer and your preferred language</Trans>
                        </CookieSwitch>
                        <CookieSwitch checked={usage} label={t`Cookies that measure website use`} onChange={setUsage}>
                            <Trans>Cookies that measure the website usage and performance</Trans>
                        </CookieSwitch>
                        <CookieSwitch
                            checked={marketing}
                            label={t`Cookies that help with our communications and marketing`}
                            onChange={setMarketing}
                        >
                            <Trans>Cookies that help us to improve our communications and marketing</Trans>
                        </CookieSwitch>
                    </div>
                    <div className="grid grid-cols-2 gap-5 p-5">
                        <Button
                            onClick={() => {
                                onClose({
                                    necessary,
                                    usage,
                                    marketing,
                                });
                            }}
                            className="w-full"
                            variant="white"
                        >
                            <Trans>Confirm choices</Trans>
                        </Button>
                        <Button
                            onClick={() => {
                                onClose({
                                    necessary: true,
                                    usage: true,
                                    marketing: true,
                                });
                            }}
                            className="w-full"
                        >
                            <Trans>Accept all</Trans>
                        </Button>
                    </div>
                </Dialog.Panel>
            </div>
        </Dialog>
    );
};

export const CookieNotice = () => {
    const [isOpen, setIsOpen] = useState(false);
    const [show, setShow] = useState(false);

    useEffect(() => {
        const cookie = getCookie('cookie_notice');
        if (!cookie) {
            setShow(true);
        }
    }, []);

    const handleAcceptCookies = ({
        necessary,
        usage,
        marketing,
    }: {
        necessary: boolean;
        usage: boolean;
        marketing: boolean;
    }) => {
        setCookie('cookie_notice', 'true', { maxAge: 60 * 60 * 24 * 365 });
        setCookie('cookie_necessary', necessary ? 'true' : 'false', { maxAge: 60 * 60 * 24 * 365 });
        setCookie('cookie_usage', usage ? 'true' : 'false', { maxAge: 60 * 60 * 24 * 365 });
        setCookie('cookie_marketing', marketing ? 'true' : 'false', { maxAge: 60 * 60 * 24 * 365 });
        setShow(false);
        setIsOpen(false);
    };

    if (!show) {
        return null;
    }

    return (
        <div className="fixed bottom-0 z-20 w-full border border-gray-900 bg-blue-900 p-6 shadow-md transition-all duration-300 ease-in-out sm:bottom-4 sm:left-4 sm:w-[600px] sm:rounded-lg">
            <div className="relative mx-auto -mt-12 mb-5 w-16">
                <Image src={cookieIcon} loading="eager" alt="Cookie Icon SVG" className="-mt-3" />
            </div>
            <div className="text-md mb-10 block w-full pt-4 leading-normal text-gray-100 ">
                <p>
                    <Trans>
                        We use our own cookies as well as third-party cookies on our websites to enhance your
                        experience, analyze our traffic, and for security and marketing.
                    </Trans>
                </p>
            </div>
            <div className="flex flex-col-reverse items-center justify-between gap-5 sm:flex-row">
                <Link className="mr-1 whitespace-nowrap text-sm text-gray-200 hover:text-gray-400" href="/privacy">
                    <Trans>Privacy Policy</Trans>
                </Link>
                <div className="flex flex-col gap-3 sm:flex-row">
                    <Button
                        variant={'ghost'}
                        size="small"
                        className="w-full text-gray-200 hover:text-gray-400"
                        onClick={() => setIsOpen(true)}
                    >
                        <Trans>Cookie settings</Trans>
                    </Button>
                    <Button
                        variant={'primary'}
                        size="small"
                        className="w-full"
                        onClick={() => handleAcceptCookies({ necessary: true, usage: true, marketing: true })}
                    >
                        <Trans>Accept cookies</Trans>
                    </Button>
                </div>
            </div>
            <CookieSettingsModal isOpen={isOpen} onClose={handleAcceptCookies} />
        </div>
    );
};
