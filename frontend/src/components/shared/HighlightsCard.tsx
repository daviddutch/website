import { Highlight, HighlightTemplate } from '@/types/LandingPage';
import { StrapiIcon } from '@/components/shared/StrapiIcon';
import clsx from 'clsx';

export const HighlightsCard = ({ highlights }: { highlights: (Highlight | HighlightTemplate)[] }) => {
    return (
        <div className="relative bg-white py-16 sm:py-24">
            <div className="mx-auto max-w-md px-4 sm:max-w-3xl sm:px-6 lg:max-w-7xl lg:px-8">
                <div
                    className={clsx(
                        'grid grid-cols-1 gap-8 sm:grid-cols-2',
                        highlights.length === 4 ? 'lg:grid-cols-4' : 'lg:grid-cols-3'
                    )}
                >
                    {highlights.map((highlight) => {
                        const attributes = 'attributes' in highlight ? highlight.attributes : highlight;
                        return (
                            <div key={attributes.title} className="flex pt-6">
                                <div className="flow-root rounded-lg bg-gray-50 px-6 pb-8 shadow-lg">
                                    <div className="-mt-6">
                                        <div>
                                            <span className="inline-flex items-center justify-center rounded-md bg-gradient-to-r from-blue-400 to-blue-500 p-3 shadow-lg">
                                                <StrapiIcon
                                                    icon={attributes.icon}
                                                    className="h-6 w-6 text-center text-2xl text-white"
                                                />
                                            </span>
                                        </div>
                                        <div className="mt-8 text-lg font-medium tracking-tight text-gray-900">
                                            {attributes.title}
                                        </div>
                                        <p className="mt-5 text-base text-gray-500">{attributes.description}</p>
                                    </div>
                                </div>
                            </div>
                        );
                    })}
                </div>
            </div>
        </div>
    );
};
