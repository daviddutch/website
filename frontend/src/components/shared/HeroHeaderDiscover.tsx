import React, { useState } from 'react';
import Link from 'next/link';
import Image from 'next/image';
import videoThumbnail from '@/assets/video-thumbnail-en.png';
import videoThumbnailFr from '@/assets/video-thumbnail-fr.png';
import videoThumbnailDe from '@/assets/video-thumbnail-de.png';
import { Trans } from '@lingui/macro';
import { Button } from '@/components/shared/Button';
import { YoutubeModal } from '@/components/shared/YoutubeModal';

export function HeroHeaderDiscover({ title, text, locale }: { title: string; text: string; locale?: string }) {
    const [isOpen, setIsOpen] = useState(false);
    const thumbnail = locale === 'fr' ? videoThumbnailFr : locale === 'de' ? videoThumbnailDe : videoThumbnail;
    return (
        <>
            <div className="relative overflow-hidden bg-white">
                <div
                    className="h-full w-1/3 shadow-dotted-fade lg:absolute lg:right-0 lg:bg-dotted-light lg:bg-dotted-20 lg:bg-dotted-10"
                    aria-hidden="true"
                ></div>
                <div className="relative pt-6 pb-16 sm:pb-24 lg:pb-32">
                    <main className="mx-auto mt-16 max-w-7xl px-4 sm:mt-24 sm:px-6 lg:mt-32">
                        <div className="lg:grid lg:grid-cols-12 lg:gap-8">
                            <div className="sm:text-center md:mx-auto md:max-w-2xl lg:col-span-6 lg:text-left">
                                <h1>
                                    <span className="block text-base font-semibold text-gray-500 sm:text-lg lg:text-base xl:text-lg">
                                        <Trans>Features</Trans>
                                    </span>
                                    <span className="mt-1 block text-4xl font-extrabold tracking-tight sm:text-5xl xl:text-7xl">
                                        <span className="block bg-gradient-to-r from-blue-400 to-blue-700 bg-clip-text pb-3 text-gray-900 text-transparent">
                                            {title}
                                        </span>
                                    </span>
                                </h1>
                                <p className="mt-3 text-base text-gray-500 sm:mt-5 sm:text-xl lg:text-lg xl:text-xl">
                                    {text}
                                </p>
                                <div className="mt-5 flex items-center justify-center gap-5 sm:mt-8 sm:inline-flex lg:justify-start">
                                    <Link href={'/demo'} passHref legacyBehavior>
                                        <Button className="w-full" size={'large'}>
                                            <Trans>Live demo</Trans>
                                        </Button>
                                    </Link>
                                    <Link href={'/signup'} passHref legacyBehavior>
                                        <Button className="w-full" variant={'secondary'} size={'large'}>
                                            <Trans>Get started</Trans>
                                        </Button>
                                    </Link>
                                </div>
                            </div>
                            <div className="relative mt-12 sm:mx-auto sm:max-w-lg lg:col-span-6 lg:mx-0 lg:mt-0 lg:flex lg:max-w-none lg:items-center">
                                <div className="relative mx-auto w-full lg:max-w-xl">
                                    <button
                                        type="button"
                                        onClick={() => setIsOpen(true)}
                                        className="relative block w-full overflow-hidden rounded-lg border border-gray-200 shadow-2xl hover:opacity-80 focus:outline-none focus:ring-2 focus:ring-blue-500 focus:ring-offset-4"
                                    >
                                        <span className="sr-only">
                                            <Trans>Watch our video to learn more</Trans>
                                        </span>
                                        <Image src={thumbnail} alt={'Video background'} className="w-full" />
                                        <span
                                            className="absolute inset-0 flex h-full w-full items-center justify-center"
                                            aria-hidden="true"
                                        >
                                            <svg
                                                className="h-20 w-20 fill-gray-100 text-red-500"
                                                fill="currentFill"
                                                viewBox="0 0 84 84"
                                            >
                                                <circle opacity="0.9" cx={42} cy={42} r={42} fill="currentColor" />
                                                <path d="M55.5039 40.3359L37.1094 28.0729C35.7803 27.1869 34 28.1396 34 29.737V54.263C34 55.8604 35.7803 56.8131 37.1094 55.9271L55.5038 43.6641C56.6913 42.8725 56.6913 41.1275 55.5039 40.3359Z" />
                                            </svg>
                                        </span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </main>
                </div>
            </div>
            <YoutubeModal isOpen={isOpen} onClose={() => setIsOpen(false)} />
        </>
    );
}
