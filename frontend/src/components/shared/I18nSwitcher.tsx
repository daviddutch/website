import { useRouter } from 'next/router';
import { useContext, useState } from 'react';
import { t } from '@lingui/macro';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronDown } from '@fortawesome/pro-regular-svg-icons';
import { setCookie } from 'cookies-next';
import { GlobalContext } from '@/components/shared/GlobalContext';

type LOCALES = 'en' | 'fr' | 'de';

export function I18nSwitcher() {
    const context = useContext(GlobalContext);
    const translatedPaths = context.translatedPaths;
    const router = useRouter();
    const [locale, setLocale] = useState<LOCALES>(router.locale!.split('-')[0] as LOCALES);

    const languages: { [key: string]: string } = {
        en: t`English`,
        fr: t`French`,
        de: t`German`,
    };

    const handleChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
        const locale = event.target.value as LOCALES;
        let path = router.asPath;

        if (translatedPaths && translatedPaths[locale]) {
            path = translatedPaths[locale];
        }

        setLocale(locale);
        setCookie('NEXT_LOCALE', locale, { maxAge: 60 * 60 * 24 * 365 });
        void router.push(path, path, { locale });
    };

    return (
        <div className="relative">
            <select
                value={locale}
                onChange={handleChange}
                className="block w-full appearance-none rounded-md border border-transparent bg-gray-700 bg-none py-2 pl-3 pr-10 text-base text-white focus:border-white focus:outline-none focus:ring-white sm:text-sm"
            >
                {Object.keys(languages).map((locale) => {
                    return (
                        <option value={locale} key={locale}>
                            {languages[locale as unknown as LOCALES]}
                        </option>
                    );
                })}
            </select>
            <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2">
                <FontAwesomeIcon icon={faChevronDown} className="h-4 w-4 text-white" aria-hidden="true" />
            </div>
        </div>
    );
}
