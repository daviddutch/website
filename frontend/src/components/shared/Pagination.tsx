import { faChevronLeft, faChevronRight } from '@fortawesome/pro-regular-svg-icons';
import { DOTS, usePagination } from './usePagination';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import clsx from 'clsx';
import Link from 'next/link';
import { Trans } from '@lingui/macro';

const classes = {
    base: 'border-gray-400 text-gray-900 bg-white hover:bg-gray-100 disabled:hover:bg-white focus:ring-blue-500 inline-flex gap-2 justify-center items-center whitespace-nowrap disabled:opacity-50 disabled:cursor-not-allowed disabled:hover:bg-none focus:outline-none focus:ring-2 focus:ring-offset-2',
    size: 'px-4 h-[2.625rem] text-sm font-medium rounded-md',
    active: 'bg-blue-100 hover:bg-blue-300',
};

type PaginationProps = {
    currentPage: number;
    getUrl: (page: number) => string;
    totalCount: number;
    pageSize: number;
    siblingCount?: number;
};

export const Pagination = ({ currentPage, getUrl, totalCount, pageSize, siblingCount = 1 }: PaginationProps) => {
    const paginationRange = usePagination({
        currentPage,
        totalCount,
        pageSize,
        siblingCount,
    });

    // If there are less than 2 times in pagination range we shall not render the component
    if (currentPage === 0 || !paginationRange || paginationRange.length < 2) {
        return null;
    }

    let lastPage = paginationRange[paginationRange.length - 1];
    return (
        <span className="isolate inline-flex rounded-md border border-gray-100 shadow-sm">
            {/* Left navigation arrow */}
            <Link
                href={getUrl(Math.max(currentPage - 1, 1))}
                key={1}
                className={clsx(classes.base, classes.size, 'rounded-r-none focus:z-10')}
            >
                <FontAwesomeIcon icon={faChevronLeft} className="-ml-0.5" />
                <span className="sr-only">
                    <Trans>Previous page</Trans>
                </span>
            </Link>
            {paginationRange.map((pageNumber, index) => {
                // If the pageItem is a DOT, render the DOTS unicode character
                if (pageNumber === DOTS) {
                    return (
                        <button
                            key={index + DOTS}
                            disabled={true}
                            className={clsx(classes.base, classes.size, pageNumber === currentPage && classes.active)}
                        >
                            ...
                        </button>
                    );
                }

                return (
                    <Link
                        href={getUrl(pageNumber)}
                        key={pageNumber}
                        className={clsx(
                            classes.base,
                            classes.size,
                            '-ml-px !rounded-none focus:z-10',
                            pageNumber === currentPage && classes.active
                        )}
                    >
                        {pageNumber}
                    </Link>
                );
            })}
            {/*  Right Navigation arrow */}
            <Link
                href={getUrl(Math.min(currentPage + 1, lastPage))}
                key={lastPage}
                className={clsx(classes.base, classes.size, '-ml-px rounded-l-none focus:z-10')}
            >
                <span className="sr-only">
                    <Trans>Next page</Trans>
                </span>
                <FontAwesomeIcon icon={faChevronRight} className="-mr-0.5" />
            </Link>
        </span>
    );
};
