import { ReactElement } from 'react';

interface Props {
    title: string;
    sectionName?: string;
    description?: string;
    children?: ReactElement;
}

export function SectionHeader({ title, sectionName, description, children }: Props) {
    return (
        <div className="bg-white">
            <div className="mx-auto max-w-7xl py-5 px-4 sm:py-10 sm:px-6 lg:px-8">
                <div className="text-center">
                    {sectionName && <p className="text-lg font-semibold text-blue-600">{sectionName}</p>}
                    {title && (
                        <h2 className="mt-1 text-2xl font-bold tracking-tight text-gray-900 sm:text-3xl lg:text-4xl">
                            {title}
                        </h2>
                    )}
                    {description && <p className="mx-auto mt-5 max-w-xl text-xl text-gray-500">{description}</p>}
                    {children}
                </div>
            </div>
        </div>
    );
}
