import { Highlight, HighlightTemplate } from '@/types/LandingPage';
import { StrapiIcon } from '@/components/shared/StrapiIcon';
import clsx from 'clsx';

export const HighlightsGrid = ({
    title,
    description,
    highlights,
}: {
    title: string;
    description?: string;
    highlights: (Highlight | HighlightTemplate)[];
}) => {
    return (
        <div className="bg-blue-700">
            <div className="mx-auto max-w-4xl px-4 py-16 sm:px-6 sm:pt-20 sm:pb-24 lg:max-w-7xl lg:px-8 lg:pt-24">
                {title && <h2 className="text-3xl font-bold tracking-tight text-white">{title}</h2>}

                {description && <p className="mt-4 max-w-3xl text-lg text-blue-200">{description}</p>}
                <div
                    className={clsx(
                        'mt-12 grid grid-cols-1 gap-x-6 gap-y-12 sm:grid-cols-2 lg:mt-16 lg:gap-x-8 lg:gap-y-16',
                        highlights.length >= 6 ? 'lg:grid-cols-3' : 'lg:grid-cols-4'
                    )}
                >
                    {highlights.map((highlight) => {
                        const attributes = 'attributes' in highlight ? highlight.attributes : highlight;
                        return (
                            <div key={attributes.title}>
                                <div>
                                    <span className="flex h-12 w-12 items-center justify-center rounded-md bg-opacity-10 bg-gradient-to-r from-blue-500 to-blue-600 text-xl">
                                        <StrapiIcon icon={attributes.icon} className="h-6 w-6 text-white" />
                                    </span>
                                </div>
                                <div className="mt-6">
                                    <h3 className="text-lg font-medium text-white">{attributes.title}</h3>
                                    <p className="mt-2 text-base text-blue-200">{attributes.description}</p>
                                </div>
                            </div>
                        );
                    })}
                </div>
            </div>
        </div>
    );
};
