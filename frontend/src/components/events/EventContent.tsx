import { MarkdownContent } from '@/components/shared/MarkdownContent';
import { Event } from '@/types/Event';

export function EventContent({ event }: { event: Event }) {
    return (
        <div className="mt-6 space-y-6 px-10 text-gray-700">
            <MarkdownContent>{event.attributes.content}</MarkdownContent>
        </div>
    );
}
