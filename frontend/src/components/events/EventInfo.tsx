import clsx from 'clsx';
import { StrapiImage } from '@/components/shared/StrapiImage';
import { Event } from '@/types/Event';
import { Employee } from '@/types/Employee';
import { IconProp } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
    faCalendar,
    faCalendarClock,
    faLanguage,
    faLocationPin,
    faPresentationScreen,
} from '@fortawesome/pro-duotone-svg-icons';
import { t } from '@lingui/macro';

function EventInfoCard({
    name,
    bgColor,
    icon,
    subtitle,
}: {
    name: string;
    bgColor: string;
    icon: IconProp;
    subtitle: string;
}) {
    return (
        <li key={name} className="col-span-1 flex rounded-md shadow-sm">
            <div
                className={clsx(
                    bgColor,
                    'flex w-16 flex-shrink-0 items-center justify-center rounded-l-md text-2xl font-medium text-white'
                )}
            >
                {/* @ts-ignore*/}
                <FontAwesomeIcon icon={icon} style={{ '--fa-secondary-opacity': 0.6 }} />
            </div>
            <div className="flex flex-1 items-center justify-between truncate rounded-r-md border-t border-r border-b border-gray-300 bg-white">
                <div className="flex-1 truncate px-4 py-2 text-sm">
                    <span className="text-xs text-gray-600 hover:text-gray-600">{name}</span>
                    <p className="text-base font-medium text-gray-800">{subtitle}</p>
                </div>
            </div>
        </li>
    );
}

function EventHost({ employee }: { employee: Employee }) {
    return (
        <div
            key={employee.attributes.email}
            className="relative flex items-center space-x-3 rounded-lg border border-gray-300 bg-gradient-to-r from-gray-50 to-gray-100 px-6 py-5 shadow-sm focus-within:ring-2 focus-within:ring-blue-500 focus-within:ring-offset-2 hover:border-gray-400"
        >
            <div className="flex-shrink-0">
                <StrapiImage media={employee.attributes.photo} className="h-20 w-20 rounded-full" />
            </div>
            <div className="min-w-0 flex-1">
                <span className="absolute inset-0" aria-hidden="true" />
                <p className="text-sm font-medium text-blue-800">
                    {employee.attributes.firstName} {employee.attributes.lastName}
                </p>
                <p className="text-sm text-gray-500">{employee.attributes.title}</p>
            </div>
        </div>
    );
}

export function EventInfo({ event }: { event: Event }) {
    return (
        <div className={'px-10 py-10'}>
            <ul role="list" className="mt-3 grid grid-cols-1 gap-5">
                {event.attributes.hosts.data.map((employee) => (
                    <EventHost key={employee.id} employee={employee} />
                ))}
                <EventInfoCard
                    name={t`Type`}
                    bgColor={'bg-gradient-to-r from-blue-400 to-blue-500'}
                    icon={faPresentationScreen}
                    subtitle={event.attributes.type}
                />
                <EventInfoCard
                    name={t`Date`}
                    bgColor={'bg-gradient-to-r from-purple-400 to-purple-500'}
                    icon={faCalendar}
                    subtitle={event.attributes.startDate}
                />
                <EventInfoCard
                    name={t`Duration`}
                    bgColor={'bg-gradient-to-r from-red-400 to-red-500'}
                    icon={faCalendarClock}
                    subtitle={event.attributes.duration}
                />
                <EventInfoCard
                    name={t`Location`}
                    bgColor={'bg-gradient-to-r from-yellow-400 to-yellow-500'}
                    icon={faLocationPin}
                    subtitle={event.attributes.location}
                />
                <EventInfoCard
                    name={t`Language`}
                    bgColor={'bg-gradient-to-r from-green-400 to-green-500'}
                    icon={faLanguage}
                    subtitle={event.attributes.language}
                />
            </ul>
        </div>
    );
}
