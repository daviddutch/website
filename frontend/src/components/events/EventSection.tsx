import { StrapiImage } from '@/components/shared/StrapiImage';
import { PageHeader } from '@/components/shared/PageHeader';
import { Event } from '@/types/Event';
import Link from 'next/link';
import { t, Trans } from '@lingui/macro';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCalendarExclamation } from '@fortawesome/pro-duotone-svg-icons';

const NoEvents = () => (
    <div className="rounded-lg border border-gray-200 bg-white py-10 shadow-lg">
        <div className="space-y-4 text-center">
            <FontAwesomeIcon icon={faCalendarExclamation} className="text-9xl text-blue-600" />
            <div className="flex-1">
                <p className="text-xl font-semibold text-gray-900">
                    <Trans>No events planned</Trans>
                </p>
                <p className="mt-3 text-base text-gray-500">
                    <Trans>Stay tuned, new events will be planned soon</Trans>
                </p>
            </div>
        </div>
    </div>
);

const EventCard = ({ event }: { event: Event }) => (
    <div key={event.id} className="flex flex-col overflow-hidden rounded-lg shadow-lg">
        <div className="flex-shrink-0">
            <StrapiImage className="max-h-max w-full object-cover" media={event.attributes.image} />
        </div>
        <div className="flex flex-1 flex-col justify-between bg-white p-6">
            <div className="flex-1">
                <Link href={`/events/${event.attributes.slug}`} className="mt-2 block">
                    <p className="text-xl font-semibold text-gray-900">{event.attributes.title}</p>
                    <p className="mt-3 text-base text-gray-500">{event.attributes.description}</p>
                </Link>
            </div>
        </div>
    </div>
);

export const EventSection = ({ events }: { events: Event[] }) => (
    <div className="relative bg-gray-50 px-4 pt-16 pb-10 sm:px-6 lg:px-8 lg:pt-24 lg:pb-20">
        <div className="absolute inset-0">
            <div className="h-1/3 bg-white sm:h-2/3" />
        </div>
        <div className="relative mx-auto max-w-7xl">
            <PageHeader title={t`Upcoming events`} description={t`Attend one of our next events`} />
            <div className="mx-auto mt-12 grid max-w-lg gap-5 lg:max-w-3xl lg:grid-cols-1">
                {events.length === 0 ? <NoEvents /> : events.map((event) => <EventCard key={event.id} event={event} />)}
            </div>
        </div>
    </div>
);
