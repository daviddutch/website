import { PageHeader } from '@/components/shared/PageHeader';
import { Post } from '@/types/Post';
import { Employee } from '@/types/Employee';
import { Category } from '@/types/Category';
import { StrapiImage } from '@/components/shared/StrapiImage';
import Link from 'next/link';
import { FormatDate } from '@/components/shared/FormatDate';

const WORDS_PER_MINUTE = 250;

type BlogCardProps = {
    post: Post;
    author: Employee;
    categories: Category[];
};

export function BlogCard({ post, author, categories }: BlogCardProps) {
    const time = Math.ceil(post.attributes.content.split(' ').length / WORDS_PER_MINUTE);
    return (
        <div key={post.attributes.title} className="flex flex-col overflow-hidden rounded-lg shadow-lg">
            <div className="flex-shrink-0">
                <Link href={'/blog/' + post.attributes.slug} className="hover:opacity-80">
                    <StrapiImage className="w-full object-cover" media={post.attributes.image} />
                </Link>
            </div>
            <div className="flex flex-1 flex-col justify-between bg-white p-6">
                <div className="flex-1">
                    <p className="text-sm font-medium text-blue-600">
                        {categories.map((category) => (
                            <Link
                                key={category.attributes.slug}
                                href={'/category/' + category.attributes.slug}
                                className="mr-2 hover:underline"
                            >
                                {category.attributes.name}
                            </Link>
                        ))}
                    </p>
                    <Link href={'/blog/' + post.attributes.slug} className="mt-2 block">
                        <p className="text-xl font-semibold text-gray-900 hover:text-gray-600">
                            {post.attributes.title}
                        </p>
                        <p className="mt-3 text-base text-gray-500 hover:text-gray-700">
                            {post.attributes.description}
                        </p>
                    </Link>
                </div>
                <div className="mt-6 flex items-center">
                    {author && (
                        <div className="flex-shrink-0">
                            <span className="sr-only">
                                {author.attributes.firstName + ' ' + author.attributes.lastName}
                            </span>
                            <StrapiImage className="h-10 w-10 rounded-full" media={author.attributes.photo} />
                        </div>
                    )}
                    <div className="ml-3">
                        <p className="text-sm font-medium text-gray-900">
                            {author ? author.attributes.firstName + ' ' + author.attributes.lastName : ''}
                        </p>
                        <div className="flex space-x-1 text-sm text-gray-500">
                            <FormatDate date={post.attributes.publicationDate} format={'P'} />
                            <span aria-hidden="true">&middot;</span>
                            <span>{time}min read</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

type BlogSectionProps = {
    posts: Post[];
    title: string;
    description: string;
};

export function BlogSection({ posts, title, description }: BlogSectionProps) {
    return (
        <div className="relative bg-gray-50 px-4 pt-16 pb-10 sm:px-6 lg:px-8 lg:pt-24 lg:pb-20">
            <div className="absolute inset-0">
                <div className="h-1/3 bg-white sm:h-2/3" />
            </div>
            <div className="relative mx-auto max-w-7xl">
                <PageHeader title={title} description={description} />
                <div className="mx-auto mt-12 grid max-w-lg gap-5 lg:max-w-none lg:grid-cols-3">
                    {posts.map((post) => (
                        <BlogCard
                            key={post.id}
                            post={post}
                            categories={post.attributes.categories.data}
                            author={post.attributes.author.data}
                        />
                    ))}
                </div>
            </div>
        </div>
    );
}
