import { Disclosure } from '@headlessui/react';
import clsx from 'clsx';
import Link from 'next/link';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars, faXmark } from '@fortawesome/pro-solid-svg-icons';
import { Trans } from '@lingui/macro';
import { Category } from '@/types/Category';

type Props = {
    categories: Category[];
    current?: Category;
};
export const BlogNavBar = ({ categories, current }: Props) => (
    <Disclosure as="nav" className="bg-gray-800">
        {({ open }) => (
            <>
                <div className="mx-auto max-w-7xl px-2 sm:px-4 lg:px-4">
                    <div className="relative flex h-16 items-center justify-between">
                        <div className="flex items-center px-2 lg:px-0">
                            <div className="hidden lg:ml-6 lg:block">
                                <div className="flex space-x-4">
                                    {/* Current: "bg-gray-900 text-white", Default: "text-gray-300 hover:bg-gray-700 hover:text-white" */}
                                    {categories.map((category, index) => {
                                        let active = category.attributes.slug === current?.attributes.slug;
                                        return (
                                            <Link
                                                href={'/category/' + category.attributes.slug}
                                                key={category.id}
                                                className={clsx(
                                                    active
                                                        ? 'rounded-md bg-gray-900 px-3 py-2 text-sm font-medium text-white'
                                                        : 'rounded-md px-3 py-2 text-sm font-medium text-gray-300 hover:bg-gray-700 hover:text-white'
                                                )}
                                            >
                                                {category.attributes.name}
                                            </Link>
                                        );
                                    })}
                                </div>
                            </div>
                        </div>
                        {/*<div className="flex flex-1 justify-center px-2 lg:ml-6 lg:justify-end">*/}
                        {/*    <div className="w-full max-w-lg lg:max-w-xs">*/}
                        {/*        <label htmlFor="search" className="sr-only">*/}
                        {/*            <Trans>Search</Trans>*/}
                        {/*        </label>*/}
                        {/*        <div className="relative">*/}
                        {/*            <div className="pointer-events-none absolute inset-y-0 left-0 flex items-center pl-3">*/}
                        {/*                <FontAwesomeIcon className="h-5 w-5 text-gray-400" icon={faMagnifyingGlass} />*/}
                        {/*            </div>*/}
                        {/*            <input*/}
                        {/*                id="search"*/}
                        {/*                name="search"*/}
                        {/*                className="block w-full rounded-md border border-transparent bg-gray-700 py-2 pl-10 pr-3 leading-5 text-gray-300 placeholder-gray-400 focus:border-white focus:bg-white focus:text-gray-900 focus:outline-none focus:ring-white sm:text-sm"*/}
                        {/*                placeholder="Search"*/}
                        {/*                type="search"*/}
                        {/*            />*/}
                        {/*        </div>*/}
                        {/*    </div>*/}
                        {/*</div>*/}
                        <div className="flex lg:hidden">
                            {/* Mobile menu button */}
                            <Disclosure.Button className="inline-flex items-center justify-center rounded-md p-2 text-gray-400 hover:bg-gray-700 hover:text-white focus:outline-none">
                                <span className="sr-only">
                                    <Trans>Open blog menu</Trans>
                                </span>
                                {open ? (
                                    <FontAwesomeIcon icon={faXmark} className="block h-6 w-6" aria-hidden="true" />
                                ) : (
                                    <FontAwesomeIcon icon={faBars} className=" blockh-6 w-6" aria-hidden="true" />
                                )}
                            </Disclosure.Button>
                        </div>
                    </div>
                </div>

                <Disclosure.Panel className="lg:hidden">
                    <div className="space-y-1 px-2 pt-2 pb-3">
                        {/* Current: "bg-gray-900 text-white", Default: "text-gray-300 hover:bg-gray-700 hover:text-white" */}
                        {categories.map((category, index) => {
                            let active = index == 3;
                            return (
                                <Disclosure.Button
                                    key={index}
                                    as="a"
                                    href="#"
                                    className={clsx(
                                        active
                                            ? 'block rounded-md bg-gray-900 px-3 py-2 text-base font-medium text-white'
                                            : 'block rounded-md px-3 py-2 text-base font-medium text-gray-300 hover:bg-gray-700 hover:text-white'
                                    )}
                                >
                                    {category.attributes.name}
                                </Disclosure.Button>
                            );
                        })}
                    </div>
                </Disclosure.Panel>
            </>
        )}
    </Disclosure>
);
