import { Tag } from '@/types/Tag';
import Link from 'next/link';

type Props = {
    tags: Tag[];
};
export const PostTags = ({ tags }: Props) => (
    <div className="flex flex-wrap gap-1.5">
        {tags.map((tag: Tag) => (
            <Link
                key={tag.attributes.slug}
                href={'/tag/' + tag.attributes.slug}
                className="inline-flex items-center rounded bg-gray-100 px-2 py-0.5 text-xs font-medium text-gray-800 hover:bg-gray-300"
            >
                {tag.attributes.name}
            </Link>
        ))}
    </div>
);
