import { Trans } from '@lingui/macro';
import { Employee } from '@/types/Employee';
import { StrapiImage } from '@/components/shared/StrapiImage';
import { faEnvelope, faPhone } from '@fortawesome/pro-regular-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import clsx from 'clsx';

export function Contacts({ contacts }: { contacts: Employee[] }) {
    if (!contacts) {
        return null;
    }
    return (
        <ul
            role="list"
            className={clsx(
                'grid grid-cols-1 gap-6 sm:grid-cols-2',
                contacts.length > 3 ? 'md:grid-cols-2 lg:grid-cols-4' : 'md:grid-cols-3'
            )}
        >
            {contacts.map((person) => (
                <li
                    key={person.attributes.email}
                    className="col-span-1 flex flex-col divide-y divide-gray-200 rounded-lg bg-white text-center shadow"
                >
                    <div className="flex flex-1 flex-col p-8">
                        <StrapiImage
                            className="mx-auto h-32 w-32 flex-shrink-0 rounded-full"
                            media={person.attributes.photo}
                        />
                        <h3 className="mt-6 text-sm font-medium text-gray-900">
                            {person.attributes.firstName} {person.attributes.lastName}
                        </h3>
                        <dl className="mt-1 flex flex-grow flex-col justify-between">
                            <dt className="sr-only">
                                <Trans>Title</Trans>
                            </dt>
                            <dd className="text-sm text-gray-500">{person.attributes.title}</dd>
                            <dt className="sr-only">
                                <Trans>Languages</Trans>
                            </dt>
                            <dd className="mt-3">
                                <span className="whitespace-nowrap rounded-full bg-gray-100 px-2 py-1 text-xs font-medium text-gray-800">
                                    {person.attributes.languages}
                                </span>
                            </dd>
                        </dl>
                    </div>
                    <div>
                        <div className="-mt-px flex divide-x divide-gray-200">
                            <div className="flex w-0 flex-1">
                                <a
                                    href={`mailto:${person.attributes.email}`}
                                    className="relative -mr-px inline-flex w-0 flex-1 items-center justify-center rounded-bl-lg border border-transparent py-4 text-sm font-medium text-gray-700 hover:text-gray-500"
                                >
                                    <FontAwesomeIcon
                                        className="h-6 w-6 text-gray-400"
                                        aria-hidden="true"
                                        icon={faEnvelope}
                                    />
                                    <span className="ml-3">
                                        <Trans>Email</Trans>
                                    </span>
                                </a>
                            </div>
                            <div className="-ml-px flex w-0 flex-1">
                                <a
                                    href={`tel:${person.attributes.phone}`}
                                    className="relative inline-flex w-0 flex-1 items-center justify-center rounded-br-lg border border-transparent py-4 text-sm font-medium text-gray-700 hover:text-gray-500"
                                >
                                    <FontAwesomeIcon
                                        className="h-5 w-5 text-gray-400"
                                        aria-hidden="true"
                                        icon={faPhone}
                                    />
                                    <span className="ml-3">
                                        <Trans>Call</Trans>
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </li>
            ))}
        </ul>
    );
}
