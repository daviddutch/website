import { Trans } from '@lingui/macro';
import Link from 'next/link';
import React from 'react';
import { Button } from '@/components/shared/Button';

export const CallToActionMain = () => (
    <div className="bg-gradient-to-r from-blue-300 to-blue-500">
        <div className="mx-auto max-w-7xl py-12 px-4 sm:px-6 lg:flex lg:items-center lg:justify-between lg:py-16 lg:px-8">
            <h2 className="text-3xl font-bold tracking-tight text-blue-900 sm:text-4xl">
                <span className="block">
                    <Trans>Ready to dive in?</Trans>
                </span>
                <span className="mt-1 block text-white">
                    <Trans>Start your free trial today.</Trans>
                </span>
            </h2>
            <div className="mt-8 flex lg:mt-0 lg:flex-shrink-0">
                <Link href={'/demo'} passHref legacyBehavior>
                    <Button variant={'white'} size={'large'}>
                        <Trans>Live demo</Trans>
                    </Button>
                </Link>
                <Link href={'/signup'} passHref legacyBehavior>
                    <Button variant={'dark'} size={'large'} className="ml-3">
                        <Trans>Get started</Trans>
                    </Button>
                </Link>
            </div>
        </div>
    </div>
);
