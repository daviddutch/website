import { Trans } from '@lingui/macro';
import Link from 'next/link';
import React from 'react';
import Image from 'next/image';
import SOC2 from '@/assets/security/SOC2.svg';
import GDPR from '@/assets/security/GDPR.svg';
import Finma from '@/assets/security/Finma.svg';
import CSA from '@/assets/security/CSA.svg';
import ISO27001 from '@/assets/security/ISO-27001.svg';
import ISO27018 from '@/assets/security/ISO-27018.svg';
import { Button } from '@/components/shared/Button';

export function SecuritySection() {
    return (
        <div className="bg-white">
            <div className="mx-auto max-w-7xl py-12 px-4 sm:px-6 lg:py-16 lg:px-8">
                <div className="lg:grid lg:grid-cols-2 lg:items-center lg:gap-8">
                    <div>
                        <h2 className="text-3xl font-bold tracking-tight text-gray-900 sm:text-4xl">
                            <Trans>Your data security is our top priority</Trans>
                        </h2>
                        <p className="mt-3 max-w-3xl text-lg text-gray-500">
                            <Trans>
                                You can rest easy knowing that we employ many security technologies and industry best
                                practices to ensure that our product and systems stay secure.
                            </Trans>
                        </p>
                        <div className="mt-8 flex gap-5">
                            <div className="rounded-md shadow">
                                <Link href={'/security'} legacyBehavior>
                                    <Button className="h-full">
                                        <Trans>Learn more</Trans>
                                    </Button>
                                </Link>
                            </div>
                            <div className="">
                                <Link href={'/contact'} legacyBehavior>
                                    <Button className="h-full" variant={'secondary'}>
                                        <Trans>Contact Us</Trans>
                                    </Button>
                                </Link>
                            </div>
                        </div>
                    </div>
                    <div className="mt-8 grid grid-cols-2 gap-0.5 md:grid-cols-3 lg:mt-0 lg:grid-cols-2">
                        <div className="col-span-1 flex justify-center bg-gray-50 py-8 px-8">
                            <Image className="max-h-12" src={SOC2} alt={'SOC2 Logo'} />
                        </div>
                        <div className="col-span-1 flex justify-center bg-gray-50 py-8 px-8">
                            <Image className="max-h-12" src={GDPR} alt={'SOC2 Logo'} />
                        </div>
                        <div className="col-span-1 flex justify-center bg-gray-50 py-8 px-8">
                            <Image className="max-h-12" src={ISO27001} alt={'SOC2 Logo'} />
                        </div>
                        <div className="col-span-1 flex justify-center bg-gray-50 py-8 px-8">
                            <Image className="max-h-12" src={ISO27018} alt={'SOC2 Logo'} />
                        </div>
                        <div className="col-span-1 flex justify-center bg-gray-50 py-8 px-8">
                            <Image className="max-h-12" src={CSA} alt={'SOC2 Logo'} />
                        </div>
                        <div className="col-span-1 flex justify-center bg-gray-50 py-8 px-8">
                            <Image className="max-h-12" src={Finma} alt={'SOC2 Logo'} />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
