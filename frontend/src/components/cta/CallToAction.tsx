import { CtaType } from '@/types/LandingPage';
import React from 'react';
import { CallToActionMain } from '@/components/cta/CallToActionMain';
import { SecuritySection } from '@/components/cta/SecuritySection';
import { CallToActionJob } from '@/components/cta/CallToActionJob';
import { CallToActionCalendly } from '@/components/cta/CallToActionCalendly';
import { CallToActionCalendlyEmbed } from '@/components/cta/CallToActionCalendlyEmbed';

export const CallToAction = ({ type }: { type?: CtaType }) => {
    if (type === CtaType.Job) {
        return <CallToActionJob />;
    }
    if (type === CtaType.Calendly) {
        return <CallToActionCalendly />;
    }
    if (type === CtaType.CalendlyEmbed) {
        return <CallToActionCalendlyEmbed />;
    }
    if (type === CtaType.Security) {
        return <SecuritySection />;
    }
    return <CallToActionMain />;
};
