import React from 'react';
import { Trans } from '@lingui/macro';
import Image from 'next/image';
import Link from 'next/link';
import { Button } from '@/components/shared/Button';

export const CallToActionCalendly = () => {
    return (
        <div className="bg-white">
            <div className="mx-auto max-w-7xl py-16 px-4 sm:px-6 lg:px-8">
                <Link
                    href={'/demo'}
                    className="rounded-lg bg-blue-gradient shadow-xl hover:bg-purple-blue-gradient lg:grid lg:grid-cols-2 lg:gap-4"
                >
                    <div className="rounded-lg bg-blue-gradient px-6 pt-10 pb-12 sm:px-16 sm:pt-16 lg:bg-none lg:py-16 lg:pr-0 xl:py-20 xl:px-20">
                        <div className="lg:self-center">
                            <h2 className="text-3xl font-bold tracking-tight text-white sm:text-4xl">
                                <span className="block">
                                    <Trans>Ready to dive in?</Trans>
                                </span>
                                <span className="block">
                                    <Trans>Book a demo today.</Trans>
                                </span>
                            </h2>
                            <p className="mt-4 text-lg leading-6 text-blue-100">
                                <Trans>
                                    Presentation of the platform in videoconference. Approximately 30 minutes of
                                    presentation and 30 minutes of questions and answers.
                                </Trans>
                            </p>

                            <Button variant={'white'} className={'mt-4'}>
                                <Trans>Choose a date</Trans>
                            </Button>
                        </div>
                    </div>
                    <div className="hidden hover:opacity-80 lg:block">
                        <Image
                            className="-translate-x-4 translate-y-6 transform rounded-md sm:translate-y-6 lg:translate-y-20"
                            src="/images/calendly-cta.png"
                            alt="App screenshot"
                            width={1000}
                            height={525}
                        />
                    </div>
                </Link>
            </div>
        </div>
    );
};
