import { IconProp } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Trans } from '@lingui/macro';
import { i18n } from '@lingui/core';
import { MarkdownContent } from '@/components/shared/MarkdownContent';

export type Highlight = {
    icon: IconProp;
    title: string;
    description: string;
};
type Props = {
    highlights: Highlight[];
};

export function SecurityHighlights({ highlights }: Props) {
    return (
        <div className="relative bg-white py-6 sm:py-10 lg:py-16">
            <div className="mx-auto max-w-md px-4 text-center sm:max-w-3xl sm:px-6 lg:max-w-7xl lg:px-8">
                <div className="grid grid-cols-1 gap-8 sm:grid-cols-2 lg:grid-cols-3">
                    {highlights.map((item) => (
                        <div key={item.title} className="flex pt-6">
                            <div className="flow-root rounded-lg bg-gray-50 px-4 pb-4">
                                <div className="-mt-6">
                                    <div>
                                        <span className="inline-flex items-center justify-center rounded-md bg-gradient-to-r from-blue-400 to-blue-500 p-3 shadow-lg">
                                            <FontAwesomeIcon className="text-3xl text-white" icon={item.icon} />
                                        </span>
                                    </div>
                                    <h3 className="mt-4 text-lg font-medium tracking-tight text-gray-900">
                                        <Trans id={item.title} />
                                    </h3>
                                    <div className="mx-3 mt-5 text-left text-base text-gray-500">
                                        <MarkdownContent>{i18n._(item.description)}</MarkdownContent>
                                    </div>
                                </div>
                            </div>
                        </div>
                    ))}
                </div>
            </div>
        </div>
    );
}
