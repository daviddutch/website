import { Trans } from '@lingui/macro';
import Link from 'next/link';

export function Privacy() {
    return (
        <div className="relative bg-gradient-to-r from-blue-400 to-blue-700">
            <div className="mx-auto max-w-7xl py-3 px-3 sm:px-6 lg:px-8">
                <div className="pr-16 sm:px-16 sm:text-center">
                    <p className="font-medium text-white">
                        <span className="inline">
                            <Trans>Learn more about how we treat privacy.</Trans>
                        </span>
                        <span className="block sm:ml-2 sm:inline-block">
                            <Link href={'/privacy'} className="font-bold text-white underline">
                                <Trans>Privacy Policy</Trans>
                                <span aria-hidden="true"> &rarr;</span>
                            </Link>
                        </span>
                    </p>
                </div>
            </div>
        </div>
    );
}
