import { Trans } from '@lingui/macro';
import React from 'react';
import Link from 'next/link';
import Image from 'next/image';

export function SocCompliance({
    title,
    subHeader,
    children,
}: {
    title: string;
    subHeader: string;
    children: React.ReactNode;
}) {
    return (
        <div className="relative bg-gray-800">
            <div className="h-56 bg-blue-600 sm:h-72 md:absolute  md:left-0  md:h-full md:w-1/2">
                <Image
                    className="h-full w-full object-cover"
                    src="/images/security.svg"
                    alt=""
                    width={1920}
                    height={1280}
                />
            </div>
            <div className="relative mx-auto max-w-7xl px-4 py-12 sm:px-6 lg:px-8 lg:py-16">
                <div className="md:ml-auto md:w-1/2 md:pl-10">
                    <h2 className="text-lg font-semibold text-gray-300">{subHeader}</h2>
                    <p className="mt-2 text-3xl font-bold uppercase tracking-tight text-white sm:text-4xl">{title}</p>
                    <p className="mt-3 text-lg text-gray-300">{children}</p>
                    <div className="mt-8">
                        <div className="inline-flex rounded-md shadow">
                            <Link
                                href="/blog/wedo-is-soc-2-type-ii-compliant"
                                className="inline-flex items-center justify-center rounded-md border border-transparent bg-white px-5 py-3 text-base font-medium text-gray-900 hover:bg-gray-50"
                            >
                                <Trans>Learn more</Trans>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
