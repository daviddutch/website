import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faClock, faEnvelope } from '@fortawesome/pro-solid-svg-icons';
import { Button } from '@/components/shared/Button';
import { Trans } from '@lingui/macro';
import { FormEventHandler, useState } from 'react';
import { useGoogleReCaptcha } from 'react-google-recaptcha-v3';
import { faEnvelopeCircleCheck } from '@fortawesome/pro-duotone-svg-icons';
import { faFacebook, faInstagram, faLinkedin, faTwitter } from '@fortawesome/free-brands-svg-icons';

type FormStatus = 'captchaError' | 'submitting' | 'success' | 'error';

const ContactInfo = () => {
    return (
        <div className="relative flex flex-col overflow-hidden bg-blue-500 py-10 px-6 sm:px-10 xl:p-12">
            <div>
                <div className="pointer-events-none absolute inset-0 sm:hidden" aria-hidden="true">
                    <svg
                        className="absolute inset-0 h-full w-full"
                        width={343}
                        height={388}
                        viewBox="0 0 343 388"
                        fill="none"
                        preserveAspectRatio="xMidYMid slice"
                        xmlns="http://www.w3.org/2000/svg"
                    >
                        <path
                            d="M-99 461.107L608.107-246l707.103 707.107-707.103 707.103L-99 461.107z"
                            fill="url(#linear1)"
                            fillOpacity=".1"
                        />
                        <defs>
                            <linearGradient
                                id="linear1"
                                x1="254.553"
                                y1="107.554"
                                x2="961.66"
                                y2="814.66"
                                gradientUnits="userSpaceOnUse"
                            >
                                <stop stopColor="#fff" />
                                <stop offset={1} stopColor="#fff" stopOpacity={0} />
                            </linearGradient>
                        </defs>
                    </svg>
                </div>
                <div
                    className="pointer-events-none absolute top-0 right-0 bottom-0 hidden w-1/2 sm:block lg:hidden"
                    aria-hidden="true"
                >
                    <svg
                        className="absolute inset-0 h-full w-full"
                        width={359}
                        height={339}
                        viewBox="0 0 359 339"
                        fill="none"
                        preserveAspectRatio="xMidYMid slice"
                        xmlns="http://www.w3.org/2000/svg"
                    >
                        <path
                            d="M-161 382.107L546.107-325l707.103 707.107-707.103 707.103L-161 382.107z"
                            fill="url(#linear2)"
                            fillOpacity=".1"
                        />
                        <defs>
                            <linearGradient
                                id="linear2"
                                x1="192.553"
                                y1="28.553"
                                x2="899.66"
                                y2="735.66"
                                gradientUnits="userSpaceOnUse"
                            >
                                <stop stopColor="#fff" />
                                <stop offset={1} stopColor="#fff" stopOpacity={0} />
                            </linearGradient>
                        </defs>
                    </svg>
                </div>
                <div
                    className="pointer-events-none absolute top-0 right-0 bottom-0 hidden w-1/2 lg:block"
                    aria-hidden="true"
                >
                    <svg
                        className="absolute inset-0 h-full w-full"
                        width={160}
                        height={678}
                        viewBox="0 0 160 678"
                        fill="none"
                        preserveAspectRatio="xMidYMid slice"
                        xmlns="http://www.w3.org/2000/svg"
                    >
                        <path
                            d="M-161 679.107L546.107-28l707.103 707.107-707.103 707.103L-161 679.107z"
                            fill="url(#linear3)"
                            fillOpacity=".1"
                        />
                        <defs>
                            <linearGradient
                                id="linear3"
                                x1="192.553"
                                y1="325.553"
                                x2="899.66"
                                y2="1032.66"
                                gradientUnits="userSpaceOnUse"
                            >
                                <stop stopColor="#fff" />
                                <stop offset={1} stopColor="#fff" stopOpacity={0} />
                            </linearGradient>
                        </defs>
                    </svg>
                </div>
                <h3 className="text-xl font-medium text-white">WEDO Sàrl</h3>
                <p className="mt-6 max-w-3xl text-base text-white">
                    Boulevard de Pérolles 3<br />
                    1700 Fribourg
                    <br />
                    Suisse
                </p>
            </div>
            <div className="flex flex-1 flex-col justify-between">
                <dl className="mt-8 space-y-3">
                    <dt>
                        <span className="sr-only">
                            <Trans>Working hours</Trans>
                        </span>
                    </dt>
                    <dd className="flex text-base text-white">
                        <FontAwesomeIcon
                            className="mt-1 h-6 w-6 flex-shrink-0 text-blue-200"
                            aria-hidden="true"
                            icon={faClock}
                        />
                        <span className="ml-3">Monday to friday</span>
                        <span className="ml-3">8:00am to 5:30pm</span>
                    </dd>
                    <dt>
                        <span className="sr-only">
                            <Trans>Email</Trans>
                        </span>
                    </dt>
                    <dd className="flex text-base text-white">
                        <FontAwesomeIcon
                            className="mt-1 h-6 w-6 flex-shrink-0 text-blue-200"
                            aria-hidden="true"
                            icon={faEnvelope}
                        />
                        <span className="ml-3">info@wedo.com</span>
                    </dd>
                </dl>
                <ul role="list" className="mt-8 flex justify-center gap-10">
                    <li>
                        <a className="text-blue-200 hover:text-blue-100" href="https://www.facebook.com/wedoswiss/">
                            <span className="sr-only">Facebook</span>
                            <FontAwesomeIcon icon={faFacebook} className="h-6 w-6 text-3xl" aria-hidden="true" />
                        </a>
                    </li>
                    <li>
                        <a className="text-blue-200 hover:text-blue-100" href="https://www.instagram.com/wedoswiss/">
                            <span className="sr-only">Instagram</span>
                            <FontAwesomeIcon icon={faInstagram} className="h-6 w-6 text-3xl" aria-hidden="true" />
                        </a>
                    </li>
                    <li>
                        <a
                            className="text-blue-200 hover:text-blue-100"
                            href="https://www.linkedin.com/company/wedoswiss"
                        >
                            <span className="sr-only">LinkedIn</span>
                            <FontAwesomeIcon icon={faLinkedin} className="h-6 w-6 text-3xl" aria-hidden="true" />
                        </a>
                    </li>
                    <li>
                        <a className="text-blue-200 hover:text-blue-100" href="https://twitter.com/wedoswiss">
                            <span className="sr-only">Twitter</span>
                            <FontAwesomeIcon icon={faTwitter} className="h-6 w-6 text-3xl" aria-hidden="true" />
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    );
};
const FormBody = ({
    handleSubmit,
    isBtnDisabled,
    status,
}: {
    handleSubmit: FormEventHandler;
    isBtnDisabled: boolean;
    status: FormStatus | null;
}) => {
    return (
        <div className="py-10 px-6 sm:px-10 lg:col-span-2 xl:p-12">
            <h3 className="text-lg font-medium text-gray-900">
                <Trans>Send us a message</Trans>
            </h3>
            <form onSubmit={handleSubmit} className="mt-6 grid grid-cols-1 gap-y-6 sm:grid-cols-2 sm:gap-x-8">
                <div>
                    <label htmlFor="full-name" className="block text-sm font-medium text-gray-900">
                        <Trans>Name</Trans>
                    </label>
                    <div className="mt-1">
                        <input
                            type="text"
                            name="fullName"
                            id="full-name"
                            autoComplete="name"
                            required
                            className="block w-full rounded-md border-gray-300 py-3 px-4 text-gray-900 shadow-sm focus:border-blue-500 focus:ring-blue-500"
                        />
                    </div>
                </div>
                <div>
                    <label htmlFor="company" className="block text-sm font-medium text-gray-900">
                        <Trans>Company</Trans>
                    </label>
                    <div className="mt-1">
                        <input
                            type="text"
                            name="company"
                            id="company"
                            autoComplete="company"
                            required
                            className="block w-full rounded-md border-gray-300 py-3 px-4 text-gray-900 shadow-sm focus:border-blue-500 focus:ring-blue-500"
                        />
                    </div>
                </div>
                <div>
                    <label htmlFor="email" className="block text-sm font-medium text-gray-900">
                        <Trans>Email</Trans>
                    </label>
                    <div className="mt-1">
                        <input
                            id="email"
                            name="email"
                            type="email"
                            autoComplete="email"
                            required
                            className="block w-full rounded-md border-gray-300 py-3 px-4 text-gray-900 shadow-sm focus:border-blue-500 focus:ring-blue-500"
                        />
                    </div>
                </div>
                <div>
                    <div className="flex justify-between">
                        <label htmlFor="phone" className="block text-sm font-medium text-gray-900">
                            <Trans>Phone</Trans>
                        </label>
                        <span id="phone-optional" className="text-sm text-gray-500">
                            <Trans>Optional</Trans>
                        </span>
                    </div>
                    <div className="mt-1">
                        <input
                            type="text"
                            name="phone"
                            id="phone"
                            autoComplete="tel"
                            className="block w-full rounded-md border-gray-300 py-3 px-4 text-gray-900 shadow-sm focus:border-blue-500 focus:ring-blue-500"
                            aria-describedby="phone-optional"
                        />
                    </div>
                </div>
                <div className="sm:col-span-2">
                    <label htmlFor="subject" className="block text-sm font-medium text-gray-900">
                        <Trans>Subject</Trans>
                    </label>
                    <div className="mt-1">
                        <input
                            type="text"
                            name="subject"
                            id="subject"
                            required
                            className="block w-full rounded-md border-gray-300 py-3 px-4 text-gray-900 shadow-sm focus:border-blue-500 focus:ring-blue-500"
                        />
                    </div>
                </div>
                <div className="sm:col-span-2">
                    <div className="flex justify-between">
                        <label htmlFor="message" className="block text-sm font-medium text-gray-900">
                            <Trans>Message</Trans>
                        </label>
                        <span id="message-max" className="text-sm text-gray-500">
                            <Trans>Max. 1000 characters</Trans>
                        </span>
                    </div>
                    <div className="mt-1">
                        <textarea
                            id="message"
                            name="message"
                            rows={10}
                            maxLength={1000}
                            className="block w-full rounded-md border-gray-300 py-3 px-4 text-gray-900 shadow-sm focus:border-blue-500 focus:ring-blue-500"
                            aria-describedby="message-max"
                            defaultValue={''}
                            required
                        />
                    </div>
                </div>
                <div className="sm:col-span-2 sm:flex sm:items-center sm:justify-between">
                    <div>
                        {status === 'captchaError' && (
                            <div className="rounded bg-red-500 p-2 font-bold text-red-100">
                                <Trans>Invalid captcha validation, please try again</Trans>
                            </div>
                        )}
                        {status === 'error' && (
                            <div className="rounded bg-red-500 p-2 font-bold text-red-100">
                                <Trans>An error occurred while submitting the form, please try again</Trans>
                            </div>
                        )}
                    </div>
                    <div className="float-right">
                        <Button type="submit" size="large" variant="primary" disabled={isBtnDisabled}>
                            <Trans>Send message</Trans>
                        </Button>
                    </div>
                </div>
            </form>
        </div>
    );
};

type ContactFormData = {
    fullName: HTMLInputElement;
    company: HTMLInputElement;
    email: HTMLInputElement;
    phone: HTMLInputElement;
    subject: HTMLInputElement;
    message: HTMLTextAreaElement;
} & HTMLFormElement;

export function ContactForm() {
    const { executeRecaptcha } = useGoogleReCaptcha();
    const [isBtnDisabled, setBtnDisabled] = useState(false);
    const [status, setStatus] = useState<FormStatus | null>(null);

    const handleSubmit: FormEventHandler<ContactFormData> = async (event) => {
        event.preventDefault();

        setBtnDisabled(true);

        if (!executeRecaptcha) {
            return;
        }

        const data = {
            fullName: event.currentTarget.fullName.value,
            company: event.currentTarget.company.value,
            email: event.currentTarget.email.value,
            phone: event.currentTarget.phone.value,
            subject: event.currentTarget.subject.value,
            message: event.currentTarget.message.value,
            token: '',
        };
        try {
            const token = await executeRecaptcha();
            if (!token) {
                setStatus('captchaError');
                return;
            }

            data.token = token;

            const options = {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(data),
            };

            const response = await fetch('/api/contact', options);

            const result = await response.json();

            setBtnDisabled(false);
            setStatus('success');
        } catch (error) {
            setStatus('error');
            setBtnDisabled(false);
        }
    };

    return (
        <div className="bg-gray-100">
            <div className="mx-auto max-w-7xl py-16 px-4 sm:py-12 sm:px-6 lg:px-8">
                <div className="relative bg-white shadow-xl">
                    <h2 className="sr-only">
                        <Trans>Contact us</Trans>
                    </h2>

                    <div className="grid grid-cols-1 lg:grid-cols-3">
                        <ContactInfo />

                        {status === 'success' ? (
                            <div className="col-span-2 flex items-center justify-center py-48">
                                <div className="text-center">
                                    <FontAwesomeIcon
                                        icon={faEnvelopeCircleCheck}
                                        className="mx-auto text-9xl text-blue-600"
                                    />
                                    <h3 className="mt-5 max-w-2xl text-2xl font-extrabold text-gray-700">
                                        <Trans>Thank you for contacting us, we will get back to you shortly</Trans>
                                    </h3>
                                </div>
                            </div>
                        ) : (
                            <FormBody handleSubmit={handleSubmit} isBtnDisabled={isBtnDisabled} status={status} />
                        )}
                    </div>
                </div>
            </div>
        </div>
    );
}
