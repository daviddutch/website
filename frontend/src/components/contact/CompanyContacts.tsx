import { faEnvelope, faPhone } from '@fortawesome/pro-regular-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { t } from '@lingui/macro';

const ContactCard = ({ title, phone, email }: { title: string; phone: string; email: string }) => {
    return (
        <div className="rounded border border-gray-200 bg-white py-10 px-6 shadow">
            <h2 className="text-2xl font-bold text-gray-900 sm:text-3xl sm:tracking-tight">{title}</h2>
            <div className="mt-9">
                <div className="flex">
                    <div className="flex-shrink-0">
                        <FontAwesomeIcon className="h-6 w-6 text-gray-400" aria-hidden="true" icon={faPhone} />
                    </div>
                    <div className="ml-3 text-base text-gray-500">
                        <p>{phone}</p>
                    </div>
                </div>
                <div className="mt-6 flex">
                    <div className="flex-shrink-0">
                        <FontAwesomeIcon className="h-6 w-6 text-gray-400" aria-hidden="true" icon={faEnvelope} />
                    </div>
                    <div className="ml-3 text-base text-gray-500">
                        <p>{email}</p>
                    </div>
                </div>
            </div>
        </div>
    );
};

export function CompanyContacts() {
    return (
        <div className="bg-gray-100">
            <div className="mx-auto max-w-7xl py-16 px-4 sm:px-6 lg:px-8">
                <div className="mx-auto max-w-lg md:grid md:max-w-none md:grid-cols-2 md:gap-8">
                    <ContactCard title={t`Support`} email={'info@wedo.com'} phone={'+41 79 892 09 39'} />
                    <ContactCard title={t`Security`} email={'security@wedo.com'} phone={'+41 79 797 98 81'} />
                </div>
            </div>
        </div>
    );
}
