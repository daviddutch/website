import { Trans } from '@lingui/macro';
import { useContext } from 'react';
import { Currency, PricingContext } from '@/state/pricingContext';
import clsx from 'clsx';

export function PricingHeader() {
    const { currency, setCurrency } = useContext(PricingContext);

    function changeCurrency(value: Currency) {
        setCurrency(value);
    }

    return (
        <div className="pt-12 sm:pt-16 lg:pt-24">
            <div className="mx-auto max-w-7xl px-4 text-center sm:px-6 lg:px-8">
                <div className="mx-auto max-w-3xl space-y-2 lg:max-w-none">
                    <h2 className="text-xl font-semibold leading-6 text-gray-300">
                        <Trans>Based on the number of seats</Trans>
                    </h2>
                    <p className="text-3xl font-bold tracking-tight text-white sm:text-4xl lg:text-5xl">
                        <Trans>Our Pricing Plans</Trans>
                    </p>
                    <p className="text-xl text-gray-300">
                        <Trans>Join 500+ customers now.</Trans>&nbsp;
                        <Trans>14-day free trial.</Trans>&nbsp;
                        <Trans>No credit card required.</Trans>
                    </p>
                </div>
                <div className="sm:align-center sm:flex sm:flex-col">
                    <div className="relative mt-6 flex self-center rounded-lg bg-gray-400 p-0.5 sm:mt-8">
                        <button
                            type="button"
                            onClick={() => changeCurrency('usd')}
                            className={clsx(
                                'relative w-1/2 whitespace-nowrap rounded-md border-gray-200 py-2 text-sm font-medium text-gray-900 shadow-sm focus:z-10 focus:outline-none sm:w-auto sm:px-8',
                                currency === 'usd' ? 'bg-white' : ''
                            )}
                        >
                            USD
                        </button>
                        <button
                            type="button"
                            onClick={() => changeCurrency('chf')}
                            className={clsx(
                                'relative w-1/2 whitespace-nowrap rounded-md border-gray-200 py-2 text-sm font-medium text-gray-900 shadow-sm focus:z-10 focus:outline-none sm:w-auto sm:px-8',
                                currency === 'chf' ? 'bg-white' : ''
                            )}
                        >
                            CHF
                        </button>
                        <button
                            type="button"
                            onClick={() => changeCurrency('eur')}
                            className={clsx(
                                'relative w-1/2 whitespace-nowrap rounded-md border-gray-200 py-2 text-sm font-medium text-gray-900 shadow-sm focus:z-10 focus:outline-none sm:w-auto sm:px-8',
                                currency === 'eur' ? 'bg-white' : ''
                            )}
                        >
                            EUR
                        </button>
                    </div>
                </div>
            </div>
        </div>
    );
}
