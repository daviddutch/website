import clsx from 'clsx';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Trans } from '@lingui/macro';
import { IconProp } from '@fortawesome/fontawesome-svg-core';
import Link from 'next/link';

export interface Feature {
    name: string;
    icon: IconProp;
}

interface Props {
    name: string;
    price: string;
    unit: string;
    frequency: string;
    features: Feature[];
    featured: boolean;
}

export function PricingPlan({ name, price, unit, frequency, features, featured = false }: Props) {
    return (
        <div className="flex flex-col overflow-hidden rounded-lg shadow-lg">
            <div className={'bg-white px-6 py-8 sm:p-10 sm:pb-6'}>
                <div>
                    <h3
                        className={clsx(
                            'inline-flex rounded-full px-4 py-1 text-base font-semibold ',
                            featured ? 'bg-green-200 text-green-700' : 'bg-blue-200 text-blue-700'
                        )}
                    >
                        <Trans id={name} />
                    </h3>
                </div>
                <div className="mt-4">
                    <span className="text-6xl font-bold tracking-tight">{price}</span>
                    <br />
                    <span className="ml-1 text-2xl font-medium lowercase tracking-normal text-gray-500">
                        /<Trans id={unit} />/<Trans id={frequency} />
                    </span>
                </div>
            </div>
            <div className={'flex flex-1 flex-col justify-between space-y-6 bg-gray-50 px-6 pt-6 pb-8 sm:p-10 sm:pt-6'}>
                <ul role="list" className="space-y-4">
                    {features.map((feature, index) => (
                        <li key={feature.name} className="flex items-start">
                            <div className="flex-shrink-0">
                                <FontAwesomeIcon icon={feature.icon} />
                            </div>
                            <p className="ml-3 text-base text-gray-700">
                                <Trans id={feature.name} />
                            </p>
                        </li>
                    ))}
                </ul>
                <div className="rounded-md shadow">
                    {featured ? (
                        <Link
                            href={'/contact'}
                            className="flex items-center justify-center rounded-md border border-transparent bg-gradient-to-r from-green-400 to-green-500 px-5 py-3 text-base font-medium text-white hover:bg-gradient-to-l"
                            aria-describedby="tier-standard"
                        >
                            <Trans>Contact us</Trans>
                        </Link>
                    ) : (
                        <Link
                            href={'/trial'}
                            className="flex items-center justify-center rounded-md border border-transparent bg-gradient-to-r from-blue-400 to-blue-500 px-5 py-3 text-base font-medium text-white hover:bg-gradient-to-l"
                            aria-describedby="tier-standard"
                        >
                            <Trans>Get started</Trans>
                        </Link>
                    )}
                </div>
            </div>
        </div>
    );
}
