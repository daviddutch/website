import { t } from '@lingui/macro';
import { Feature, PricingPlan } from '@/components/pricing/PricingPlan';
import { useContext } from 'react';
import { PricingContext, PricingContextProps } from '@/state/pricingContext';

interface Plan {
    name: string;
    pricing: {
        usd: string;
        chf: string;
        eur: string;
    };
    features: Feature[];
}

export function PricingTable({ plans }: { plans: Plan[] }) {
    const { currency } = useContext<PricingContextProps>(PricingContext);
    return (
        <div className="mt-8 bg-gray-50 pb-12 sm:mt-12 sm:pb-16 lg:mt-16 lg:pb-24">
            <div className="relative">
                <div className="absolute inset-0 h-3/4 bg-blue-800" />
                <div className="relative z-10 mx-auto max-w-7xl px-4 sm:px-6 lg:px-8">
                    <div className="mx-auto max-w-md space-y-4 lg:grid lg:max-w-5xl lg:grid-cols-2 lg:gap-5 lg:space-y-0">
                        {plans.map((plan, index) => (
                            <PricingPlan
                                key={index}
                                name={plan.name}
                                features={plan.features}
                                featured={index === 1}
                                price={plan.pricing[currency]}
                                unit={t`user`}
                                frequency={t`month`}
                            />
                        ))}
                    </div>
                </div>
            </div>
        </div>
    );
}
