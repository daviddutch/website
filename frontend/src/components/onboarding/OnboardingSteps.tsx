import clsx from 'clsx';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
    faCommentsQuestionCheck,
    faHandshake,
    faPersonChalkboard,
    faPresentationScreen,
    faRocket,
} from '@fortawesome/pro-regular-svg-icons';
import { Button } from '@/components/shared/Button';
import { t, Trans } from '@lingui/macro';
import Link from 'next/link';

const steps = [
    {
        name: t`Demo`,
        description: t`The first step of the process is a free online demo. We will take 30 minutes to show you how to manage your tasks and meetings on our collaborative platform. We will also discuss how WEDO will fit your needs.`,
        href: '/demo',
        button: t`Schedule a demo`,
        icon: faPresentationScreen,
        color: 'bg-blue-300',
    },
    {
        name: t`Preparation`,
        description: t`You will take time to list the users you need in your WEDO network. We will help you define the structure of your network by discussing which workspaces, meetings and checklists you want to manage in WEDO. To get started download and fill the Excel file below.`,
        href: '/meet/chris',
        button: t`Contact your CSM`,
        icon: faHandshake,
        color: 'bg-blue-400',
    },
    {
        name: t`Network Setup`,
        description: t`In a conference call with your customer success manager (CSM), you will start using WEDO. He will help you setup the structure of your network, and create your workspaces.`,
        href: '/meet/chris',
        button: t`Contact your CSM`,
        icon: faRocket,
        color: 'bg-blue-500',
    },
    {
        name: 'Training',
        description: t`Plan an onsite training with your CSM to train your colleagues on the usage of WEDO. After the training you will invite your colleagues to join your WEDO network.`,
        href: '/meet/chris',
        button: t`Contact your CSM`,
        icon: faPersonChalkboard,
        color: 'bg-blue-600',
    },
    {
        name: t`FAQ`,
        description: t`Make a second appointment with your CSM three weeks after the training. He will answer to all your questions and make sure your are getting value out of WEDO.`,
        href: '/meet/chris',
        button: t`Contact your CSM`,
        icon: faCommentsQuestionCheck,
        color: 'bg-blue-700',
    },
];

export function OnboardingSteps() {
    return (
        <nav aria-label="Progress">
            <ol role="list" className="overflow-hidden">
                {steps.map((step, stepIdx) => (
                    <li key={step.name} className={clsx(stepIdx !== steps.length - 1 ? 'pb-20' : '', 'relative')}>
                        <>
                            {stepIdx !== steps.length - 1 ? (
                                <div
                                    className={clsx('absolute top-4 left-6 -ml-px mt-0.5 h-full w-0.5', step.color)}
                                    aria-hidden="true"
                                />
                            ) : null}
                            <div className="group relative flex items-start">
                                <span className="h-13 flex items-center">
                                    <span
                                        className={clsx(
                                            'relative z-10 flex h-12 w-12 items-center justify-center rounded-full',
                                            step.color
                                        )}
                                    >
                                        <FontAwesomeIcon
                                            icon={step.icon}
                                            className="text-2xl text-white"
                                            aria-hidden="true"
                                        />
                                    </span>
                                </span>
                                <span className="ml-6 flex min-w-0 flex-col">
                                    <span className="mb-3 text-4xl font-medium text-gray-800">
                                        <Trans id={step.name} />
                                    </span>
                                    <span className="text-lg text-gray-500">
                                        <Trans id={step.description} />
                                    </span>
                                </span>
                                <span className="self-center pl-3">
                                    <Link href={step.href} passHref legacyBehavior>
                                        <Button className="w-52" size={'normal'} variant={'secondary'}>
                                            <Trans id={step.button} />
                                        </Button>
                                    </Link>
                                </span>
                            </div>
                        </>
                    </li>
                ))}
            </ol>
        </nav>
    );
}
