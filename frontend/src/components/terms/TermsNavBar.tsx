import { Disclosure } from '@headlessui/react';
import clsx from 'clsx';
import Link from 'next/link';
import { Trans } from '@lingui/macro';
import { Menu } from '@/types/Menu';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars, faXmark } from '@fortawesome/pro-solid-svg-icons';

export function TermsNavBar({ menu, currentSlug }: { menu: Menu[]; currentSlug: string }) {
    return (
        <Disclosure as="nav" className="bg-gray-800">
            {({ open }) => (
                <>
                    <div className="mx-auto max-w-7xl px-2 sm:px-4 lg:px-4">
                        <div className="relative flex h-16 items-center justify-between">
                            <div className="flex items-center px-2 lg:px-0">
                                <div className="hidden lg:ml-6 lg:block">
                                    <div className="flex space-x-4">
                                        {menu.map((item) => {
                                            let active = item.slug == currentSlug;
                                            return (
                                                <Link
                                                    href={'/' + item.slug}
                                                    key={item.slug}
                                                    className={clsx(
                                                        active
                                                            ? 'rounded-md bg-gray-600 px-3 py-2 text-sm font-medium text-white'
                                                            : 'rounded-md px-3 py-2 text-sm font-medium text-gray-300 hover:bg-gray-700 hover:text-white'
                                                    )}
                                                >
                                                    {item.name}
                                                </Link>
                                            );
                                        })}
                                    </div>
                                </div>
                            </div>
                            <div className="flex lg:hidden">
                                {/* Mobile menu button */}
                                <Disclosure.Button className="inline-flex items-center justify-center rounded-md p-2 text-gray-400 hover:bg-gray-700 hover:text-white focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white">
                                    <span className="sr-only">
                                        <Trans>Open terms menu</Trans>
                                    </span>
                                    {open ? (
                                        <FontAwesomeIcon icon={faXmark} className="block h-6 w-6" aria-hidden="true" />
                                    ) : (
                                        <FontAwesomeIcon icon={faBars} className=" blockh-6 w-6" aria-hidden="true" />
                                    )}
                                </Disclosure.Button>
                            </div>
                        </div>
                    </div>

                    <Disclosure.Panel className="lg:hidden">
                        <div className="space-y-1 px-2 pt-2 pb-3">
                            {/* Current: "bg-gray-900 text-white", Default: "text-gray-300 hover:bg-gray-700 hover:text-white" */}
                            {menu.map((item, index) => {
                                let active = index == 3;
                                return (
                                    <Link href={'/' + item.slug} key={item.slug} legacyBehavior>
                                        <Disclosure.Button
                                            key={item.slug}
                                            as="a"
                                            className={clsx(
                                                active
                                                    ? 'block rounded-md bg-gray-400 px-3 py-2 text-base font-medium text-white'
                                                    : 'block rounded-md px-3 py-2 text-base font-medium text-gray-300 hover:bg-gray-700 hover:text-white'
                                            )}
                                        >
                                            {item.name}
                                        </Disclosure.Button>
                                    </Link>
                                );
                            })}
                        </div>
                    </Disclosure.Panel>
                </>
            )}
        </Disclosure>
    );
}
