import { Trans } from '@lingui/macro';

export const ResponsibleDisclosurePolicy = () => (
    <div>
        <p>
            <Trans>
                Data security is a top priority for WEDO, and WEDO believes that working with skilled security
                researchers can identify weaknesses in any technology.
            </Trans>
        </p>
        <p>
            <Trans>
                If you believe you’ve found a security vulnerability in WEDO’s service, please notify us; we will work
                with you to resolve the issue promptly.
            </Trans>
        </p>
        <h3>
            <Trans>DISCLOSURE POLICY</Trans>
        </h3>
        <ul>
            <li>
                <Trans>
                    If you believe you’ve discovered a potential vulnerability, please let us know by emailing us at
                    <a href="mailto:security@wedo.com">security@wedo.com</a>. We will acknowledge your email within five
                    business days.
                </Trans>
            </li>
            <li>
                <Trans>
                    Provide us with a reasonable amount of time to resolve the issue before disclosing it to the public
                    or a third party. We aim to resolve critical issues within ten business days of disclosure.
                </Trans>
            </li>
            <li>
                <Trans>
                    Make a good faith effort to avoid violating privacy, destroying data, or interrupting or degrading
                    the WEDO service. Please only interact with accounts you own or for which you have explicit
                    permission from the account holder.
                </Trans>
            </li>
        </ul>
        <h3>
            <Trans>EXCLUSIONS</Trans>
        </h3>
        <p>
            <Trans>
                WEDO is providing this service to help ensure a safe and secure environment for all of its users. As
                such, any users believed to be engaging in the below activities will have their user credentials
                immediately deactivated.
            </Trans>
        </p>
        <p>
            <Trans>While researching, we’d like you to refrain from:</Trans>
        </p>
        <ul>
            <li>
                <Trans>Distributed Denial of Service (DDoS)</Trans>
            </li>
            <li>
                <Trans>Spamming</Trans>
            </li>
            <li>
                <Trans>Social engineering or phishing of WEDO employees or contractors</Trans>
            </li>
            <li>
                <Trans>Any attacks against WEDO’s physical property or data centers</Trans>
            </li>
        </ul>
        <p>
            <Trans>Thank you for helping to keep WEDO and our users safe!</Trans>
        </p>
        <h3>
            <Trans>CONTACT</Trans>
        </h3>
        <p>
            <Trans>
                WEDO is always open to feedback, questions, and suggestions. If you would like to talk to us, please
                feel free to email us at <a href="mailto:info@wedo.com">info@wedo.com</a>.
            </Trans>
        </p>
    </div>
);
