import { Trans } from '@lingui/macro';

export const PrivacyPolicy = () => (
    <div>
        <h3>
            <Trans>Preamble (mission statement, scope, changes)</Trans>
        </h3>
        <p>
            <Trans>
                WEDO Sàrl takes your privacy seriously. We are committed to process any personal information we collect
                (hereafter generally referred to as information) in full transparency, in accordance with applicable
                laws and regulations, so as to ensure the accuracy, confidentiality and security of your information.
            </Trans>
        </p>
        <p>
            <Trans>
                This privacy notice applies to the information we collect from you as an individual through our website
                located at <a href="www.wedo.com">wedo.com</a> or mobile applications and related services (altogether
                “Website”).
            </Trans>
        </p>
        <p>
            <Trans>
                By accessing the Website <b>you agree with this Privacy Policy</b> and the processing of personal
                information described herein. If you do not agree, please refrain from accessing this Website.
            </Trans>
        </p>
        <p>
            <Trans>
                This privacy notice is dated May 2021. It may be updated from time to time, in which case you shall be
                notified through a formal notice on our Website of any such modification.
            </Trans>
        </p>

        <h3>
            <Trans>1. What is personal information?</Trans>
        </h3>

        <p>
            <Trans>
                Personal information is any information that can enable the identification of an individual. Information
                may, as such, enable your identification (like your first and last names, email addressor phone number),
                or may enable such identification and provide information when combined with one another. In so far as
                any such information may enable to identify you as a person, whether taken separately or combined with
                one another, it shall be considered as personal information.
            </Trans>
        </p>

        <h3>
            <Trans>2. What information do We collect from you?</Trans>
        </h3>

        <p>
            <Trans>
                Information that we may collect through your browsing on our Website may typically consist of
                information:
            </Trans>
        </p>
        <ul>
            <li>
                <Trans>
                    Information you give us: browsing our website enables you to fill in several registration forms to
                    sign up for a demo, receive our newsletter, subscribe to our service, get support, invite us to call
                    you back or get information related to our products.
                </Trans>
            </li>
        </ul>

        <p>
            <Trans>
                All these instances will require your providing us, at your own will and thus with your consent, some
                personal information in order to meet your request, such as, in particular:
            </Trans>
        </p>
        <ul>
            <li>
                <Trans>your email address;</Trans>
            </li>
            <li>
                <Trans>your first name;</Trans>
            </li>
            <li>
                <Trans>your name;</Trans>
            </li>
            <li>
                <Trans>your address, including your country;</Trans>
            </li>
            <li>
                <Trans>your phone number;</Trans>
            </li>
        </ul>

        <p>
            <Trans>
                Further information might be provided when you exchange with us through our contact page or otherwise,
                in which case such processing will be initiated by you, at your own will and thus with your consent.
            </Trans>
        </p>

        <p>
            <Trans>
                When you subscribe to our service, you shall provide us with all relevant information in order to enable
                us to charge you for the services subscribed to, such as billing information and credit card number.
            </Trans>
        </p>

        <p>
            <Trans>
                Information your Users give to us. The authorized users to which you grant access to our Service will
                share with us the following information on a voluntary basis:
            </Trans>
        </p>
        <ul>
            <li>
                <Trans>their email address;</Trans>
            </li>
            <li>
                <Trans>their first name;</Trans>
            </li>
            <li>
                <Trans>their name;</Trans>
            </li>
            <li>
                <Trans>their picture.</Trans>
            </li>
        </ul>

        <p>
            <Trans>
                We believe it important to draw your attention upon the fact that any document uploaded on our platform
                or customer service system will be processed by our providers, namely Exoscale and Intercom.
            </Trans>
        </p>

        <p>
            <Trans>
                Information we get from your browsing our Website. This information which, other than your internet
                protocol address, does not contain personal information, includes:
            </Trans>
        </p>

        <ul>
            <li>
                <Trans>
                    Device information: for instance your hardware model, operating system version or unique device
                    identifier.
                </Trans>
            </li>

            <li>
                <Trans>
                    Log information: information automatically collected when you browse our Website in server logs
                    include your internet protocol address and device event information (such as crashes, system
                    activity, browser type, browser language as well as date and time of your browsing activities).
                </Trans>
            </li>

            <li>
                <Trans>
                    Cookies and similar technologies: cookies or similar technologies are used to collect and store
                    information when you visit our Website to identify your browser or device. In order to obtain more
                    information and in order to learn how and to what extent you can avoid such automatic collection of
                    information please refer to our Cookie Notice.
                </Trans>
            </li>
        </ul>

        <h3>
            <Trans>3. Why do we collect such information?</Trans>
        </h3>

        <p>
            <Trans>Such information is collected to achieve the following purposes:</Trans>
        </p>

        <ul>
            <li>
                <Trans>To invite you for a demo of our product;</Trans>
            </li>
            <li>
                <Trans>To enable you to access and use our services;</Trans>
            </li>
            <li>
                <Trans>
                    To answer your queries when you contact us directly (either for IT support or otherwise such as
                    through our customer service system);
                </Trans>
            </li>
            <li>
                <Trans>
                    To provide you with information related to our products and services and send you our newsletter;
                </Trans>
            </li>
            <li>
                <Trans>To improve our products and services;</Trans>
            </li>
            <li>
                <Trans>
                    To send you special offers and inform you about events related to our products and services;
                </Trans>
            </li>
            <li>
                <Trans>To inform you about any technical or functional problem our service may encounter.</Trans>
            </li>
        </ul>
        <h3>
            <Trans>4. Who might we share your information with?</Trans>
        </h3>

        <p>
            <Trans>We may disclose and share some of your information with:</Trans>
        </p>

        <ul>
            <li>
                <Trans>
                    Service providers and processors we may engage with to provide services to us on our behalf, such as
                    hosting or support of our Website, the sending of our newsletter, our customer service system
                    provider or payment processors. In providing their services, these providers may access, receive,
                    maintain or otherwise process your information on our behalf. Our contracts with these providers do
                    not permit use of your information for their own purposes, including their marketing purposes.
                    Consistent with applicable legal requirements, we take commercially reasonable steps to require
                    third parties to adequately safeguard your information and only process it in accordance with our
                    instructions.
                </Trans>
            </li>
            <li>
                <Trans>
                    Any third party to which we are ordered to disclose such information as a result of a Court order,
                    governmental authority and/or by law.
                </Trans>
            </li>
            <li>
                <Trans>Any relevant entity for emergency purposes that compel such disclosure.</Trans>
            </li>
        </ul>

        <h3>
            <Trans>5. Where will my information be stored?</Trans>
        </h3>
        <p>
            <Trans>
                We take steps to ensure that the information collected under this privacy notice is processed according
                to the provisions of this notice and the requirements of applicable law wherever the data is located.
            </Trans>
        </p>
        <p>
            <Trans>
                Your information will be stored and normally processed from and in Switzerland. Your personal
                information is stored at Exoscale SA whose data centers belong to Equinix SA. Both companies are seated
                in Switzerland.
            </Trans>
        </p>
        <p>
            <Trans>
                Notwithstanding this, depending upon the location of our service providers, your information may
                occasionally be processed outside of the EEA. Our newsletter is notably sent through Pipedrive, and our
                customer service chat provided by Intercom.
            </Trans>
        </p>
        <p>
            <Trans>
                When we transfer information outside of the European Economic Area, we shall only do so in accordance
                with the law, notably by entering into data transfer agreements and Standard Contractual Clauses with
                the relevant entities. Intercom being a company seated in the United States of America, and while
                Intercom relies on Standard Contractual Clauses to enable the processing of the data exchanged through
                the chat system, we highly recommend you not to upload any document or share sensitive information
                through our customer service system.
            </Trans>
        </p>
        <h3>
            <Trans>6. How do we protect your information?</Trans>
        </h3>

        <p>
            <Trans>We are committed to protecting the security of your personal information.</Trans>
        </p>
        <p>
            <Trans>
                We use a variety of security technologies and procedures to help protect your personal information from
                unauthorized access, use, modification or disclosure to an appropriate level depending upon the
                sensitivity of the information.
            </Trans>
        </p>
        <p>
            <Trans>
                We further contractually ensure that any third party (as described above) equally processes your
                personal information in a secured way to ensure its confidentiality and integrity. See for instance for
                Exoscale: <a href="https://www.exoscale.com/compliance/">www.exoscale.com/compliance/</a>.
            </Trans>
        </p>

        <h3>
            <Trans>7. How long do we retain your information?</Trans>
        </h3>
        <p>
            <Trans>
                We shall retain the information you provided to us as long as you have an account with us, as well as
                after closure of your account so long as the law requires us to, notably from an accounting perspective
                or that we may face a legal claim with regards to your account.
            </Trans>
        </p>
        <p>
            <Trans>
                We shall delete any and all your information at your request within thirty days following such request;
                provided, however, that we may have to keep part or all of your personal information to meet our legal
                obligations, and that any such deletion may be considered as a termination of your subscription and
                prevent you from further benefiting from our services.
            </Trans>
        </p>

        <h3>
            <Trans>8. Social Media and Other Websites</Trans>
        </h3>
        <p>
            <Trans>
                Wherever WEDO Sàrl is present in any of the various social media forums like Facebook, Twitter,
                Pinterest, Instagram, LinkedIn, YouTube, etc., you should be familiar with and understand the tools
                provided by those sites that allow you to make choices about how you share your information in your
                social media profile(s).
            </Trans>
        </p>
        <p>
            <Trans>
                For these forums the privacy practices or policies of these third parties apply, so we encourage you to
                read the applicable privacy notices, terms of use and related information about how your information is
                used in these social media environments.
            </Trans>
        </p>
        <p>
            <Trans>
                Subsequently our Website may contain links to third-party websites, notably through social media feeds;
                if you follow these links, you will exit our Website. While these third-party websites are selected with
                care, we cannot accept liability for the use of your information by these organizations. For more
                information and details, please consult the privacy terms of the websites you are visiting (if such a
                statement is provided).
            </Trans>
        </p>

        <h3>
            <Trans>9. How can I access the information you hold about me?</Trans>
        </h3>

        <p>
            <Trans>
                Should you have any question related to such processing or to this privacy notice, including so as to
                have access to such information and to require, to the extent entitled by applicable law, its
                rectification, erasure, objection or restriction, as well as its portability, you may contact us by
                sending an email to: <a href="mailto:info@wedo.com">info@wedo.com</a>.
            </Trans>
        </p>
        <p>
            <Trans>
                As any individual, we draw your attention upon the fact that you are entitled to lodge a complaint with
                a supervisory authority should you consider that we do not to process your information in accordance
                with applicable laws and regulations and did not respond to your request in a satisfactory way.
            </Trans>
        </p>

        <h3>
            <Trans>10. Governing Law and Jurisdiction</Trans>
        </h3>

        <p>
            <Trans>
                Any dispute arising out of or related to this Policy shall be subject to Swiss Law, and shall be
                submitted to the exclusive jurisdiction of the 1ère Cour d’appel civil du Tribunal cantonal du Canton de
                Fribourg.
            </Trans>
        </p>
    </div>
);
