import { Header } from '@/components/shared/Header';
import { NextPage } from 'next';
import { Footer } from '@/components/shared/Footer';
import React, { ReactElement } from 'react';
import { CallToAction } from '@/components/cta/CallToAction';
import { TermsNavBar } from '@/components/terms/TermsNavBar';

interface PageProps {
    menu: { slug: string; name: string }[];
    currentSlug: string;
    children: ReactElement;
}

export const TermsPage: NextPage<PageProps> = ({ menu, currentSlug, children }) => {
    return (
        <>
            <Header />
            <TermsNavBar menu={menu} currentSlug={currentSlug} />
            {children}
            <CallToAction />
            <Footer />
        </>
    );
};
