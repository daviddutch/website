import { Trans } from '@lingui/macro';
import Link from 'next/link';

export const TermsOfServices = () => (
    <div>
        <p>
            <Trans>
                WEDO Sàrl ("We" or "WEDO") is a Swiss company located at Boulevard de Pérolles 3, 1700 Fribourg
                (CHE-267.068.989) which operates through the website <a href="www.wedo.com">wedo.com</a> ("Site") a
                platform to improve productivity, collaboration and organization of teams and related projects
                ("Service").
            </Trans>
        </p>
        <p>
            <Trans>
                The purpose of these Terms is to define the manner in which You, as a Customer, may use the Service, of
                which Our Appendixes I (Data Processing Agreement) and II (Support Agreement) ("Terms") form an integral
                part. They are supplemented by Our Privacy Policy, which You can find <Link href="/privacy">here</Link>.
            </Trans>
        </p>
        <p>
            <Trans>
                Our Terms may be modified and completed at regular intervals as they evolve, in which case We will
                inform You either on our website or by means of an e-mail the month before their update. The access and
                use of Our Service after You have been informed of this update, as well as the absence of cancellation
                of Your subscription by the end of the Subscription Period, implies Your adherence to the latest version
                of the said Terms.
            </Trans>
        </p>
        <h3>
            <Trans>1. REGISTRATION</Trans>
        </h3>
        <p>
            <Trans>
                1.1 Access and use of Our Service requires the opening of an account through Our Site ("Account").
            </Trans>
        </p>
        <p>
            <Trans>1.2 You provide Us with the following information when You register:</Trans>
        </p>
        <ul>
            <li>
                <Trans>Company name</Trans>
            </li>
            <li>
                <Trans>Billing address</Trans>
            </li>
            <li>
                <Trans>Name and surname of the representative</Trans>
            </li>
            <li>
                <Trans>Email address of the representative</Trans>
            </li>
            <li>
                <Trans>Phone number</Trans>
            </li>
        </ul>
        <p>
            <Trans>
                As a Customer, You warrant to Us the accuracy, sincerity and reliability of the information provided to
                Us, as well as the fact that You are of legal age and entitled to subscribe to Our Service in Your
                country of residence, respectively are entitled as a representative to bind the Customer.
            </Trans>
        </p>
        <p>
            <Trans>
                We draw Your attention to the fact that We reserve the right to take legal action against You, including
                criminal action, in case of identity theft.
            </Trans>
        </p>
        <p>
            <Trans>
                The use of the Service also involves the processing of data relating to Your content and Your Users. We
                refer You to Our Privacy Policy available <Link href="/privacy">here</Link>.
            </Trans>
        </p>
        <p>
            <Trans>
                1.3 Your subscription contract is only validly concluded once you have validated your subscription and
                checked the box for acceptance of our general terms and conditions and the privacy policy, after which
                an invoice confirming this will be sent to You immediately by email to the address that You have given
                Us, containing a summary of the related information ("Confirmation"). You can download Your Contract at
                any time from Your Account or by sending Us a request to this effect, either by mail or by email, as
                indicated in art. 7.6.
            </Trans>
        </p>
        <p>
            <Trans>
                1.4 The opening of the Account confers the right to access and use the Service only to the Customer
                having opened the said account, for a number of users equivalent to the number of licenses for which the
                Customer has subscribed to the Service ("Customer Users"). Customer is responsible for the use of the
                Service by its Customer Users and Third Party Users. As a Customer, it is Your responsibility to ensure
                that Your Customer Users and Third Party Users keep their access codes to the Service confidential
                (login and password) and to inform Us of any abuse in this regard. Any transmission of access to third
                parties without Our prior consent is prohibited and You will be solely responsible for the consequences
                resulting from this, in which case You agree to compensate Us for any damage that We may suffer as a
                result.
            </Trans>
        </p>
        <p>
            <Trans>
                1.5 Access to Our Service is reserved for internal use by the Customer in connection with the management
                of its teams and projects, to the exclusion of any commercial use. Any undeclared and previously
                discussed commercial use will result in the immediate termination of Your subscription and, thereby, the
                possibility of accessing and using Our Service, without prior warning and without any possible refund.
                In case of violation of this section 1.5, You alone will bear the entire responsibility and consequences
                resulting from it, and You commit to compensate Us for any damage that We may suffer as a result.
            </Trans>
        </p>
        <h3>
            <Trans>2. PAYMENT</Trans>
        </h3>
        <p>
            <Trans>
                2.1 The price of the Service is charged on an annual basis at the price per user subscribed by the
                Customer as indicated on the website <a href="www.wedo.com">wedo.com</a> ("Subscription Period").
            </Trans>
        </p>
        <p>
            <Trans>
                2.2 This price is paid in advance by the Customer using the payment methods offered by the platform at
                the beginning of each subscription period.
            </Trans>
        </p>
        <p>
            <Trans>
                2.3 We reserve the right to revise Our pricing conditions, in which case You will be informed by email
                before the expiration of Your Subscription Period; failure to terminate the contract will result in the
                application of the new pricing conditions for any renewal as defined in art. 2.4 or addition of new
                users during the Period as defined in art. 2.7.
            </Trans>
        </p>
        <p>
            <Trans>
                You may ask Us to take advantage of the Service during a trial period previously agreed upon with WEDO,
                in which case You are released from Your obligation to pay during this period, while being subject to
                the provisions of these terms and conditions, in particular Article 3.
            </Trans>
        </p>
        <p>
            <Trans>
                2.4 At the end of the Subscription Period, subject to termination before the end of the Subscription
                Period, the subscription is automatically renewed for the same period and the card used is automatically
                charged.
            </Trans>
        </p>
        <p>
            <Trans>
                2.5 The prices invoiced are inclusive of all taxes and entitle the user to access and use the Service
                within the above-mentioned limits. The prices displayed on our Site are exclusive of tax.
            </Trans>
        </p>
        <p>
            <Trans>
                2.6 The Service is equipped with an online payment security system that allows the Customer to encrypt
                the transmission of his or her banking data. We use external suppliers, Chargebee and Stripe, for the
                payment process and do not store any data relating to your bank details and cards. For more information
                on this subject, We invite You to consult our privacy policy.
            </Trans>
        </p>
        <p>
            <Trans>
                2.7 Once payment has been made, the number of users may not be reduced before the next contractual
                deadline, and no refunds may be made during the Subscription Period to which the Customer has
                subscribed. However, Customer may subscribe to the Service for new users during the Subscription Period,
                in which case these new users will be charged pro rata temporis until the end of the current
                Subscription Period, after which the subscription is renewed for all Users subscribed during the
                Subscription Period in accordance with Article 2.4.
            </Trans>
        </p>
        <p>
            <Trans>
                2.8 In the event that We find that the number of Your Customer Users accessing Our Service exceeds the
                number of Users for which You have subscribed, We reserve the right to invoice the excess of Users pro
                rata temporis under the conditions set forth in Article 2.7.
            </Trans>
        </p>
        <h3>
            <Trans>3. OPERATION OF THE SERVICE</Trans>
        </h3>
        <p>
            <Trans>
                3.1 Once You have entered into Your Agreement, You may access and use Our Service within the limits
                defined by the subscription You have purchased.
            </Trans>
        </p>
        <p>
            <Trans>3.2 You are prohibited from :</Trans>
        </p>
        <p>
            <Trans>Use the Service in an illegal manner or in contravention of these Terms;</Trans>
        </p>
        <ul>
            <li>
                <Trans>
                    Use the Service to harass, abuse, stalk, threaten, defame or otherwise infringe upon the rights of
                    others;
                </Trans>
            </li>
            <li>
                <Trans>
                    Sell, copy, rent, lease, loan, distribute, transfer, make available, or sublicense all or part of
                    the content on the Site, Our Service, or use Our Service for commercial purposes without prior
                    consent;
                </Trans>
            </li>
            <li>
                <Trans>
                    Attempt to gain unauthorized access to Our systems or those of Our subcontractors or engage in any
                    activity that may disrupt, diminish the quality of, or interfere with the performance or
                    functionality of the Site and Our related Service, whether by reverse engineering, attempting to
                    copy, modify or adapt it, or to seek to develop a competing service;
                    <p>
                        <Trans>Remove any trademark or logo belonging to Us, as represented in Our Service;</Trans>
                    </p>
                </Trans>
            </li>
            <li>
                <Trans>
                    Use the Site and/or the Service for abusive purposes by deliberately introducing a virus or any
                    other malicious program;
                </Trans>
            </li>
            <li>
                <Trans>
                    Denigrate Our activities or engage in any behavior that may damage Our reputation, whether through
                    Our Site, Our Service or outside (for example on social networks).
                </Trans>
            </li>
        </ul>
        <p>
            <Trans>
                3.3 We reserve the right to suspend Your subscription and therefore access and use of Our Service in the
                event that:
            </Trans>
        </p>
        <ul>
            <li>
                <Trans>You are in breach of your obligations under these Terms;</Trans>
            </li>
            <li>
                <Trans>
                    We are compelled to do so by security reasons or endangerment of Our systems or Our Service.
                </Trans>
            </li>
        </ul>

        <p>
            <Trans>We will endeavor to inform You in advance of any such suspension to the extent possible.</Trans>
        </p>
        <p>
            <Trans>
                3.4 You understand that We may at any time add new features to Our Service, and if necessary, modify or
                delete them. Your subscription automatically gives You the right to use all the features that are part
                of the Service and included in the subscription You have purchased, it being specified that their
                modification or deletion does not give You the right to any refund other than the right to terminate
                Your subscription in accordance with section 4.1.
            </Trans>
        </p>
        <h3>
            <Trans>4. TERMINATION</Trans>
        </h3>
        <p>
            <Trans>
                4.1. Cancellation at the Customer's initiative. You are entitled to unsubscribe and terminate Your
                subscription at any time by sending an email to info@wedo.com, it being specified that Your cancellation
                will then take place for the next contractual term. Upon expiration of Your subscription, Your Account
                will be disabled and access and use of Our Service will be terminated. No refund will be given.
            </Trans>
        </p>
        <p>
            <Trans>
                4.2. Termination at the initiative of WEDO. We also have the right to terminate Your subscription for
                the next contractual term, in which case We will inform You by sending an email to the address that You
                provided to Us during Your registration. In case of violation of these Terms, in particular but not
                limited to art. 3.2, We reserve the right to suspend Your Account for the necessary time needed for the
                verifications that may be carried out and, if necessary, to terminate Your Contract with immediate
                effect; whether it is a suspension or a termination, You will be informed by sending a prior notice to
                this same email address
            </Trans>
        </p>
        <p>
            <Trans>
                4.3 Early Termination. WEDO shall have the right to terminate Your subscription at any time in the event
                that You fail to remedy a breach of these terms and conditions ten (10) days after We have brought such
                breach to Your attention in writing (email being deemed written).
            </Trans>
        </p>
        <p>
            <Trans>
                4.4 Automatic Termination. Your subscription will automatically terminate in the event that either party
                ceases to exist, is declared bankrupt, or is subject to any other event of proven insolvency.
            </Trans>
        </p>
        <p>
            <Trans>
                4.5 Access to and use of the Service is no longer possible from the moment the termination given in
                application of this art. 4 takes effect.
            </Trans>
        </p>
        <p>
            <Trans>
                4.6 Your content will be deleted from the servers hosting it within ninety (90) days following the
                expiration of Your subscription, except for back-ups that will be deleted following the usual cleaning
                cycle.
            </Trans>
        </p>
        <p>
            <Trans>
                4.7 Termination by exercising in application to this article shall not entitle you to any refund.
            </Trans>
        </p>
        <h3>
            <Trans>5. INTELLECTUAL PROPERTY</Trans>
        </h3>
        <p>
            <Trans>
                5.1 WEDO owns all intellectual property rights in our Site and its contents and in the related Service,
                which rights also include the related know-how.
            </Trans>
        </p>
        <p>
            <Trans>
                5.2 The license granted to You to access and use Our Service is non-exclusive, non-transferable. It is
                granted to You in consideration of the remuneration provided for in section 2, for the duration of Your
                subscription and as long as the latter is not terminated in accordance with the terms of these
                conditions.
            </Trans>
        </p>
        <p>
            <Trans>
                5.3 You are prohibited from using our trademarks and/or logos without our written permission, as well as
                from copying or reproducing our Site.
            </Trans>
        </p>
        <p>
            <Trans>
                5.4 The content that You, respectively Your Users, upload and share within the framework of the use of
                Our Service belongs to You and You warrant to Us that You own it or have obtained the rights for it from
                its owners. You grant Us a license to such content to the fullest extent necessary to enable You to use
                the Service, to the exclusion of any other use.
            </Trans>
        </p>
        <p>
            <Trans>
                5.5 We welcome any feedback or suggestions that You may provide in relation to Our Service, and We shall
                be entitled to use or not to use such feedback or suggestions as We see fit and without being liable to
                You in any way whatsoever, including financially.
            </Trans>
        </p>
        <h3>
            <Trans>6. LIABILITY AND WARRANTY</Trans>
        </h3>
        <p>
            <Trans>
                6.1 WEDO shall be exempt from any liability arising from the performance of the Contract between Us to
                the fullest extent permitted by applicable law.
            </Trans>
        </p>
        <p>
            <Trans>
                6.2 Notwithstanding art. 6.1, We will make reasonable efforts to ensure that the Service is available 24
                hours a day, 7 days a week, but shall not be held responsible for any unavailability due to bugs,
                technical constraints and other maintenance operations, which shall not give rise to any right to any
                reimbursement whatsoever
            </Trans>
        </p>
        <p>
            <Trans>
                6.3 The Site may contain links to other sites that are neither edited nor controlled by WEDO and for
                which we cannot be held responsible.
            </Trans>
        </p>
        <h3>
            <Trans>7. PRIVACY</Trans>
        </h3>
        <p>
            <Trans>
                7.1 We commit to keep secret and not to disclose to third parties any information communicated to Us by
                the Customer or of which We may have become aware in the course of the performance of the Contract, in
                particular related to the content put online by the Customer and its Users in the context of the use of
                the Service (hereinafter referred to as "the Information"), subject to (1) Art. 7.2, (2) in the event
                that such a request is made to Us by an authority competent to do so, (3) in the event that such
                disclosure is necessary for Us to comply with Our legal obligations and (4) in the event that such
                Information has fallen into the public domain or has been made known to third parties through no fault
                of Our own. The Information will remain the property of the Customer.
            </Trans>
        </p>
        <p>
            <Trans>
                7.2 We undertake not to use the Information for any purpose other than for the proper performance of the
                Services.
            </Trans>
        </p>
        <p>
            <Trans>
                7.3 We undertake to disclose the Information only to those of its employees and third parties who are
                directly and necessarily involved in the performance of the Service, it being understood that Our
                employees and subcontractors are bound by obligations similar to those mentioned in this Clause.
            </Trans>
        </p>
        <h3>
            <Trans>8. MISCELLANEOUS</Trans>
        </h3>
        <p>
            <Trans>
                8.1 FORCE MAJEURE. The parties agree that, in the event that performance of the Contract proves
                impossible due to an event of force majeure, i.e., an unforeseeable event beyond the control of either
                party, neither party may be held liable for the non-performance, failure or delay in performance of any
                of its obligations that may be due to the occurrence of said event. The performance of the Contract and
                the resulting obligations shall thus be suspended for as long as the force majeure event lasts, it being
                specified that the payment made for the current month during which the force majeure event occurred
                shall nevertheless be retained.
            </Trans>
        </p>
        <p>
            <Trans>
                8.2 INVALIDITY. In the event that any provision of these Terms is held to be invalid, such invalidity
                shall not affect the validity of the remaining Terms. The invalid provision will be replaced and
                interpreted in such a way as to ensure its validity by a provision that is as close as possible in
                spirit to the invalid provision.
            </Trans>
        </p>
        <p>
            <Trans>
                8.3 TRANSERT. You are prohibited from transferring the Agreement without Our prior written consent.
            </Trans>
        </p>
        <p>
            <Trans>
                8.4 REFERENCE. WEDO is entitled to use Your trademark or logo as a reference to promote Our Service to
                third parties.
            </Trans>
        </p>
        <p>
            <Trans>
                8.5 BETA VERSION. WEDO may invite You to take advantage of and test a Beta version under evaluation. In
                the event that You agree to test a Beta version, You understand that the said version is under
                evaluation and that You agree to test it as is, at Your own risk, without the benefit of support, the
                stability of the production version and all related security guarantees.
            </Trans>
        </p>
        <p>
            <Trans>8.6 COMMUNICATION.</Trans>
        </p>
        <Trans>
            <p>Any communication to be sent to WEDO shall be:</p>
            <ul>
                <li>By email to: info@wedo.com.</li>
                <li>By mail to the address: WEDO Sàrl, Boulevard de Pérolles 3, 1700 Fribourg.</li>
            </ul>
        </Trans>
        <p>
            <Trans>
                8.7 APPLICABLE LAW AND COMPETENT COURT. The validity and performance of these Terms and Conditions and
                the Contract shall be governed by Swiss law, to the exclusion of its rules arising from the Federal Law
                on Private International Law. Any dispute arising directly or indirectly from these Terms and the
                Contract shall be submitted to the exclusive jurisdiction of the 1st Civil Court of Appeal of the Canton
                of Fribourg, to which the parties hereby irrevocably agree to submit.
            </Trans>
        </p>
    </div>
);
