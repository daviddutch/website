import { Trans } from '@lingui/macro';

export const DataProcessingAgreement = () => (
    <div>
        <p>
            <Trans>
                This Data Processing Agreement (“Agreement”) sets out the terms, requirements and conditions on which
                WEDO Sàrl (“WEDO”) will process Personal Data when providing services to you as a Client having
                subscribed to our services.
            </Trans>
        </p>

        <p>
            <Trans>This Agreement is an annex to the WEDO Terms of Services and forms an integrant part of it.</Trans>
        </p>

        <h3>
            <Trans>1. DEFINITIONS</Trans>
        </h3>
        <p>
            <Trans>For the purposes of this Agreement:</Trans>
        </p>

        <p>
            <Trans>
                1.1 <b>“Applicable Data Protection Law”</b> means privacy laws applicable to WEDO as the Processor of
                the Personal Data, or Client as Data Controller of Personal data, including but not limited to the
                Federal Data Protection Act of 1992 and its forthcoming revised version, the Regulation (EU) 2016/679 of
                the European Parliament and of the Council of 27 April 2016 (General Data Protection Regulation, ‘GDPR’)
                and any further implementation, amendment, replacements or renewals thereof (collectively called the “EU
                Legislation”), as well as all binding national laws implementing the EU Legislation and other binding
                data protection or data security directives, laws, regulations and rulings valid at the given time.
            </Trans>
        </p>
        <p>
            <Trans>
                1.2 <b>“Processing”</b> or <b>“Process”</b> means any operation or set of operations which is performed
                on Personal Data or on sets of Personal Data, whether or not by automated means, such as collection,
                recording, organization, structuring, storage, adaptation or alteration, retrieval, consultation, use,
                disclosure by transmission, dissemination or otherwise making available, alignment or combination,
                restriction, erasure or destruction.
            </Trans>
        </p>
        <p>
            <Trans>
                1.3 The words <b>“Data Controller“</b>, <b>“Data Subject“</b>, <b>“Personal Data“</b>,{' '}
                <b>“Data Processor”</b> and <b>“Sub-Processor“</b>, <b>“Processing”</b> or <b>“Process”</b> shall be
                construed in accordance with the definition of <b>“Processing“</b>.
            </Trans>
        </p>

        <h3>
            <Trans>2. PROCESSING OF PERSONAL DATA</Trans>
        </h3>
        <p>
            <Trans>WEDO shall:</Trans>
        </p>
        <p>
            <Trans>
                2.1 process the Client’s Personal Data exclusively in accordance with the provisions of this Agreement
                and only pursuant to the Client’s written instructions, and use the Personal Data for the sole purpose
                of the performance of this Agreement and no other use (e.g., for commercial purposes), as set out in
                Appendix 1;
            </Trans>
        </p>
        <p>
            <Trans>
                2.2 ensure that persons authorised to Process the Personal Data have committed themselves to
                confidentiality or are under an appropriate statutory obligation of confidentiality;
            </Trans>
        </p>
        <p>
            <Trans>
                2.3 adopt the security measures required under Applicable Data Protection Law as detailed by WEDO from
                time to time, including the following: (i) resiliency of systems and services related to the processing
                activities; (ii) ability to timely re-store the availability and the access to Personal Data, where a
                physical or technical incident occurs; (iii) procedures in order to test, verify and assess regularly
                the effectiveness of the technical and organizational measures aimed at ensuring the security of
                Personal Data Processing activities;
            </Trans>
        </p>
        <p>
            <Trans>
                2.4 inform the Client of any new Sub-Processor, other than the approved Sub-Processors listed in
                Appendix 1. The Client is entitled to object to such appointment; provided, however, that, in this case,
                its sole right shall be to terminate its Agreement with WEDO. Each Sub-Processor shall be bound by the
                same obligations detailed in this clause 2.2;
            </Trans>
        </p>
        <p>
            <Trans>2.5 assist the Client to respond to requests for exercising the Data Subjects’ rights;</Trans>
        </p>
        <p>
            <Trans>
                2.6 notify the Client immediately (and in any event within a period not exceeding 48 hours) after
                becoming aware of any actual or suspected breach of Personal Data or any security breach leading to, in
                an accidental or unlawful manner, the deletion, loss, alteration, unauthorized disclosure of the
                Personal Data transmitted, stored or processed in any other manner, or the unauthorized access to such
                Personal Data, as well as if, in its opinion, the Client’s instruction should infringe the Data
                Protection Laws;
            </Trans>
        </p>
        <p>
            <Trans>
                2.7 upon termination of this Agreement for whatever reason, cease Processing any Personal Data on behalf
                of the Client and, at the Client’s option, either forthwith return to the Client all the Personal Data
                that it is Processing or has Processed on behalf of the Client and/or any documentation or materials
                containing them and any copies thereof, or according to the Client’s instructions, securely delete or
                destroy, the Personal Data within thirty (30) calendar days of being requested to do so by the Client.
            </Trans>
        </p>

        <h3>
            <Trans>3. TERMINATION</Trans>
        </h3>
        <p>
            <Trans>
                This Agreement shall terminate in accordance with the terms set out in the WEDO Terms of Service.
            </Trans>
        </p>

        <h3>
            <Trans>APPENDIX 1: DESCRIPTION OF PROCESSING</Trans>
        </h3>
        <h4>
            <Trans>Subject matter and duration of the Processing of Customer Personal Data</Trans>
        </h4>
        <p>
            <Trans>
                The subject matter and duration of the Processing of the Client Personal Data are set out in the
                Agreement.
            </Trans>
        </p>
        <p>
            <Trans>
                The subject matter of the data processing is the collection, processing and use of Personal Data for the
                Client to the extent this is required for carrying out the Agreement and is limited to the term of the
                Agreement.
            </Trans>
        </p>
        <h4>
            <Trans>The nature and purpose of the Processing of Client Personal Data</Trans>
        </h4>
        <p>
            <Trans>
                Responding to customer requests, sending notifications and reminders about activity in WEDO, sending
                transactional emails such as password recovery.
            </Trans>
        </p>
        <h4>
            <Trans>The types of Client Personal Data to be Processed</Trans>
        </h4>
        <p>
            <Trans>
                Users first name, last name, email address and optionally: job title, phone number, location and
                organization name. Client billing address.
            </Trans>
        </p>
        <h4>
            <Trans>The categories of Data Subject to whom the Client Personal Data relates</Trans>
        </h4>
        <p>
            <Trans>
                Employees of the Client and other specific invited users such as business partners or customers.
            </Trans>
        </p>
        <h4>
            <Trans>Service Data</Trans>
        </h4>
        <p>
            <Trans>
                <b>“Service Data”</b> is Personal Data or other information that Users: input directly into the
                Platform; create within the Platform; send to the Platform; or provide to WEDO through authorized
                methods as part of other Service.
            </Trans>
        </p>

        <h3>
            <Trans>APPENDIX 2: APPROVED SUBPROCESSORS</Trans>
        </h3>
        <h4>
            <Trans>Contractors</Trans>
        </h4>
        <p>
            <Trans>
                WEDO uses certain subcontractors to assist in the operations necessary to provide the WEDO Services. The
                following is a list of the names and locations of material third-party subcontractors. Subcontractors do
                not have access to Service Data.
            </Trans>
        </p>

        <table>
            <thead>
                <tr>
                    <th>
                        <Trans>No.</Trans>
                    </th>
                    <th>
                        <Trans>Subprocessor</Trans>
                    </th>
                    <th>
                        <Trans>Scope</Trans>
                    </th>
                    <th>
                        <Trans>Location</Trans>
                    </th>
                    <th>
                        <Trans>Compliance</Trans>
                    </th>
                    <th>
                        <Trans>Data subjects</Trans>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1</td>
                    <td>Equinix (Suisse) GmbH</td>
                    <td>
                        <Trans>Data center operator</Trans>
                    </td>
                    <td>
                        <Trans>Switzerland</Trans>
                    </td>
                    <td>ISO 27001, GDPR</td>
                    <td>
                        <Trans>Service Data</Trans>
                    </td>
                </tr>
            </tbody>
        </table>

        <h4>
            <Trans>Infrastructure Sub-processors – Service Data Storage and Processing</Trans>
        </h4>
        <p>
            <Trans>
                WEDO owns or controls access to the infrastructure that WEDO uses to host and process Service Data
                submitted through the Services. Currently, the WEDO production infrastructure used for hosting Service
                Data for the Services are located in co-location facilities in Switzerland and in the infrastructure
                sub-processors listed below. The following table describes the countries and legal entities engaged by
                WEDO in the storage of Service Data. WEDO also uses additional services provided by these sub-processors
                to process Service Data as needed to provide the Services.
            </Trans>
        </p>
        <table>
            <thead>
                <tr>
                    <th>
                        <Trans>No.</Trans>
                    </th>
                    <th>
                        <Trans>Subprocessor</Trans>
                    </th>
                    <th>
                        <Trans>Scope</Trans>
                    </th>
                    <th>
                        <Trans>Location</Trans>
                    </th>
                    <th>
                        <Trans>Compliance</Trans>
                    </th>
                    <th>
                        <Trans>Data subjects</Trans>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>2</td>
                    <td>Akenes SA (Exoscale)</td>
                    <td>
                        <Trans>Hosting</Trans>
                    </td>
                    <td>
                        <Trans>Switzerland</Trans>
                    </td>
                    <td>ISO 27001, GDPR</td>
                    <td>
                        <Trans>Service Data</Trans>
                    </td>
                </tr>
            </tbody>
        </table>

        <h4>
            <Trans>Service Specific Sub-processors</Trans>
        </h4>
        <p>
            <Trans>
                WEDO engages a few third party service providers that process Personal Data on behalf of WEDO in
                connection with the provision of our Services to customers.
            </Trans>
        </p>
        <table>
            <thead>
                <tr>
                    <th>
                        <Trans>No.</Trans>
                    </th>
                    <th>
                        <Trans>Subprocessor</Trans>
                    </th>
                    <th>
                        <Trans>Scope</Trans>
                    </th>
                    <th>
                        <Trans>Location</Trans>
                    </th>
                    <th>
                        <Trans>Compliance</Trans>
                    </th>
                    <th>
                        <Trans>Data subjects</Trans>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>3</td>
                    <td>Intercom</td>
                    <td>
                        <Trans>Chat service for customer support</Trans>
                    </td>
                    <td>
                        <Trans>United States</Trans>
                    </td>
                    <td>ISO 27001, SOC 2 Type II, GDPR</td>
                    <td>
                        <Trans>First name, last name and email address</Trans>
                    </td>
                </tr>
                <tr>
                    <td>4</td>
                    <td>Mailgun</td>
                    <td>
                        <Trans>Email service provider</Trans>
                    </td>
                    <td>
                        <Trans>European Union</Trans>
                    </td>
                    <td>
                        <Trans>GDPR</Trans>
                    </td>
                    <td>
                        <Trans>First name, last name and email address</Trans>
                    </td>
                </tr>
                <tr>
                    <td>5</td>
                    <td>Pipedrive</td>
                    <td>
                        <Trans>Newsletter</Trans>
                    </td>
                    <td>
                        <Trans>United States</Trans>
                    </td>
                    <td>ISO 27001, GDPR</td>
                    <td>
                        <Trans>First name, last name and email address</Trans>
                    </td>
                </tr>
                <tr>
                    <td>6</td>
                    <td>Twilio</td>
                    <td>
                        <Trans>SMS delivery service</Trans>
                    </td>
                    <td>
                        <Trans>United States</Trans>
                    </td>
                    <td>ISO 27001, GDPR</td>
                    <td>
                        <Trans>Phone number</Trans>
                    </td>
                </tr>
                <tr>
                    <td>7</td>
                    <td>Chargebee</td>
                    <td>
                        <Trans>Subscription management</Trans>
                    </td>
                    <td>
                        <Trans>European Union</Trans>
                    </td>
                    <td>ISO 27001, SOC 2 Type II, GDPR</td>
                    <td>
                        <Trans>Billing address</Trans>
                    </td>
                </tr>
                <tr>
                    <td>8</td>
                    <td>Sentry</td>
                    <td>
                        <Trans>Error Tracking</Trans>
                    </td>
                    <td>
                        <Trans>United States</Trans>
                    </td>
                    <td>GDPR</td>
                    <td>
                        <Trans>No user data</Trans>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
);
