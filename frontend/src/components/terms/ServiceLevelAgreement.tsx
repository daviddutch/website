import { Trans } from '@lingui/macro';

export const ServiceLevelAgreement = () => (
    <div>
        <p>
            <Trans>
                This Service Level Agreement (“SLA”) sets out the terms, requirements and conditions on which WEDO Sàrl
                will provide you support with regards to your access and use of our services.
            </Trans>
        </p>

        <p>
            <Trans>This Agreement is an annex to the WEDO Terms of Service and forms an integrant part of it.</Trans>
        </p>

        <h3>
            <Trans>1. DEFINITIONS</Trans>
        </h3>
        <p>
            <Trans>For the purposes of this SLA:</Trans>
        </p>

        <p>
            <Trans>
                <b>“Support”</b> means assistance to the Client’s staff.
            </Trans>
        </p>

        <p>
            <Trans>
                <b>“Functional Updates”</b> means enhanced and improved versions of the code intended for permanent use.
                Functional Updates includes any such code whether referred to as updates, upgrades, releases, versions
                or by any other name and whether or not released generally to customers.
            </Trans>
        </p>

        <p>
            <Trans>
                <b>“Maintenance Updates”</b> means revised and corrected versions of code intended for permanent use.
                Maintenance Updates includes any such code whether referred to as updates, upgrades, releases, versions
                or by any other name and whether or not released generally to customers.
            </Trans>
        </p>

        <p>
            <Trans>
                <b>“Materials”</b> means Workarounds, Documentation Updates, Databases and Bulletins as defined in
                Clause 4 below.
            </Trans>
        </p>

        <p>
            <Trans>
                <b>“Patches”</b> means code intended as a temporary solution of a specific malfunction. Patches includes
                any such code whether referred to as patch, bug-fix, correction or by any other name.
            </Trans>
        </p>

        <p>
            <Trans>
                <b>“Program Error”</b> means a defect in the code of our product which is reproducible and which causes
                such product to malfunction, in particular not to function substantially in conformance with Product
                documentation.
            </Trans>
        </p>

        <p>
            <Trans>
                <b>“Support Issue”</b> means any malfunctions, defects, or missing or deviant functionality of products,
                including Program Errors, experienced by Client during productive Use of Products.
            </Trans>
        </p>

        <h3>
            <Trans>2. ACQUISITION OF MAINTENANCE</Trans>
        </h3>
        <p>
            <Trans>
                In consideration of the Client’s subscription to our services, WEDO shall provide the Client with
                Updates and/or Support, as the case may be.
            </Trans>
        </p>
        <p>
            <Trans>
                In case of third party products, i.e. products that are owned by third parties and whose usage is
                licensed by WEDO in Client’s favour, WEDO shall specify, in the Order Form[PG1] , whether the Support is
                delivered by WEDO itself or whether WEDO procures Support to be provided by the third party. In the
                latter case, the Support terms shall be subject to the relevant third party’s SLA’s terms
            </Trans>
        </p>

        <h3>
            <Trans>3. SUPPORT</Trans>
        </h3>
        <p>
            <Trans>
                In providing Support, WEDO is responsible for the resolution of Program Errors, and other Support
                Issues, in accordance with the terms of this SLA.
            </Trans>
        </p>
        <p>
            <Trans>Client shall:</Trans>
        </p>
        <ul>
            <li>Designate an individual for coordination of Support.</li>
            <li>Assist Supplier in the reproduction of Program Errors reported.</li>
        </ul>
        <h3>
            <Trans>4. UPDATES</Trans>
        </h3>
        <p>
            <Trans>
                In providing Updates, Supplier shall deliver software Patches, Maintenance and Functional Updates or
                Successor Products to Customers as they become available. In addition, Supplier shall provide the
                following Materials:
            </Trans>
        </p>

        <ul>
            <li>Methods or procedures for avoiding or bypassing Program Errors (‘Workarounds’).</li>
            <li>
                Amendments to Product documentation for completing, correcting or clarifying use and operating
                instructions (‘Documentation Updates’).
            </li>
            <li>Access to on-line databases of documented Product issues and their resolution (‘Database’).</li>
            <li>
                Information on pending Maintenance Updates and Functional Updates, including information on the Program
                Errors or missing functionality they are meant to address (‘Bulletins’).
            </li>
        </ul>
        <h3>
            <Trans>5. AVAILABILITY OF CHAT SUPPORT</Trans>
        </h3>
        <p>
            <Trans>Unless otherwise agreed, chat Support will be available as follows:</Trans>
        </p>

        <table>
            <thead>
                <tr>
                    <th>
                        <Trans>Hours & Working Days (Swiss time)</Trans>
                    </th>
                    <th>
                        <Trans>Languages</Trans>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <Trans>Monday to Friday, 8:00 to 17:00</Trans>
                    </td>
                    <td>
                        <Trans>French, German, English</Trans>
                    </td>
                </tr>
            </tbody>
        </table>

        <h3>
            <Trans>6. REPORTING AND CLASSIFICATION OF SUPPORT ISSUES</Trans>
        </h3>
        <p>
            <Trans>
                Any Support Issue reported by a Client which cannot be resolved immediately shall be acknowledged by
                WEDO and allocated an identifying reference number (‘Acknowledgement’).
            </Trans>
        </p>
        <p>
            <Trans>
                Support Issues, in particular Program Errors, shall be classified, based on Customer’s evaluation, as
                follows:
            </Trans>
        </p>

        <h4>
            <Trans>Severity Definitions</Trans>
        </h4>
        <table>
            <thead>
                <tr>
                    <th>
                        <Trans>Priority</Trans>
                    </th>
                    <th>
                        <Trans>Definition</Trans>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td className="whitespace-nowrap">Critical 1</td>
                    <td>
                        <Trans>
                            Service to many staff is unavailable or seriously impaired. The loss of service has an
                            extremely high impact on the business process and may directly result in lost profit,
                            revenue or risk management i.e. enterprise critical e.g. A major infrastructure failure
                            (e.g. loss of central service, all communication to a site, or site service etc). There is
                            no readily available alternative to perform work. Service may be restored temporarily
                            through a Workaround until a more permanent resolution is completed. The incident has a high
                            impact on the business process. Consequences are clearly visible but can be contained. e.g.
                            a major service failure with widespread effect, but not sufficiently critical to be
                            immediately business affecting (as defined above). An otherwise Critical incident for which
                            backup/Workaround procedures are in place and operating, but for which there is still some
                            risk of recurrence.
                        </Trans>
                    </td>
                </tr>
                <tr>
                    <td className="whitespace-nowrap">Urgent 2</td>
                    <td>
                        <Trans>
                            Incident has impact on the business process. Consequences are seen e.g. Department is
                            concerned, loss of service to more than one user. Service failure affecting one or more
                            users, though not widespread, where there is no adequate Workaround, or which has the
                            potential to cause major business impact if not resolved. During core business hours, will
                            receive attention within 4 working hours by assigned analyst/resolution Group.
                        </Trans>
                    </td>
                </tr>
                <tr>
                    <td className="whitespace-nowrap">Standard 3</td>
                    <td>
                        <Trans>
                            Incident has some impact on business process. Consequences are seen but there is an
                            alternative way to perform work. The loss of service does not result in a major impact on
                            the business e.g. User incident where the work process is significantly hindered. Office
                            hours only Incident has impact on business process. Consequences are seen but there is a
                            readily available alternative to perform work. The loss of service does not result in any
                            direct impact on the business e.g. User incident where the work process is affected but not
                            enough to prevent it.
                        </Trans>
                    </td>
                </tr>
            </tbody>
        </table>

        <h4>
            <Trans>Severity Levels and Response and Resolution Times</Trans>
        </h4>

        <table>
            <thead>
                <tr>
                    <th>
                        <Trans>Critical 1</Trans>
                    </th>
                    <th>
                        <Trans>Urgent 2</Trans>
                    </th>
                    <th>
                        <Trans>Standard 3</Trans>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <Trans>
                            Initial Response Time during Core Business Hours (Ticket is set to Work in Progress status)
                        </Trans>
                    </td>
                    <td>
                        <Trans>1 hour</Trans>
                    </td>
                    <td>
                        <Trans>4 hours</Trans>
                    </td>
                </tr>
                <tr>
                    <td>
                        <Trans>Time to Resolve (Ticket is set to Resolve)</Trans>
                    </td>
                    <td>
                        <Trans>One business day (anticipated maximum time barring exceptional cases)</Trans>
                    </td>
                    <td>
                        <Trans>Five business days (anticipated maximum time barring exceptional cases)</Trans>
                    </td>
                </tr>
            </tbody>
        </table>

        <p>
            <Trans>
                Acknowledgements of Support Issues issued by WEDO shall contain the agreed classification and a first
                estimate of temporary resolution time or next communication.
            </Trans>
        </p>
        <p>
            <Trans>
                Requests raised by Client for changes to Products which are not Support Issues shall be dealt with as
                requests for Services.
            </Trans>
        </p>
        <p>
            <Trans>
                Parties agree that, considering the services at stake, Support issues will in most instances be
                classified as either Urgent 2 or Standard 3, but should normally not be considered as Critical 1.
            </Trans>
        </p>

        <h3>
            <Trans>7. SUPPORT ISSUE RESOLUTION</Trans>
        </h3>
        <p>
            <Trans>
                WEDO shall analyse Support Issues and undertake their resolution, in accordance with resolution methods
                and times set out in this Clause 7 and in Clause 6 above.
            </Trans>
        </p>
        <p>
            <Trans>
                Where a Support Issue is caused by a Program Error, it shall be finally resolved by supply of a
                Maintenance or Functional Update, but shall be temporarily resolved by a Patch or a Workaround, subject
                to the following restrictions:
            </Trans>
        </p>

        <h4>
            <Trans>Delivery of Permanent Error Resolution</Trans>
        </h4>

        <table>
            <thead>
                <tr>
                    <th>
                        <Trans>Severity</Trans>
                    </th>
                    <th>
                        <Trans>Temporary Resolution</Trans>
                    </th>
                    <th>
                        <Trans>Permanent Resolution</Trans>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <Trans>Critical 1</Trans>
                    </td>
                    <td>
                        <Trans>Patch (or Workaround, if agreed)</Trans>
                    </td>
                    <td>
                        <Trans>Next Maintenance Update</Trans>
                    </td>
                </tr>
                <tr>
                    <td>
                        <Trans>Urgent 2</Trans>
                    </td>
                    <td>
                        <Trans>Workaround (or Patch, if agreed)</Trans>
                    </td>
                    <td>
                        <Trans>Next Maintenance Update</Trans>
                    </td>
                </tr>
                <tr>
                    <td>
                        <Trans>Standard 3</Trans>
                    </td>
                    <td>
                        <Trans>Workaround (optional)</Trans>
                    </td>
                    <td>
                        <Trans>
                            Next Maintenance Update unless this constitutes a disproportionate effort, otherwise next
                            Functional Update
                        </Trans>
                    </td>
                </tr>
            </tbody>
        </table>

        <p>
            <Trans>Functional Updates shall incorporate all prior Maintenance Updates.</Trans>
        </p>
        <p>
            <Trans>
                WEDO shall provisionally close a Support Issue upon delivery of a temporary resolution, if any, and
                finally close it upon delivery of the Maintenance Update or Functional Update, as the case may be, which
                resolves the Issue permanently.
            </Trans>
        </p>

        <h3>
            <Trans>8. EXCLUSIONS</Trans>
        </h3>
        <p>
            <Trans>Support Issue resolution is not due under Maintenance if the Support Issue is caused by:</Trans>
        </p>

        <ul>
            <li>
                <Trans>another application used by Customer;</Trans>
            </li>
            <li>
                <Trans>any operation or use disregarding documentation or other advice given under Support;</Trans>
            </li>
            <li>
                <Trans>
                    malfunctions of the equipment, electricity breakdown or other events independent of the operation
                    and use of the services;
                </Trans>
            </li>
            <li>
                <Trans>any accidental, malicious or negligent misuse or damage.</Trans>
            </li>
        </ul>
    </div>
);
