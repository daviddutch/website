import { Trans } from '@lingui/macro';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFacebook, faGithub, faInstagram, faLinkedin, faTwitter } from '@fortawesome/free-brands-svg-icons';
import Image from 'next/image';
import { Employee } from '@/types/Employee';
import { StrapiImage } from '@/components/shared/StrapiImage';

type Props = {
    people: Employee[];
};

export function Team({ people }: Props) {
    return (
        <div className="bg-gray-900">
            <div className="mx-auto max-w-7xl py-12 px-4 sm:px-6 lg:px-8 lg:py-24">
                <div className="space-y-12">
                    <div className="space-y-5 sm:space-y-4 md:max-w-xl lg:max-w-3xl xl:max-w-none">
                        <h2 className="text-3xl font-bold tracking-tight text-white sm:text-4xl">
                            <Trans>Meet our the team behind WEDO</Trans>
                        </h2>
                        <p className="text-xl text-gray-300">
                            <Trans>
                                The team at WEDO is made up of professional individuals who excel in their own area of
                                expertise to help deliver the best service possible.
                            </Trans>
                        </p>
                    </div>
                    <ul
                        role="list"
                        className="space-y-4 sm:grid sm:grid-cols-2 sm:gap-6 sm:space-y-0 lg:grid-cols-3 lg:gap-8"
                    >
                        {people.map((person) => (
                            <li
                                key={person.attributes.email}
                                className="rounded-lg bg-gray-800 py-10 px-6 text-center xl:px-10 xl:text-left"
                            >
                                <div className="space-y-6 xl:space-y-10">
                                    {person.attributes.photo.data ? (
                                        <StrapiImage
                                            className="mx-auto h-40 w-40 rounded-full xl:h-56 xl:w-56"
                                            media={person.attributes.photo}
                                        />
                                    ) : (
                                        <Image
                                            className="mx-auto h-40 w-40 rounded-full xl:h-56 xl:w-56"
                                            src={'/images/unknown.png'}
                                            height={1080}
                                            width={1080}
                                            alt={'No profile picture'}
                                        />
                                    )}
                                    <div className="space-y-2 xl:flex xl:items-center xl:justify-between">
                                        <div className="space-y-1 text-lg font-medium leading-6">
                                            <h3 className="text-white">
                                                {person.attributes.firstName} {person.attributes.lastName}
                                            </h3>
                                            <p className="text-blue-400">{person.attributes.title}</p>
                                        </div>

                                        <ul role="list" className="flex justify-center space-x-5">
                                            {person.attributes.twitterUrl && (
                                                <li>
                                                    <a
                                                        href={person.attributes.twitterUrl}
                                                        target={'_blank'}
                                                        rel="noopener noreferrer"
                                                        className="text-gray-400 hover:text-gray-300"
                                                    >
                                                        <span className="sr-only">Twitter</span>
                                                        <FontAwesomeIcon icon={faTwitter} />
                                                    </a>
                                                </li>
                                            )}
                                            {person.attributes.facebookUrl && (
                                                <li>
                                                    <a
                                                        href={person.attributes.facebookUrl}
                                                        target={'_blank'}
                                                        rel="noopener noreferrer"
                                                        className="text-gray-400 hover:text-gray-300"
                                                    >
                                                        <span className="sr-only">Facebook</span>
                                                        <FontAwesomeIcon icon={faFacebook} />
                                                    </a>
                                                </li>
                                            )}
                                            {person.attributes.instagramUrl && (
                                                <li>
                                                    <a
                                                        href={person.attributes.instagramUrl}
                                                        target={'_blank'}
                                                        rel="noopener noreferrer"
                                                        className="text-gray-400 hover:text-gray-300"
                                                    >
                                                        <span className="sr-only">Instagram</span>
                                                        <FontAwesomeIcon icon={faInstagram} />
                                                    </a>
                                                </li>
                                            )}
                                            {person.attributes.linkedinUrl && (
                                                <li>
                                                    <a
                                                        href={person.attributes.linkedinUrl}
                                                        target={'_blank'}
                                                        rel="noopener noreferrer"
                                                        className="text-gray-400 hover:text-gray-300"
                                                    >
                                                        <span className="sr-only">LinkedIn</span>
                                                        <FontAwesomeIcon icon={faLinkedin} />
                                                    </a>
                                                </li>
                                            )}
                                            {person.attributes.githubUrl && (
                                                <li>
                                                    <a
                                                        href={person.attributes.githubUrl}
                                                        target={'_blank'}
                                                        rel="noopener noreferrer"
                                                        className="text-gray-400 hover:text-gray-300"
                                                    >
                                                        <span className="sr-only">GitHub</span>
                                                        <FontAwesomeIcon icon={faGithub} />
                                                    </a>
                                                </li>
                                            )}
                                        </ul>
                                    </div>
                                </div>
                            </li>
                        ))}
                    </ul>
                </div>
            </div>
        </div>
    );
}
