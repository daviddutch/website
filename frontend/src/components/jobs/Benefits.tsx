/*
  This example requires Tailwind CSS v2.0+

  This example requires some changes to your config:

  ```
  // tailwind.config.js
  const colors = require('tailwindcss/colors')

  module.exports = {
    // ...
    theme: {
      extend: {
        colors: {
          sky: colors.sky,
          teal: colors.teal,
          rose: colors.rose,
        },
      },
    },
  }
  ```
*/
import clsx from 'clsx';
import {
    faBeerFoam,
    faCouch,
    faCupTogo,
    faDesktopCode,
    faHomeHeart,
    faPercent,
    faSkiing,
    faSmileBeam,
    faSunglasses,
    faTableTennis,
    faTrainSubway,
    faUmbrellaBeach,
} from '@fortawesome/pro-duotone-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { t, Trans } from '@lingui/macro';

const actions = [
    {
        title: t`5 weeks vacation`,
        href: '#',
        icon: faUmbrellaBeach,
        iconForeground: 'text-teal-800',
        iconBackground: 'bg-gradient-to-r from-teal-100 to-teal-200',
    },
    {
        title: t`Part time possible`,
        href: '#',
        icon: faPercent,
        iconForeground: 'text-purple-800',
        iconBackground: 'bg-gradient-to-r from-purple-100 to-purple-200',
    },
    {
        title: t`Home office possible`,
        href: '#',
        icon: faHomeHeart,
        iconForeground: 'text-sky-800',
        iconBackground: 'bg-gradient-to-r from-sky-100 to-sky-200',
    },
    {
        title: t`Modern workstations`,
        href: '#',
        icon: faDesktopCode,
        iconForeground: 'text-yellow-800',
        iconBackground: 'bg-gradient-to-r from-yellow-100 to-yellow-200',
    },
    {
        title: t`Free coffee and tea`,
        href: '#',
        icon: faCupTogo,
        iconForeground: 'text-rose-800',
        iconBackground: 'bg-gradient-to-r from-rose-100 to-rose-200',
    },
    {
        title: t`Near the train station`,
        href: '#',
        icon: faTrainSubway,
        iconForeground: 'text-blue-800',
        iconBackground: 'bg-gradient-to-r from-blue-100 to-blue-200',
    },
    {
        title: t`Lounge area`,
        href: '#',
        icon: faCouch,
        iconForeground: 'text-teal-800',
        iconBackground: 'bg-gradient-to-r from-teal-100 to-teal-200',
    },
    {
        title: t`Table tennis`,
        href: '#',
        icon: faTableTennis,
        iconForeground: 'text-purple-800',
        iconBackground: 'bg-gradient-to-r from-purple-100 to-purple-200',
    },
    {
        title: "Apero's",
        href: '#',
        icon: faBeerFoam,
        iconForeground: 'text-sky-800',
        iconBackground: 'bg-gradient-to-r from-sky-100 to-sky-200',
    },
    {
        title: t`Summer Outings`,
        href: '#',
        icon: faSunglasses,
        iconForeground: 'text-yellow-800',
        iconBackground: 'bg-gradient-to-r from-yellow-100 to-yellow-200',
    },
    {
        title: t`Winter Outings`,
        href: '#',
        icon: faSkiing,
        iconForeground: 'text-rose-800',
        iconBackground: 'bg-gradient-to-r from-rose-100 to-rose-200',
    },
    {
        title: t`Good Mood`,
        href: '#',
        icon: faSmileBeam,
        iconForeground: 'text-blue-800',
        iconBackground: 'bg-gradient-to-r from-blue-100 to-blue-200',
    },
];

export function Benefits() {
    return (
        <div>
            <div className="divide-y divide-gray-400 overflow-hidden rounded-lg bg-gray-200 shadow-md sm:grid sm:grid-cols-2 sm:gap-px sm:divide-y-0 sm:border sm:border-gray-100 lg:grid-cols-4">
                {actions.map((action, actionIdx) => (
                    <div
                        key={action.title}
                        className={clsx(
                            actionIdx === 0 ? 'rounded-tl-lg rounded-tr-lg sm:rounded-tr-none' : '',
                            actionIdx === 3 ? 'sm:rounded-tr-lg' : '',
                            actionIdx === actions.length - 4 ? 'sm:rounded-bl-lg' : '',
                            actionIdx === actions.length - 1 ? 'rounded-bl-lg rounded-br-lg sm:rounded-bl-none' : '',
                            'group relative bg-white p-6 text-center focus-within:ring-2 focus-within:ring-inset focus-within:ring-blue-500'
                        )}
                    >
                        <div>
                            <span
                                className={clsx(
                                    action.iconBackground,
                                    action.iconForeground,
                                    'inline-flex rounded-lg p-3 text-2xl ring-4 ring-white'
                                )}
                            >
                                <FontAwesomeIcon aria-hidden="true" icon={action.icon} />
                            </span>
                        </div>
                        <div className="mt-8">
                            <h3 className="text-lg font-medium">
                                <span className="absolute inset-0" aria-hidden="true" />
                                <Trans id={action.title} />
                            </h3>
                        </div>
                        <span
                            className="pointer-events-none absolute top-6 right-6 text-gray-300 group-hover:text-gray-400"
                            aria-hidden="true"
                        ></span>
                    </div>
                ))}
            </div>
        </div>
    );
}
