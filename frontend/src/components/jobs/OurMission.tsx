import { Trans } from '@lingui/macro';

export function OurMission() {
    return (
        <div className="bg-gradient-to-r from-blue-500 to-blue-700">
            <div className="mx-auto max-w-2xl py-16 px-4 text-center sm:py-20 sm:px-6 lg:px-8">
                <h2 className="text-3xl font-bold tracking-tight text-white sm:text-4xl">
                    <span className="block">
                        <Trans>Our mission</Trans>
                    </span>
                </h2>
                <p className="mt-4 text-lg font-medium leading-6 text-blue-200">
                    <Trans>
                        WEDO Sàrl is a growing startup based in Fribourg. Our goal is simplifying teamwork in companies.
                        We develop a collaborative platform named WEDO. With WEDO you can prepare your meetings, write
                        the meeting minutes live and track tasks from a meeting to the next.
                    </Trans>
                </p>
            </div>
        </div>
    );
}
