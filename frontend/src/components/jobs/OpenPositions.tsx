import { Job } from '@/types/Job';
import Link from 'next/link';
import { faUsers } from '@fortawesome/pro-regular-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faLocationPin } from '@fortawesome/pro-solid-svg-icons';

export function OpenPositions({ jobs }: { jobs: Job[] }) {
    return (
        <div className="overflow-hidden border border-gray-200 bg-white shadow sm:rounded-md">
            <ul role="list" className="divide-y divide-gray-200">
                {jobs.map((job) => (
                    <li key={job.id}>
                        <Link href={`/jobs/${job.attributes.slug}`} className="block hover:bg-gray-50">
                            <div className="px-4 py-4 sm:px-6">
                                <div className="flex items-center justify-between">
                                    <p className="truncate text-sm font-medium text-blue-500">{job.attributes.title}</p>
                                    <div className="ml-2 flex flex-shrink-0">
                                        <p className="inline-flex rounded-full bg-green-100 px-2 text-xs font-semibold leading-5 text-green-800">
                                            {job.attributes.workload}
                                        </p>
                                    </div>
                                </div>
                                <div className="mt-2 sm:flex sm:justify-between">
                                    <div className="sm:flex">
                                        <p className="flex items-center text-sm text-gray-500">
                                            <FontAwesomeIcon
                                                className="mr-1.5 h-5 w-5 flex-shrink-0 text-gray-400"
                                                aria-hidden="true"
                                                icon={faUsers}
                                            />
                                            {job.attributes.department}
                                        </p>
                                        <p className="mt-2 flex items-center text-sm text-gray-500 sm:mt-0 sm:ml-6">
                                            <FontAwesomeIcon
                                                className="mr-1.5 h-5 w-5 flex-shrink-0 text-gray-400"
                                                aria-hidden="true"
                                                icon={faLocationPin}
                                            />
                                            {job.attributes.location}
                                        </p>
                                    </div>
                                    <div className="mt-2 flex items-center text-sm text-gray-500 sm:mt-0">
                                        <p>{job.attributes.languages}</p>
                                    </div>
                                </div>
                            </div>
                        </Link>
                    </li>
                ))}
            </ul>
        </div>
    );
}
