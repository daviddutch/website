import Image from 'next/image';

const files = [
    {
        title: 'WEDO employees drinking beer on a mountain',
        source: '/images/team/outings-wedo-team1.jpg',
    },
    {
        title: 'WEDO employees on a mountain',
        source: '/images/team/outings-wedo-team2.jpg',
    },
    {
        title: 'WEDO employees having dinner',
        source: '/images/team/outings-wedo-team3.jpg',
    },
    {
        title: 'WEDO employees at a throwing axe competition',
        source: '/images/team/outings-wedo-team4.jpg',
    },
    // {
    //     title: 'WEDO employees drinking beer on a mountain',
    //     source: '/images/team/outings-wedo-team5.jpg',
    // },
    {
        title: 'WEDO employees drinking beer on a mountain',
        source: '/images/team/outings-wedo-team6.jpg',
    },
    {
        title: 'WEDO employees doing the Aare river rafting',
        source: '/images/team/outings-wedo-team7.jpg',
    },
];

export function Outings() {
    return (
        <div className="mx-auto max-w-7xl py-12 px-4 sm:px-6 lg:px-8 lg:py-24">
            <ul
                role="list"
                className="grid grid-cols-2 gap-x-4 gap-y-4 sm:grid-cols-3 sm:gap-x-4 lg:grid-cols-3 xl:gap-x-4"
            >
                {files.map((file) => (
                    <li key={file.source} className="relative">
                        <div className="aspect-square group block w-full overflow-hidden rounded-lg bg-gray-100">
                            <Image
                                src={file.source}
                                alt={file.title}
                                className="pointer-events-none object-cover group-hover:opacity-75"
                                width={500}
                                height={200}
                            />
                        </div>
                    </li>
                ))}
            </ul>
        </div>
    );
}
