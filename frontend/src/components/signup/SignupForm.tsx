import { Trans } from '@lingui/macro';
import Image from 'next/image';
import { useGoogleReCaptcha } from 'react-google-recaptcha-v3';
import { FormEventHandler, useState } from 'react';
import { useRouter } from 'next/router';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEnvelopeOpenText } from '@fortawesome/pro-duotone-svg-icons';

type FormStatus = 'captchaError' | 'submitting' | 'success' | 'error';

const SignupFormBody = ({ isBtnDisabled, status }: { isBtnDisabled: boolean; status: FormStatus | null }) => {
    return (
        <>
            <div>
                <label htmlFor="fullName" className="block text-sm font-medium text-gray-700">
                    <Trans>Full name</Trans>
                </label>
                <div className="mt-1">
                    <input
                        id="fullName"
                        name="fullName"
                        type="text"
                        autoComplete="first_name"
                        required
                        className="block w-full appearance-none rounded-md border border-gray-300 px-3 py-2 placeholder-gray-400 shadow-sm focus:border-blue-500 focus:outline-none focus:ring-blue-500 sm:text-sm"
                    />
                </div>
            </div>
            <div>
                <label htmlFor="organization" className="block text-sm font-medium text-gray-700">
                    <Trans>Organization</Trans>
                </label>
                <div className="mt-1">
                    <input
                        id="organization"
                        name="organization"
                        type="text"
                        autoComplete="organization"
                        required
                        className="block w-full appearance-none rounded-md border border-gray-300 px-3 py-2 placeholder-gray-400 shadow-sm focus:border-blue-500 focus:outline-none focus:ring-blue-500 sm:text-sm"
                    />
                </div>
            </div>
            <div>
                <label htmlFor="email" className="block text-sm font-medium text-gray-700">
                    <Trans>Business email address</Trans>
                </label>
                <div className="mt-1">
                    <input
                        id="email"
                        name="email"
                        type="email"
                        autoComplete="email"
                        required
                        className="block w-full appearance-none rounded-md border border-gray-300 px-3 py-2 placeholder-gray-400 shadow-sm focus:border-blue-500 focus:outline-none focus:ring-blue-500 sm:text-sm"
                    />
                </div>
            </div>

            <div className="flex items-center justify-between">
                <div className="flex items-center">
                    <input
                        id="terms"
                        name="terms"
                        type="checkbox"
                        required
                        className="h-4 w-4 rounded border-gray-300 text-blue-600 focus:ring-blue-500"
                    />
                    <label htmlFor="terms" className="ml-2 block text-sm text-gray-900">
                        <Trans>I accept the terms of services and privacy policy</Trans>
                    </label>
                </div>
            </div>

            {status === 'captchaError' && (
                <div className="rounded bg-red-500 p-2 font-bold text-red-100">
                    <Trans>Invalid captcha validation, please try again</Trans>
                </div>
            )}
            {status === 'error' && (
                <div className="rounded bg-red-500 p-2 font-bold text-red-100">
                    <Trans>An error occurred while submitting the form, please try again</Trans>
                </div>
            )}

            <div>
                <button
                    type="submit"
                    disabled={isBtnDisabled}
                    className="flex w-full justify-center rounded-md border border-transparent bg-gradient-to-r from-blue-400 to-blue-500 py-2 px-4 text-sm font-medium text-white shadow-sm hover:from-pink-500 hover:to-indigo-500 focus:outline-none"
                >
                    <Trans>Start trial</Trans>
                </button>
            </div>
        </>
    );
};

type ContactFormData = {
    fullName: HTMLInputElement;
    organization: HTMLInputElement;
    email: HTMLInputElement;
    terms: HTMLInputElement;
} & HTMLFormElement;

export function SignupForm() {
    const { executeRecaptcha } = useGoogleReCaptcha();
    const [isBtnDisabled, setBtnDisabled] = useState(false);
    const [status, setStatus] = useState<FormStatus | null>(null);
    const router = useRouter();
    const locale = router.locale || router.defaultLocale!;

    const handleSubmit: FormEventHandler<ContactFormData> = async (event) => {
        event.preventDefault();

        setBtnDisabled(true);

        if (!executeRecaptcha) {
            return;
        }

        const data = {
            fullName: event.currentTarget.fullName.value,
            organization: event.currentTarget.organization.value,
            email: event.currentTarget.email.value,
            locale: locale,
            token: '',
        };
        try {
            const token = await executeRecaptcha();
            if (!token) {
                setStatus('captchaError');
                return;
            }

            data.token = token;

            const options = {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(data),
            };

            const response = await fetch('/api/signup', options);

            const result = await response.json();

            setBtnDisabled(false);
            setStatus('success');
        } catch (error) {
            setStatus('error');
            setBtnDisabled(false);
        }
    };

    return (
        <>
            <div className="flex min-h-max flex-col justify-center bg-gradient-to-r from-gray-50 to-gray-100 py-12 sm:px-6 lg:px-8">
                <div className="sm:mx-auto sm:w-full sm:max-w-md">
                    <Image
                        className="mx-auto h-12 w-auto"
                        width={200}
                        height={50}
                        src={'/icon.svg'}
                        alt={'WEDO logo'}
                    />
                    <h2 className="mt-6 text-center text-3xl font-bold tracking-tight text-gray-800">
                        <Trans>Try WEDO for Free</Trans>
                    </h2>
                    <p className="mt-2 text-center text-sm text-gray-600">
                        <Trans>14-day free trial</Trans> • <Trans>No Credit Card Required</Trans> •{' '}
                        <Trans>Cancel Anytime</Trans>
                    </p>
                </div>
                <div className="mt-8 sm:mx-auto sm:w-full sm:max-w-lg">
                    <div className="bg-white py-8 px-4 shadow sm:rounded-lg sm:px-10">
                        <form onSubmit={handleSubmit} className="space-y-6">
                            {status === 'success' ? (
                                <div className="flex items-center justify-center py-4">
                                    <div className="text-center">
                                        <FontAwesomeIcon
                                            icon={faEnvelopeOpenText}
                                            className="mx-auto text-9xl text-blue-600"
                                        />
                                        <h3 className="mt-5 max-w-2xl text-2xl font-extrabold text-gray-700">
                                            <Trans>Thank you for requesting a trial</Trans>
                                        </h3>
                                        <p className="mt-2">
                                            <Trans>We've sent you an email with a link to start your free trial.</Trans>
                                        </p>
                                        <p className="mt-2">
                                            <Trans>
                                                If you don't see it, please check your spam folder or contact us.
                                            </Trans>
                                        </p>
                                    </div>
                                </div>
                            ) : (
                                <SignupFormBody status={status} isBtnDisabled={isBtnDisabled} />
                            )}
                        </form>
                    </div>
                </div>
            </div>
        </>
    );
}
