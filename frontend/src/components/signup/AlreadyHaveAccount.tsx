import { Trans } from '@lingui/macro';

export function AlreadyHaveAccount() {
    return (
        <div className="relative bg-gradient-to-r from-blue-400 to-blue-700">
            <div className="mx-auto max-w-7xl py-3 px-3 sm:px-6 lg:px-8">
                <div className="pr-16 sm:px-16 sm:text-center">
                    <p className="font-medium text-white">
                        <span className="inline">
                            <Trans>Already have an account?</Trans>
                        </span>
                        <span className="block sm:ml-2 sm:inline-block">
                            <a href="https://login.wedo.swiss" className="font-bold text-white underline">
                                <Trans>Log in</Trans>
                                <span aria-hidden="true"> &rarr;</span>
                            </a>
                        </span>
                    </p>
                </div>
            </div>
        </div>
    );
}
