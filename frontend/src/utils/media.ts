import { getStrapiURL } from './api';

export interface StrapiMedia {
    data: {
        id?: string;
        attributes: {
            name?: string;
            alternativeText?: string;
            caption?: string;
            width?: number;
            height?: number;
            ext?: string;
            mime?: string;
            size?: number;
            url: string;
            previewUrl?: null;
        };
    };
}

export function getStrapiMedia(media: any): string {
    if (!media?.data) {
        return '';
    }
    const { url } = media.data.attributes;
    return url.startsWith('/') ? getStrapiURL(url) : url;
}
