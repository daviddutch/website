import { I18n } from '@lingui/core';
import { en, fr, de } from 'make-plural/plurals';
import { Messages } from '@lingui/core/esm/i18n';

export function initTranslation(i18n: I18n): void {
    i18n.loadLocaleData({
        en: { plurals: en },
        fr: { plurals: fr },
        de: { plurals: de },
    });
}

export async function loadTranslation(locale: string, isProduction = true): Promise<Messages[]> {
    let data;
    if (isProduction) {
        data = await import(`../translations/locales/${locale}/messages`);
    } else {
        data = await import(`@lingui/loader!../translations/locales/${locale}/messages.po`);
    }
    return data.messages;
}
