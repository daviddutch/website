export function getStrapiURL(path = '') {
    return `${process.env.NEXT_PUBLIC_STRAPI_API_URL || 'http://localhost:1337'}${path}`;
}

const stringifyParams = (queryObj: object, nesting = ''): string => {
    const pairs = Object.entries(queryObj).map(([key, val]) => {
        // Handle a second base case where the value to encode is an array
        if (val === undefined) {
            return '';
        } else if (Array.isArray(val)) {
            return val
                .map((subVal, index) =>
                    [nesting === '' ? `${key}[${index}]` : nesting + `[${key}][${index}]`, subVal]
                        .map(encodeURIComponent)
                        .join('=')
                )
                .join('&');
        } else if (typeof val === 'object') {
            return stringifyParams(val, nesting === '' ? key : nesting + `[${key}]`);
        } else {
            return [nesting === '' ? key : nesting + `[${key}]`, val].map(encodeURIComponent).join('=');
        }
    });
    return pairs.join('&');
};

export async function fetchAPI<T>(path: string, urlParamsObject = {}, options = {}): Promise<{ data: T }> {
    // Merge default and user options
    const mergedOptions = {
        headers: {
            'Content-Type': 'application/json',
        },
        ...options,
    };

    // Build request URL
    const queryString = stringifyParams(urlParamsObject);
    const requestUrl = `${getStrapiURL(`/api${path}${queryString ? `?${queryString}` : ''}`)}`;

    // Trigger API call
    const response = await fetch(requestUrl, mergedOptions);

    // Handle response
    if (!response.ok) {
        console.error(response.statusText);
        throw new Error(`An error occured please try again`);
    }
    return await response.json();
}
