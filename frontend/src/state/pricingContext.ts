import React from 'react';

export type Currency = 'chf' | 'eur' | 'usd';

export interface PricingContextProps {
    currency: Currency;
    setCurrency: (currency: Currency) => void;
}

export const PricingContext = React.createContext<PricingContextProps>({
    currency: 'chf',
    setCurrency: () => {},
});
