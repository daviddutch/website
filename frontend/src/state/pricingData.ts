import { t } from '@lingui/macro';
import {
    faBriefcase,
    faCalendarStar,
    faChartNetwork,
    faCheck,
    faCircleCheck,
    faCircleInfo,
    faClone,
    faClouds,
    faFileLines,
    faFileSignature,
    faKey,
    faLifeRing,
    faPersonCirclePlus,
} from '@fortawesome/pro-regular-svg-icons';

export const plans = [
    {
        name: t`Pro`,
        pricing: {
            chf: 'CHF 19.90',
            usd: '$19.90',
            eur: 'EUR 19.90',
        },
        features: [
            {
                name: t`Unlimited Meetings`,
                icon: faCalendarStar,
            },
            {
                name: t`Unlimited Tasks`,
                icon: faCheck,
            },
            {
                name: t`Unlimited Checklists`,
                icon: faCircleCheck,
            },
            {
                name: t`Project Management`,
                icon: faBriefcase,
            },
            {
                name: t`File Sharing`,
                icon: faFileLines,
            },
            {
                name: t`Custom Fields`,
                icon: faClone,
            },
            {
                name: t`Live Chat Support`,
                icon: faLifeRing,
            },
        ],
    },
    {
        name: t`Enterprise`,
        pricing: {
            chf: 'CHF 24.90',
            usd: '$24.90',
            eur: 'EUR 24.90',
        },
        features: [
            {
                name: t`All features included in the PRO plan`,
                icon: faCircleInfo,
            },
            {
                name: t`Active Directory Integration with ADFS`,
                icon: faClouds,
            },
            {
                name: t`SAML 2.0 Single Sign-On`,
                icon: faChartNetwork,
            },
            {
                name: t`User Provisioning & Deprovisioning (SCIM)`,
                icon: faPersonCirclePlus,
            },
            {
                name: t`Two-factor Authentication Enforcing`,
                icon: faKey,
            },
            {
                name: t`Custom Terms`,
                icon: faFileSignature,
            },
        ],
    },
];

export const faq = [
    {
        question: t`How does WEDO's pricing work?`,
        answer: t`WEDO is sold by pack of users. A user is a person from your organization with an account.`,
    },
    {
        question: t`Can I change my plan?`,
        answer: t`You can upgrade your subscription at any time you want. You can downgrade or cancel your subscription for the end of your billing period.`,
    },
    {
        question: t`Can I use WEDO for my whole organization, or just my team?`,
        answer: t`Either or both! We have huge enterprises using it organization-wide as well as one and two-people teams or even a single department.`,
    },
    {
        question: t`How are upgrades billed?`,
        answer: t`When you upgrade your subscription, we immediately bill the pro-rated amount for your current cycle. Unfortunately, we cannot offer refunds for plan cancellations.`,
    },
    {
        question: t`What forms of payment do you accept?`,
        answer: t`You can purchase your subscription with any major credit card. We can also issue an invoice payable by bank transfer.`,
    },
    {
        question: t`How secure is WEDO?`,
        answer: t`Protecting your data is our top priority. That’s why all your data are stored in Switzerland in ISO 27001 certified data centers. To learn more please read our [security page](/security).`,
    },
    {
        question: t`Will I be charged sales tax?`,
        answer: t`Yes. We will apply state and local sales tax based on your billing address to your total charge. All prices are shown without applicable taxes.`,
    },
    {
        question: t`Can we integrate our Identity Management solutions with WEDO?`,
        answer: t`Yes. WEDO uses SAML 2.0 for integration of Single Sign On and/or Active Directory solutions with the customer. We are on the [Azure Marketplace](https://azuremarketplace.microsoft.com/en-us/marketplace/apps/aad.wedo?tab=Overview) and [Okta Integration Network](https://www.okta.com/integrations/wedo/).`,
    },
];
