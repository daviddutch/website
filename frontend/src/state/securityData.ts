import { t } from '@lingui/macro';
import { Highlight } from '@/components/security/SecurityHighlights';
import { faChartArea, faCloudRainbow, faHome, faKey, faServer, faUsers } from '@fortawesome/pro-regular-svg-icons';

export const faq = [
    {
        question: t`Is the data located in Switzerland?`,
        answer: t`WEDO is hosted on Exoscale, a hosting provider based in Switzerland. The datacenters are located in Zürich (ZH). They are ISO/IEC 27001 (Information security management systems) certified. For more information: [exoscale.com](https://exoscale.com).`,
    },
    {
        question: t`What is your backup policy?`,
        answer: t`The database is replicated in real time on a different server. In case of failure of the primary database, the replica is used. A full database backup is done once a day. We keep the backups from the last seven days and the last four fridays. To protect against datacenter failure, backups are transfered to a different geographical location.`,
    },
    {
        question: t`Do you implement SDLC best practices in your development life cycle?`,
        answer: t`Yes, every change in WEDO code goes throught a serie of automatic tests and code reviews. Then, manual testing is done. If no problems are found, the code is published to production. To avoid mistakes, different static analysis tools are used during the development life cylce.`,
    },
    {
        question: t`Do you perform regular vulnerability and penetration testing?`,
        answer: t`Yes. We use automation for regular vulnerability tests of the product code before release to production. We also perform annual Full Penetration Testing by an external specialized Penetration Testing Company. Prospects and customers may receive a copy of the penetration testing report upon signing a Non Disclosure Agreement with us.`,
    },
    {
        question: t`Do you use the HTTPS protocol?`,
        answer: t`WEDO uses HTTPS protocol everywhere. The Hypertext Transfer Protocol Secure (HTTPS) is an authentication of the accessed website and protection of the privacy and integrity of the exchanged data while in transit. It protects against man-in-the-middle attacks.`,
    },
    {
        question: t`How are passwords stored?`,
        answer: t`We enforce a certain level of password complexity. Every password must be at least 8 characters long and contain: one lowercase, one number and one special character. Every password is stored in the database in the form of a bcrypt hash using a salt and several iterations. If a user forgets his password, he can reset it with a link sent to his email address.`,
    },
    {
        question: t`Do you support Two-factor Authentication?`,
        answer: t`You can enable Two-factor Authentication (2FA) to add an extra layer of security for your account. When enabled, 2FA requires an extra passcode when loggin in. This extra passcode is store in an app on your phone and is regenerated every 30 seconds. This makes things much harder for potential attackers, as they would not only need your username and password, but also be in possession of your 2FA device.`,
    },
    {
        question: t`Can I export my data?`,
        answer: t`Legaly your data belongs to you. You can export them at any time directly from the platform in standard formats (PDF, XLSX or CSV). If you don’t renew your contract, we guarantee you enough time to access the platform to export your data.`,
    },
    {
        question: t`How is your Service Level Agreement calculated?`,
        answer: t`We guarantee an availability of 99.9% of the platform.

                The monthly availability is computed with the following formula:

            
            Definitions:
            
            T = total number of minutes in the month
            D = total number of unplanned downtime minutes in the month
            M = total number of planned maintenance minutes in the month
            If our availability is under the threshold, the downtime will be credited to your subscription duration.`,
    },
];

export const highlights: Highlight[] = [
    {
        icon: faCloudRainbow,
        title: t`Virtual private cloud`,
        description: t`- Hosted in ISO 27001 certified data centers
- Hosted in Switzerland
- Highly secure cloud-based SaaS offering`,
    },
    {
        icon: faHome,
        title: t`Multi-tenancy`,
        description: t`- Multi-layer tenant data segregation
- Each tenant accessed from a separate subdomain`,
    },
    {
        icon: faKey,
        title: t`Encryption`,
        description: t`- HTTPS with TLS is used on all pages
- The certificate was generated using a 2048-Bit private key
- All data is encrypted during transit`,
    },
    {
        icon: faServer,
        title: t`Backups`,
        description: t`- Data is replicated in real time on a different server
- Full database backup once a day
- Backups are encrypted using an RSA key
- Geographically redundant backups`,
    },
    {
        icon: faChartArea,
        title: t`High availability`,
        description: t`- Monitored on [wedo.statuspage.io](https://wedo.statuspage.io/)
- Incidents reported on our status page
- Contractually binding 99.9% availability`,
    },
    {
        icon: faUsers,
        title: t`Customer controls access`,
        description: t`- Role-based permissions via Admin Dashboard
- Two-factor authentication (2FA)
- Integrates with your existing Identity Management
- SAML 2.0 interface for Single Sign-On & Active Directory`,
    },
];
