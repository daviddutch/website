import { t } from '@lingui/macro';

export const menu = [
    {
        slug: 'terms',
        name: t`Terms of Services`,
    },
    {
        slug: 'privacy',
        name: t`Privacy Policy`,
    },
    {
        slug: 'sla',
        name: t`Service Level Agreement`,
    },
    {
        slug: 'dpa',
        name: t`Data Processing Agreement`,
    },
];
