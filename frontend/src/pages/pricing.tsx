import { Header } from '@/components/shared/Header';
import { GetStaticProps, NextPage } from 'next';
import { loadTranslation } from '@/utils/i18n';
import { PricingHeader } from '@/components/pricing/PricingHeader';
import { PricingTable } from '@/components/pricing/PricingTable';
import { PricingServices } from '@/components/pricing/PricingServices';
import { Footer } from '@/components/shared/Footer';
import { FAQ } from '@/components/shared/FAQ';
import React from 'react';
import { Currency, PricingContext } from '@/state/pricingContext';
import { CustomerLogos } from '@/components/shared/CustomerLogos';
import { Messages } from '@lingui/core/esm/i18n';
import { faq, plans } from '@/state/pricingData';
import { fetchPage } from '@/services/page';
import { fetchCustomerLogos } from '@/services/logos';
import { Page } from '@/types/Page';
import { Logo } from '@/types/Logo';
import { t } from '@lingui/macro';
import { CtaType, LogosSectionType } from '@/types/LandingPage';
import { CallToAction } from '@/components/cta/CallToAction';

interface PageProps {
    translation: Messages[];
    page: Page;
    logos: Logo[];
}

const PricingPage: NextPage<PageProps> = ({ logos }) => {
    const [currency, setCurrency] = React.useState<Currency>('chf');
    return (
        <PricingContext.Provider value={{ currency, setCurrency }}>
            <Header />
            <div className="bg-blue-800">
                <PricingHeader />
                <PricingTable plans={plans} />
            </div>
            <CustomerLogos
                logos={logos}
                type={LogosSectionType.Simple}
                title={t`Trusted by over 500 forward-thinking companies`}
            />
            <CallToAction type={CtaType.Security} />
            <FAQ entries={faq} />
            <PricingServices />
            <CallToAction type={CtaType.Main} />
            <Footer />
        </PricingContext.Provider>
    );
};

export const getStaticProps: GetStaticProps = async ({ locale }) => {
    const translation = await loadTranslation(locale!, process.env.NODE_ENV === 'production');
    const [page, logosRes] = await Promise.all([
        fetchPage('pricing', locale),
        fetchCustomerLogos({ showPricing: true }),
    ]);

    return {
        props: {
            translation,
            page,
            logos: logosRes.data,
            faq: faq,
            plans: plans,
        },
    };
};

export default PricingPage;
