import { ImageResponse } from '@vercel/og';
import { NextRequest } from 'next/server';

export const config = {
    runtime: 'experimental-edge',
};

const getParam = (req: NextRequest, param: string, fallback: string): string => {
    const { searchParams } = new URL(req.url);
    return searchParams.get(param) || fallback;
};

const font = fetch(new URL('../../assets/Inter-Bold.otf', import.meta.url)).then((res) => res.arrayBuffer());

export default async function (req: NextRequest) {
    try {
        const fontData = await font;

        const data = JSON.parse(decodeURIComponent(escape(atob(getParam(req, 'data', '')))));

        if (data.url) {
            return new ImageResponse(
                (
                    <div tw="flex bg-white text-black">
                        <img src={data.url} className="h-full" />
                    </div>
                ),
                {
                    width: 1280,
                    height: 720,
                }
            );
        }

        return new ImageResponse(
            (
                <div
                    tw="flex flex-row items-center justify-center w-full h-full bg-white text-black"
                    style={{
                        fontFamily: 'Inter',
                    }}
                >
                    <div tw="absolute flex">
                        <svg
                            width="1200"
                            height="600"
                            viewBox="0 0 1200 600"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                        >
                            <g clip-path="url(#clip0_1_4)">
                                <rect width="1200" height="600" fill="url(#paint0_linear_1_4)" />
                                <path
                                    d="M578.746 635.755C559.694 616.703 568.411 584.172 594.436 577.199L1355.67 373.228C1381.69 366.255 1405.51 390.069 1398.53 416.094L1194.56 1177.32C1187.59 1203.35 1155.06 1212.07 1136 1193.01L578.746 635.755Z"
                                    fill="url(#paint1_linear_1_4)"
                                />
                                <path
                                    d="M677.5 94.6891C700.833 108.161 700.833 141.839 677.5 155.311L-5 549.352C-28.3333 562.824 -57.5 545.985 -57.5 519.042V-269.042C-57.5 -295.985 -28.3333 -312.824 -5 -299.352L677.5 94.6891Z"
                                    fill="url(#paint2_linear_1_4)"
                                />
                                <path
                                    d="M154.584 89.2771L144.645 52.278L134.705 89.2771C134.152 91.6437 132.023 93.3004 129.499 93.3004C126.973 93.3004 124.845 91.6437 124.291 89.356L111.669 45.3359C111.59 44.9403 111.511 44.4681 111.511 44.0737C111.511 41.786 113.404 39.8927 115.693 39.8927C117.665 39.8927 119.242 41.2337 119.716 43.0481L129.971 82.4917L140.78 42.8115C141.252 41.1549 142.752 39.8927 144.724 39.8927C146.539 39.8927 148.116 41.1537 148.59 42.8115L159.318 82.4917L169.573 43.0481C170.047 41.2337 171.625 39.8927 173.597 39.8927C175.963 39.8927 177.778 41.7848 177.778 44.0737C177.778 44.4681 177.7 44.9403 177.62 45.3359L165.077 89.356C164.446 91.6426 162.396 93.3004 159.87 93.3004C157.345 93.3004 155.215 91.6437 154.584 89.2771Z"
                                    fill="white"
                                />
                                <path
                                    d="M185.743 88.7237V44.4681C185.743 41.786 187.242 40.286 190.003 40.286H217.93C219.823 40.286 221.48 41.8637 221.48 43.837C221.48 45.7291 219.825 47.2292 217.93 47.2292H193.632V62.6122H217.378C219.27 62.6122 220.928 64.1098 220.928 66.0043C220.928 67.9765 219.271 69.5542 217.378 69.5542H193.632V85.9651H217.93C219.823 85.9651 221.48 87.4627 221.48 89.3572C221.48 91.3294 219.825 92.9071 217.93 92.9071H190.003C187.242 92.9059 185.743 91.4071 185.743 88.7237Z"
                                    fill="white"
                                />
                                <path
                                    d="M230.945 88.7237V44.4681C230.945 41.786 232.443 40.286 235.205 40.286H249.72C266.13 40.286 277.016 51.4891 277.016 66.6354C277.016 81.8618 266.13 92.9059 249.72 92.9059H235.205C232.444 92.9059 230.945 91.4071 230.945 88.7237ZM249.72 85.9639C261.869 85.9639 268.89 77.2851 268.89 66.6354C268.89 55.8267 262.106 47.2292 249.72 47.2292H238.834V85.9639H249.72Z"
                                    fill="white"
                                />
                                <path
                                    d="M310.857 39.4193C326.555 39.4193 337.364 51.0169 337.364 66.6354C337.364 82.2538 326.555 93.8514 310.857 93.8514C295.158 93.8514 284.349 82.2538 284.349 66.6354C284.349 51.0169 295.158 39.4193 310.857 39.4193ZM310.857 46.4403C299.576 46.4403 292.476 55.039 292.476 66.6354C292.476 78.1529 299.576 86.8305 310.857 86.8305C321.98 86.8305 329.237 78.1529 329.237 66.6354C329.239 55.039 321.98 46.4403 310.857 46.4403Z"
                                    fill="white"
                                />
                                <path
                                    d="M31.493 59.8682C30.6712 59.3937 30 58.2304 30 57.2814V32.3955C30 30.262 31.5118 29.3884 33.3604 30.4551L71.7301 52.6082C72.5519 53.0827 72.5519 53.8587 71.7301 54.332L48.3112 67.8523C47.4894 68.3268 46.1459 68.3268 45.3253 67.8523L31.493 59.8682ZM48.3101 79.6571C47.4882 80.1316 46.1448 80.1316 45.3241 79.6571L31.493 71.6718C30.6712 71.1973 30 71.5859 30 72.5337V100.604C30 102.738 31.5118 103.612 33.3604 102.545L92.4315 68.4398C94.2788 67.3731 94.2788 65.6269 92.4315 64.5602L84.9384 60.2344C84.1165 59.7599 82.7731 59.7599 81.9513 60.2344L48.3101 79.6571Z"
                                    fill="url(#paint3_linear_1_4)"
                                />
                            </g>
                            <defs>
                                <linearGradient
                                    id="paint0_linear_1_4"
                                    x1="-113.255"
                                    y1="560.471"
                                    x2="80.5326"
                                    y2="-12.1801"
                                    gradientUnits="userSpaceOnUse"
                                >
                                    <stop stop-color="#0072CE" />
                                    <stop offset="1" stop-color="#2F94E7" />
                                </linearGradient>
                                <linearGradient
                                    id="paint1_linear_1_4"
                                    x1="535.88"
                                    y1="592.889"
                                    x2="1550.1"
                                    y2="864.649"
                                    gradientUnits="userSpaceOnUse"
                                >
                                    <stop stop-color="#0760A7" />
                                    <stop offset="1" stop-color="#00263A" />
                                </linearGradient>
                                <linearGradient
                                    id="paint2_linear_1_4"
                                    x1="730"
                                    y1="125"
                                    x2="-320"
                                    y2="125"
                                    gradientUnits="userSpaceOnUse"
                                >
                                    <stop stop-color="#0760A7" />
                                    <stop offset="1" stop-color="#00263A" />
                                </linearGradient>
                                <linearGradient
                                    id="paint3_linear_1_4"
                                    x1="24.6035"
                                    y1="91.1575"
                                    x2="62.1914"
                                    y2="48.8712"
                                    gradientUnits="userSpaceOnUse"
                                >
                                    <stop stop-color="white" stop-opacity="0.7" />
                                    <stop offset="1" stop-color="white" />
                                </linearGradient>
                                <clipPath id="clip0_1_4">
                                    <rect width="1200" height="600" fill="white" />
                                </clipPath>
                            </defs>
                        </svg>
                    </div>
                    <div tw="flex flex-col w-full justify-center items-center">
                        <h1 tw="text-5xl max-w-3/4 text-white text-center" style={{ fontWeight: 900 }}>
                            {data.title}
                        </h1>
                        <h2 tw="text-3xl max-w-3/4 font-bold text-center tracking-tight text-white">
                            {data.description}
                        </h2>
                    </div>
                </div>
            ),
            {
                width: 1200,
                height: 600,
                fonts: [
                    {
                        name: 'Inter',
                        data: fontData,
                        style: 'normal',
                    },
                ],
            }
        );
    } catch (e: any) {
        console.log(`${e.message}`);
        return new Response(`Failed to generate the image`, {
            status: 500,
        });
    }
}
