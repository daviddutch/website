import { NextApiRequest, NextApiResponse } from 'next';
import fetch from 'node-fetch';

const notifyWebhook = async (url: string, body: object) => {
    const res = await fetch(url, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({ body }),
    });
};

type RecaptchaResponse = {
    success: boolean;
    score: number;
};

const isRecaptchaValid = async (token: string): Promise<boolean> => {
    const secretKey = process.env.RECAPTHA_SECRET_KEY;

    const response = await fetch(
        `https://www.google.com/recaptcha/api/siteverify?secret=${secretKey}&response=${token}`
    );
    let data = (await response.json()) as RecaptchaResponse;

    return data.success && data.score >= 0.5;
};

export default async (req: NextApiRequest, res: NextApiResponse) => {
    const body = req.body;

    if (!(await isRecaptchaValid(body.token))) {
        return res.json({
            status: 'Failed',
            message: 'Something went wrong, please try again!!!',
        });
    }

    if (!body.fullName || !body.email) {
        return res.status(400).json({ data: 'Missing required fields' });
    }

    if (body.email === 'test@wedo.swiss') {
        return res.status(200).json({ status: 'Success', message: 'Thank you for testing this form.' });
    }

    const data = {
        fullName: body.fullName,
        phone: body.phone,
        organization: body.organization,
        email: body.email,
        locale: body.locale,
    };

    await notifyWebhook('https://hooks.zapier.com/hooks/catch/940328/b71rw86/', data);

    return res.status(200).json({ status: 'Success', message: 'Thank you for requesting a trial.' });
};
