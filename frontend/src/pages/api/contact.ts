import { NextApiRequest, NextApiResponse } from 'next';
import formData from 'form-data';
import Mailgun from 'mailgun.js';
import fetch from 'node-fetch';

const mailgun = new Mailgun(formData);
const mg = mailgun.client({
    username: 'api',
    key: process.env.MAILGUN_API_KEY || 'key-cc4b6414ed581db470e4782354d13c34',
    url: 'https://api.eu.mailgun.net',
});

type RecaptchaResponse = {
    success: boolean;
    score: number;
};

const isRecaptchaValid = async (token: string): Promise<boolean> => {
    const secretKey = process.env.RECAPTHA_SECRET_KEY;

    const response = await fetch(
        `https://www.google.com/recaptcha/api/siteverify?secret=${secretKey}&response=${token}`
    );
    let data = (await response.json()) as RecaptchaResponse;

    return data.success && data.score >= 0.5;
};

export default async (req: NextApiRequest, res: NextApiResponse) => {
    // Get data submitted in request's body.
    const body = req.body;

    if (!(await isRecaptchaValid(body.token))) {
        return res.json({
            status: 'Failed',
            message: 'Something went wrong, please try again!!!',
        });
    }

    // Guard clause checks for first and last name,
    // and returns early if they are not found
    if (!body.fullName || !body.email || !body.message) {
        // Sends a HTTP bad request error code
        return res.status(400).json({ data: 'Missing required fields' });
    }

    const emailToSend = {
        from: 'info@wedo.swiss',
        to: 'info@wedo.swiss',
        subject: 'Message from wedo.com',
        html: `<div>
            <h2>New message sent from wedo.com</h2>   
            <ul>
                <li>Name: <b>${body.fullName}</b></li>
                <li>Company: <b>${body.company}</b></li>
                <li>Phone: <b>${body.phone}</b></li>
                <li>Email: <b>${body.email}</b></li>
                <li>Subject: <b>${body.subject}</b></li>
            </ul> 
            <div>
                <b>Message:</b>
                <p>${body.message}</p>
            </div>
          </div>`,
    };

    await mg.messages.create('wedo.swiss', emailToSend);

    return res.status(200).json({ status: 'Success', message: 'Thank you for contacting us.' });
};
