import { Header } from '@/components/shared/Header';
import { GetStaticProps, NextPage } from 'next';
import { loadTranslation } from '@/utils/i18n';
import { Footer } from '@/components/shared/Footer';
import React from 'react';
import { Messages } from '@lingui/core/esm/i18n';
import { PageHeader } from '@/components/shared/PageHeader';
import { CallToAction } from '@/components/cta/CallToAction';
import { OnboardingSteps } from '@/components/onboarding/OnboardingSteps';
import { fetchPage } from '@/services/page';
import { Page } from '@/types/Page';
import { t } from '@lingui/macro';

export const getStaticProps: GetStaticProps = async ({ locale }) => {
    const translation = await loadTranslation(locale!, process.env.NODE_ENV === 'production');
    const page = await fetchPage('onboarding', locale);

    return {
        props: {
            page,
            translation,
        },
    };
};

interface PageProps {
    translation: Messages[];
    page: Page;
}

const OnboardingPage: NextPage<PageProps> = () => {
    return (
        <>
            <Header />
            <PageHeader
                title={t`Onboarding process`}
                description={t`We help you integrate WEDO in less than 30 days`}
            />
            <div className="mx-auto my-20 max-w-4xl">
                <OnboardingSteps />
            </div>
            <CallToAction />
            <Footer />
        </>
    );
};

export default OnboardingPage;
