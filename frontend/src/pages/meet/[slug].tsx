import { Employee } from '@/types/Employee';
import { Header } from '@/components/shared/Header';
import { PageHeader } from '@/components/shared/PageHeader';
import { CallToAction } from '@/components/cta/CallToAction';
import { Footer } from '@/components/shared/Footer';
import { fetchAPI } from '@/utils/api';
import { GetStaticProps } from 'next';
import { loadTranslation } from '@/utils/i18n';
import { InlineWidget } from 'react-calendly';
import React from 'react';
import { t } from '@lingui/macro';
import { Page } from '@/types/Page';

const MeetMe = ({ employee }: { employee: Employee }) => {
    return (
        <div>
            <Header />
            <PageHeader
                title={t`Schedule a meeting with ${employee.attributes.firstName}`}
                description={employee.attributes.languages}
                sectionName={employee.attributes.title}
            />
            <InlineWidget
                styles={{
                    height: '930px',
                }}
                url={
                    employee.attributes.calendlyGlobal +
                    '?embed_domain=wedo.swiss&embed_type=Inline&text_color=00263a&primary_color=0072ce'
                }
            />
            <CallToAction />
            <Footer />
        </div>
    );
};

export const getStaticPaths = async ({ locales }: { locales: string[] }) => {
    const fetchEmployees = async (locale: string) => {
        return await fetchAPI<Employee[]>('/employees', {
            fields: ['slug'],
            locale: locale,
        });
    };
    const paths = await Promise.all(
        locales.map(async (locale) =>
            fetchEmployees(locale).then((pages) =>
                pages.data.map((page) => ({
                    params: { slug: page.attributes.slug },
                    locale,
                }))
            )
        )
    ).then((values) => values.flat());

    return {
        paths: paths,
        fallback: false,
    };
};

export const getStaticProps: GetStaticProps = async ({ params, locale }) => {
    const translation = await loadTranslation(locale!, process.env.NODE_ENV === 'production');
    const employeeRes = await fetchAPI<Employee[]>('/employees', {
        filters: {
            slug: params?.slug,
        },
        populate: {
            fields: ['*'],
        },
    });
    let employee = employeeRes.data[0];

    const page: Page = {
        id: '',
        attributes: {
            name: t`Schedule a meeting with ${employee.attributes.firstName}`,
            seo: {
                metaTitle: t`Schedule a meeting with ${employee.attributes.firstName}`,
                metaDescription: '',
                article: false,
            },
        },
    };
    return {
        props: { employee, page, translation },
        revalidate: 60,
    };
};

export default MeetMe;
