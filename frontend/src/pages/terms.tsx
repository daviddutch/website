import { GetStaticProps, NextPage } from 'next';
import { loadTranslation } from '@/utils/i18n';
import React from 'react';
import { Messages } from '@lingui/core/esm/i18n';
import { menu } from '@/state/termsData';
import { fetchPage } from '@/services/page';
import { Page } from '@/types/Page';
import { TermsPage } from '@/components/terms/TermsPage';
import { TermsContent } from '@/components/terms/TermsContent';
import { Menu } from '@/types/Menu';
import { t } from '@lingui/macro';
import { TermsOfServices } from '@/components/terms/TermsOfServices';

interface PageProps {
    translation: Messages[];
    page: Page;
    menu: Menu[];
}

const TosPage: NextPage<PageProps> = ({ menu }) => {
    return (
        <TermsPage menu={menu} currentSlug={'terms'}>
            <TermsContent
                title={t`Terms of Service`}
                sectionName={t`Terms`}
                description={t`Last Updated: June 8, 2021`}
            >
                <TermsOfServices />
            </TermsContent>
        </TermsPage>
    );
};

export const getStaticProps: GetStaticProps = async ({ locale }) => {
    const translation = await loadTranslation(locale!, process.env.NODE_ENV === 'production');
    const [page] = await Promise.all([fetchPage('terms', locale)]);

    return {
        props: {
            translation,
            menu,
            page,
        },
    };
};

export default TosPage;
