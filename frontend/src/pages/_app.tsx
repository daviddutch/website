import { config } from '@fortawesome/fontawesome-svg-core';
import '@fortawesome/fontawesome-svg-core/styles.css';
import '../styles/globals.css';
import type { AppProps } from 'next/app';
import { initTranslation } from '@/utils/i18n';
import { i18n } from '@lingui/core';
import { useRouter } from 'next/router';
import React, { useEffect, useRef } from 'react';
import Head from 'next/head';
import { I18nProvider } from '@lingui/react';
import { Messages } from '@lingui/core/esm/i18n';
import { Page } from '@/types/Page';
import { SeoHeaders } from '@/components/shared/SeoHeaders';
import { CookieNotice } from '@/components/shared/CookieNotice';
import { GlobalProvider, getDateFnsLocale } from '@/components/shared/GlobalContext';
import { Post } from '@/types/Post';
import { Analytics } from '@vercel/analytics/react';

config.autoAddCss = false;

initTranslation(i18n);

type BaseAppProps = {
    translation: Messages;
    page: Page;
    post?: Post;
    translatedPaths: { [key: string]: string };
};

function BaseApp({ Component, pageProps }: AppProps<BaseAppProps>) {
    const { page, translation, post, translatedPaths } = pageProps;
    const router = useRouter();
    const currentLocale = router.locale || router.defaultLocale!;
    const firstRender = useRef(true);

    // run only once on the first render (for server side)
    if (translation && firstRender.current) {
        i18n.load(currentLocale, translation);
        i18n.activate(currentLocale);
        firstRender.current = false;
    }

    useEffect(() => {
        if (translation) {
            i18n.load(currentLocale, translation);
            i18n.activate(currentLocale);
        }
    }, [currentLocale, translation]);

    return (
        <>
            <Head>
                <link rel="shortcut icon" href="/icon.svg" />
                {router.locales!.concat('x-default').map((locale: string) => {
                    const localePath = ['x-default', 'en'].includes(locale) ? '' : `/${locale}`;

                    const path = translatedPaths && translatedPaths[locale] ? translatedPaths[locale] : router.asPath;
                    const href = `${process.env.NEXT_PUBLIC_SITE_URL}${localePath}${path}`;

                    return <link key={locale} rel="alternate" hrefLang={locale} href={href} />;
                })}
            </Head>
            <main>
                <I18nProvider i18n={i18n}>
                    <GlobalProvider locale={getDateFnsLocale(currentLocale)} translatedPaths={translatedPaths}>
                        <CookieNotice />
                        {page?.attributes?.seo && <SeoHeaders seo={page.attributes.seo} post={post} />}
                        <Component {...pageProps} />
                        <Analytics />
                    </GlobalProvider>
                </I18nProvider>
            </main>
        </>
    );
}

export default BaseApp;
