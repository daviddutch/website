import { Header } from '@/components/shared/Header';
import { CallToAction } from '@/components/cta/CallToAction';
import { Footer } from '@/components/shared/Footer';
import { fetchAPI } from '@/utils/api';
import { BlogNavBar } from '@/components/blog/BlogNavBar';
import { BlogSection } from '@/components/blog/BlogSection';
import { Pagination } from '@/components/shared/Pagination';
import { Post } from '@/types/Post';
import { Category } from '@/types/Category';
import { GetStaticProps } from 'next';
import { loadTranslation } from '@/utils/i18n';
import { fetchPage } from '@/services/page';
import { Page } from '@/types/Page';
import { countArticles } from '@/services/blog';

type Props = {
    page: Page;
    articles: Post[];
    categories: Category[];
    category: Category;
    currentPage: number;
    count: number;
};

const CategoryPage = ({ articles, categories, category, currentPage, count }: Props) => {
    return (
        <>
            <Header />
            <BlogNavBar categories={categories} current={category} />
            <BlogSection
                title={category.attributes.name}
                description={category.attributes.description}
                posts={articles}
            />
            <div className="bg-gray-50 py-4 text-center">
                <Pagination
                    currentPage={currentPage || 1}
                    totalCount={count}
                    pageSize={9}
                    getUrl={(page) => {
                        return page === 1 ? '/category' : `/category/${category.attributes.slug}/${page}`;
                    }}
                />
            </div>
            <CallToAction />
            <Footer />
        </>
    );
};

export const getStaticPaths = async ({ locales }: { locales: string[] }) => {
    const fetchCategories = async (locale: string) => {
        return await fetchAPI<Category[]>('/categories', {
            locale: locale,
            fields: ['slug'],
            pagination: {
                start: 0,
                limit: 100,
            },
        });
    };

    const values = await Promise.all(
        locales.map(async (locale) => {
            const categories = await fetchCategories(locale);
            const values = await Promise.all(
                categories.data.map(async (category) => {
                    const count = await countArticles(locale, 0, 0, { categories: category.id });
                    const pages = Math.ceil(count / 9);
                    return Array.from({ length: pages }, (_, i) => ({
                        params: { slug: category.attributes.slug, page: (i + 1).toString() },
                        locale: locale,
                    }));
                })
            );
            return values.flat();
        })
    );

    return {
        paths: values.flat(),
        fallback: false,
    };
};

export const getStaticProps: GetStaticProps = async ({ locale, params }) => {
    const currentPage = params?.page ? parseInt(params.page.toString()) : 1;
    const translation = await loadTranslation(locale!, process.env.NODE_ENV === 'production');
    const categoriesRes = await fetchAPI<Category[]>('/categories', {
        locale: locale,
        populate: '*',
    });
    const category = categoriesRes.data.find((c) => c.attributes.slug == params?.slug);
    // Run API calls in parallel
    const [page, articlesRes] = await Promise.all([
        fetchPage('blog', locale),
        fetchAPI('/articles', {
            locale: locale,
            sort: {
                publicationDate: 'desc',
            },
            filters: {
                categories: category?.id,
            },
            populate: {
                image: true,
                categories: { populate: true },
                author: {
                    populate: ['photo'],
                },
            },
            pagination: {
                start: currentPage * 9 - 9,
                limit: 9,
            },
        }),
    ]);

    const count = await countArticles(locale!, 0, 0, { categories: category?.id });

    return {
        props: {
            page,
            articles: articlesRes.data,
            categories: categoriesRes.data,
            category: category,
            seo: category?.attributes.seo,
            translation,
            currentPage,
            count,
        },
        revalidate: 60,
    };
};

export default CategoryPage;
