import { Header } from '@/components/shared/Header';
import { SignupForm } from '@/components/signup/SignupForm';
import { Footer } from '@/components/shared/Footer';
import { Messages } from '@lingui/core/esm/i18n';
import { loadTranslation } from '@/utils/i18n';
import { GetStaticProps, NextPage } from 'next';
import { AlreadyHaveAccount } from '@/components/signup/AlreadyHaveAccount';
import { fetchPage } from '@/services/page';
import { Page } from '@/types/Page';
import { GoogleReCaptchaProvider } from 'react-google-recaptcha-v3';

interface PageProps {
    translation: Messages[];
    page: Page;
}

const Signup: NextPage<PageProps> = ({ page }) => {
    return (
        <GoogleReCaptchaProvider
            reCaptchaKey={process.env.NEXT_PUBLIC_RECAPTHA_SITE_KEY || ''}
            scriptProps={{
                async: false,
                defer: true,
                appendTo: 'body',
                nonce: undefined,
            }}
            container={{
                parameters: {
                    badge: 'bottomleft',
                },
            }}
        >
            <Header />
            <SignupForm />
            <AlreadyHaveAccount />
            <Footer />
        </GoogleReCaptchaProvider>
    );
};
export const getStaticProps: GetStaticProps = async ({ locale }) => {
    const translation = await loadTranslation(locale!, process.env.NODE_ENV === 'production');
    // Run API calls in parallel
    const [page] = await Promise.all([fetchPage('signup', locale)]);

    return {
        props: {
            page,
            translation,
        },
    };
};

export default Signup;
