import { Header } from '@/components/shared/Header';
import { CallToAction } from '@/components/cta/CallToAction';
import { Footer } from '@/components/shared/Footer';
import { fetchAPI } from '@/utils/api';
import { BlogNavBar } from '@/components/blog/BlogNavBar';
import { BlogSection } from '@/components/blog/BlogSection';
import { Pagination } from '@/components/shared/Pagination';
import { Post } from '@/types/Post';
import { Category } from '@/types/Category';
import { GetStaticProps } from 'next';
import { loadTranslation } from '@/utils/i18n';
import { fetchPage } from '@/services/page';
import { Page } from '@/types/Page';
import { t } from '@lingui/macro';
import { countArticles } from '@/services/blog';

type Props = {
    page: Page;
    articles: Post[];
    categories: Category[];
    count: number;
};

const Index = ({ articles, categories, count }: Props) => {
    return (
        <>
            <Header />
            <BlogNavBar categories={categories} />
            <BlogSection title={t`Blog`} description={''} posts={articles} />
            <div className="bg-gray-50 py-4 text-center">
                <Pagination
                    currentPage={1}
                    totalCount={count}
                    pageSize={9}
                    getUrl={(page) => {
                        return page === 1 ? '/blog' : `/blog/page/${page}`;
                    }}
                />
            </div>
            <CallToAction />
            <Footer />
        </>
    );
};

export const getStaticProps: GetStaticProps = async ({ locale }) => {
    const translation = await loadTranslation(locale!, process.env.NODE_ENV === 'production');

    const count = await countArticles(locale!);

    // Run API calls in parallel
    const [page, articlesRes, categoriesRes] = await Promise.all([
        fetchPage('blog', locale),
        fetchAPI<Post[]>('/articles', {
            locale: locale,
            sort: {
                publicationDate: 'desc',
            },
            populate: {
                image: true,
                categories: { populate: true },
                author: {
                    populate: ['photo'],
                },
            },
            pagination: {
                start: 0,
                limit: 9,
            },
        }),
        fetchAPI('/categories', { locale: locale, populate: '*' }),
    ]);

    return {
        props: {
            page,
            articles: articlesRes.data,
            categories: categoriesRes.data,
            translation,
            count,
        },
        revalidate: 60,
    };
};

export default Index;
