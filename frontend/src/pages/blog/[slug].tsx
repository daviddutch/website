import { Header } from '@/components/shared/Header';
import { BlogPostContent } from '@/components/blog/BlogPostContent';
import { Footer } from '@/components/shared/Footer';
import { fetchAPI } from '@/utils/api';
import { Post } from '@/types/Post';
import { GetStaticProps } from 'next';
import { loadTranslation } from '@/utils/i18n';
import { CallToActionMain } from '@/components/cta/CallToActionMain';
import { BlogSection } from '@/components/blog/BlogSection';
import { t } from '@lingui/macro';
import { FormatDate } from '@/components/shared/FormatDate';
import { StrapiImage } from '@/components/shared/StrapiImage';

const BlogPost = ({ post, relatedPosts }: { post: Post; relatedPosts: Post[] }) => {
    const author = post.attributes.author.data?.attributes;
    return (
        <>
            <Header />
            <div className="bg-white">
                <div className="mx-auto max-w-7xl py-10 px-4 sm:py-16 sm:px-6 lg:px-8">
                    <div className="text-center">
                        <p className="text-lg font-semibold text-blue-600">
                            {post.attributes.categories.data.map((c) => c.attributes.name).join(' | ')}
                        </p>
                        <h1 className="mt-1 bg-gradient-to-r from-blue-400 to-blue-700 bg-clip-text bg-clip-text py-3 text-3xl font-bold font-extrabold tracking-tight text-gray-900 text-transparent sm:text-4xl lg:text-5xl">
                            {post.attributes.title}
                        </h1>
                        {author && (
                            <p className="mx-auto mt-5 max-w-xl text-xl text-gray-500">
                                <StrapiImage className="h-10 w-10 rounded-full" media={author.photo} />
                                {author.firstName + ' ' + author.lastName}
                                {' | '}
                                <FormatDate format={'PPPP'} date={post.attributes.publicationDate} />
                            </p>
                        )}
                    </div>
                </div>
            </div>
            <BlogPostContent post={post} />
            <BlogSection posts={relatedPosts} title={t`Related posts`} description={''} />
            <CallToActionMain />
            <Footer />
        </>
    );
};

export async function getStaticPaths({ locales }: { locales: string[] }) {
    const fetchArticles = async (locale: string) => {
        return await fetchAPI<Post[]>('/articles', {
            fields: ['slug'],
            locale: locale,
            pagination: {
                start: 0,
                limit: 100,
            },
        });
    };
    const paths = await Promise.all(
        locales.map(async (locale) =>
            fetchArticles(locale).then((pages) =>
                pages.data.map((page) => ({
                    params: { slug: page.attributes.slug },
                    locale,
                }))
            )
        )
    ).then((values) => values.flat());

    return {
        paths: paths,
        fallback: false,
    };
}

export const getStaticProps: GetStaticProps = async ({ params, locale }) => {
    const translation = await loadTranslation(locale!, process.env.NODE_ENV === 'production');
    const pagesRes = await fetchAPI<Post[]>('/articles', {
        locale: locale,
        filters: {
            slug: params?.slug,
        },
        populate: {
            image: true,
            localizations: true,
            fields: ['*'],
            author: { populate: true },
            categories: { populate: true },
            tags: { populate: true },
            seo: { populate: true },
        },
    });
    const post = pagesRes.data[0];
    const translatedPaths = Object.fromEntries(
        post.attributes.localizations.data.map((l) => [l.attributes.locale, `/blog/${l.attributes.slug}`])
    );
    translatedPaths[locale!] = `/blog/${post.attributes.slug}`;

    const postCategories = post.attributes.categories.data;
    const relatedRes = await fetchAPI<Post[]>('/articles', {
        locale: locale,
        filters: {
            categories: postCategories.length > 0 ? postCategories[0].id : '',
        },
        pagination: {
            start: 0,
            limit: 3,
        },
        populate: {
            image: true,
            fields: ['*'],
            author: { populate: true },
            categories: { populate: true },
            tags: { populate: true },
            seo: { populate: true },
        },
    });

    return {
        props: {
            post: post,
            page: { attributes: { seo: { ...post.attributes.seo, article: true } } },
            relatedPosts: relatedRes.data,
            translation,
            translatedPaths,
        },
        revalidate: 60,
    };
};

export default BlogPost;
