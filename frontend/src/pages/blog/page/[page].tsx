import { Header } from '@/components/shared/Header';
import { CallToAction } from '@/components/cta/CallToAction';
import { Footer } from '@/components/shared/Footer';
import { fetchAPI } from '@/utils/api';
import { BlogNavBar } from '@/components/blog/BlogNavBar';
import { BlogSection } from '@/components/blog/BlogSection';
import { Pagination } from '@/components/shared/Pagination';
import { Post } from '@/types/Post';
import { Category } from '@/types/Category';
import { GetStaticProps } from 'next';
import { loadTranslation } from '@/utils/i18n';
import { fetchPage } from '@/services/page';
import { Page } from '@/types/Page';
import { t, Trans } from '@lingui/macro';
import { countArticles } from '@/services/blog';
import { useRouter } from 'next/router';
import React from 'react';

type Props = {
    page: Page;
    articles: Post[];
    categories: Category[];
    currentPage: number;
    count: number;
};

const BlogPage = ({ articles, categories, currentPage, count }: Props) => {
    const router = useRouter();

    if (router.isFallback) {
        return (
            <div>
                <Trans>Loading...</Trans>
            </div>
        );
    }
    return (
        <>
            <Header />
            {categories && <BlogNavBar categories={categories} />}
            <BlogSection title={t`Blog`} description={''} posts={articles} />
            <div className="bg-gray-50 py-4 text-center">
                <Pagination
                    currentPage={currentPage}
                    totalCount={count}
                    pageSize={9}
                    getUrl={(page) => {
                        return page === 1 ? '/blog' : `/blog/page/${page}`;
                    }}
                />
            </div>
            <CallToAction />
            <Footer />
        </>
    );
};

export async function getStaticPaths({ locales }: { locales: string[] }) {
    const values = await Promise.all(
        locales.map(async (locale) => {
            const count = await countArticles(locale);
            const pages = Math.ceil(count / 9);
            return Array.from({ length: pages }, (_, i) => ({
                params: { page: (i + 1).toString(), locale: locale },
            }));
        })
    );

    return {
        fallback: true,
        paths: values.flat(),
    };
}

export const getStaticProps: GetStaticProps = async ({ locale, params }) => {
    const currentPage = params?.page ? parseInt(params.page.toString()) : 1;

    const translation = await loadTranslation(locale!, process.env.NODE_ENV === 'production');
    const [page, articlesRes, categoriesRes] = await Promise.all([
        fetchPage('blog', locale),
        fetchAPI('/articles', {
            locale: locale,
            sort: {
                publicationDate: 'desc',
            },
            populate: {
                image: true,
                categories: { populate: true },
                author: {
                    populate: ['photo'],
                },
            },
            pagination: {
                start: currentPage * 9 - 9,
                limit: 9,
            },
        }),
        fetchAPI('/categories', { locale: locale, populate: '*' }),
    ]);
    const count = await countArticles(locale!);

    return {
        props: {
            page,
            articles: articlesRes.data,
            categories: categoriesRes.data,
            translation,
            currentPage,
            count,
        },
        revalidate: 60,
    };
};

export default BlogPage;
