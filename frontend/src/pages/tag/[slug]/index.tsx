import { Header } from '@/components/shared/Header';
import { CallToAction } from '@/components/cta/CallToAction';
import { Footer } from '@/components/shared/Footer';
import { fetchAPI } from '@/utils/api';
import { BlogSection } from '@/components/blog/BlogSection';
import { Pagination } from '@/components/shared/Pagination';
import { Post } from '@/types/Post';
import { Tag } from '@/types/Tag';
import { GetStaticProps } from 'next';
import { loadTranslation } from '@/utils/i18n';
import { fetchPage } from '@/services/page';
import { Page } from '@/types/Page';
import { countArticles } from '@/services/blog';

type Props = {
    page: Page;
    articles: Post[];
    tag: Tag;
    currentPage: number;
    count: number;
};

const TagPage = ({ articles, tag, currentPage, count }: Props) => {
    return (
        <>
            <Header />
            <BlogSection title={tag.attributes.name} description={''} posts={articles} />
            <div className="bg-gray-50 py-4 text-center">
                <Pagination
                    currentPage={currentPage || 1}
                    totalCount={count}
                    pageSize={9}
                    getUrl={(page) => {
                        return page === 1 ? '/tag' : `/tag/${tag.attributes.slug}/${page}`;
                    }}
                />
            </div>
            <CallToAction />
            <Footer />
        </>
    );
};

export const getStaticPaths = async ({ locales }: { locales: string[] }) => {
    const fetchTags = async (locale: string) => {
        return await fetchAPI<Tag[]>('/tags', {
            locale: locale,
            fields: ['slug'],
            pagination: {
                start: 0,
                limit: 100,
            },
        });
    };
    const values = await Promise.all(
        locales.map(async (locale) =>
            fetchTags(locale).then((pages) =>
                pages.data.map((page) => ({
                    params: { slug: page.attributes.slug },
                    locale,
                }))
            )
        )
    );

    return {
        paths: values.flat(),
        fallback: false,
    };
};

export const getStaticProps: GetStaticProps = async ({ locale, params }) => {
    const currentPage = 1;
    const translation = await loadTranslation(locale!, process.env.NODE_ENV === 'production');
    const tagsRes = await fetchAPI<Tag[]>('/tags', {
        locale: locale,
        populate: '*',
        pagination: {
            start: 0,
            limit: 100,
        },
    });
    const tag = tagsRes.data.find((c) => c.attributes.slug == params?.slug)!;
    const [page, articlesRes] = await Promise.all([
        fetchPage('blog', locale),
        fetchAPI('/articles', {
            locale: locale,
            sort: {
                publicationDate: 'desc',
            },
            filters: {
                tags: tag?.id,
            },
            populate: {
                image: true,
                tags: { populate: true },
                categories: { populate: true },
                author: {
                    populate: ['photo'],
                },
            },
            pagination: {
                start: currentPage * 9 - 9,
                limit: 9,
            },
        }),
    ]);

    const count = await countArticles(locale!, 0, 0, { tags: tag?.id });

    const translatedPaths = Object.fromEntries(
        tag.attributes.localizations.data.map((l) => [l.attributes.locale, `/tag/${l.attributes.slug}`])
    );
    translatedPaths[locale!] = `/tag/${tag.attributes.slug}`;

    return {
        props: {
            page,
            articles: articlesRes.data,
            tag: tag,
            seo: tag?.attributes.seo,
            translation,
            currentPage,
            count,
            translatedPaths,
        },
        revalidate: 60,
    };
};

export default TagPage;
