import { Header } from '@/components/shared/Header';
import { Footer } from '@/components/shared/Footer';
import { fetchAPI } from '@/utils/api';
import { GetStaticProps } from 'next';
import { loadTranslation } from '@/utils/i18n';
import { PageHeader } from '@/components/shared/PageHeader';
import { Outings } from '@/components/jobs/Outings';
import { OurMission } from '@/components/jobs/OurMission';
import { SectionHeader } from '@/components/shared/SectionHeader';
import { Benefits } from '@/components/jobs/Benefits';
import { OpenPositions } from '@/components/jobs/OpenPositions';
import { Job } from '@/types/Job';
import { fetchPage } from '@/services/page';
import { Page } from '@/types/Page';
import { CtaType } from '@/types/LandingPage';
import { CallToAction } from '@/components/cta/CallToAction';
import { t } from '@lingui/macro';

type Props = {
    page: Page;
    jobs: Job[];
};

const Index = ({ jobs }: Props) => {
    return (
        <>
            <Header />
            <PageHeader title={t`We are hiring!`} description={t`Join our team`} />
            <Outings />
            <OurMission />
            <div className="mx-auto max-w-5xl py-16 px-4">
                <SectionHeader sectionName={t`Benefits`} title={t`Work environment`} />
                <Benefits />
            </div>
            <div className="mx-auto max-w-5xl py-16 px-4">
                <SectionHeader sectionName={t`Jobs`} title={t`Open positions`} />
                <OpenPositions jobs={jobs} />
            </div>
            <CallToAction type={CtaType.Main} />
            <Footer />
        </>
    );
};

export const getStaticProps: GetStaticProps = async ({ locale }) => {
    const translation = await loadTranslation(locale!, process.env.NODE_ENV === 'production');
    // Run API calls in parallel
    const [page, jobsRes] = await Promise.all([
        fetchPage('jobs', locale),
        fetchAPI('/jobs', { locale: locale, populate: ['image', 'hosts'] }),
    ]);

    return {
        props: {
            page,
            jobs: jobsRes.data,
            translation,
        },
        revalidate: 60,
    };
};

export default Index;
