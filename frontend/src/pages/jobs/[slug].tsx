import { Header } from '@/components/shared/Header';
import { SectionHeader } from '@/components/shared/SectionHeader';
import { Footer } from '@/components/shared/Footer';
import { fetchAPI } from '@/utils/api';
import { GetStaticProps } from 'next';
import { loadTranslation } from '@/utils/i18n';
import { Job } from '@/types/Job';
import { JobContent } from '@/components/jobs/JobContent';
import { CallToAction } from '@/components/cta/CallToAction';
import { CtaType } from '@/types/LandingPage';
import Link from 'next/link';
import { Trans } from '@lingui/macro';

const JobPage = ({ job }: { job: Job }) => {
    return (
        <>
            <Header />
            <SectionHeader title={job.attributes.title} description={''} sectionName={'We are hiring a'}>
                <Link
                    href={job.attributes.applyUrl}
                    className="mt-4 inline-flex items-center rounded-md border border-transparent bg-blue-600 px-4 py-2 text-sm font-medium text-white shadow-sm hover:bg-blue-700 focus:outline-none"
                >
                    <Trans>Apply here</Trans>
                </Link>
            </SectionHeader>

            <JobContent job={job} />
            <CallToAction type={CtaType.Job} />
            <Footer />
        </>
    );
};

export async function getStaticPaths({ locales }: { locales: string[] }) {
    const fetchJobs = async (locale: string) => {
        return await fetchAPI<Job[]>('/jobs', {
            fields: ['slug'],
            locale: locale,
        });
    };
    const paths = await Promise.all(
        locales.map(async (locale) =>
            fetchJobs(locale).then((pages) =>
                pages.data.map((page) => ({
                    params: { slug: page.attributes.slug },
                    locale,
                }))
            )
        )
    ).then((values) => values.flat());

    return {
        paths: paths,
        fallback: false,
    };
}

export const getStaticProps: GetStaticProps = async ({ params, locale }) => {
    const translation = await loadTranslation(locale!, process.env.NODE_ENV === 'production');
    const pagesRes = await fetchAPI<Job[]>('/jobs', {
        locale: locale,
        filters: {
            slug: params?.slug,
        },
        populate: {
            hosts: {
                populate: {
                    fields: ['*'],
                    photo: '*',
                },
            },
        },
    });

    return {
        props: { job: pagesRes.data[0], translation },
        revalidate: 60,
    };
};

export default JobPage;
