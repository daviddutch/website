import type { GetStaticProps } from 'next';
import { loadTranslation } from '@/utils/i18n';
import { Header } from '@/components/shared/Header';
import { Footer } from '@/components/shared/Footer';
import React from 'react';
import { fetchAPI } from '@/utils/api';
import { LandingPage } from '@/types/LandingPage';
import { LandingPageContent } from '@/components/shared/LandingPageContent';
import { HeroHeaderHome } from '@/components/shared/HeroHeaderHome';

export const getStaticProps: GetStaticProps = async ({ locale }) => {
    const translation = await loadTranslation(locale!, process.env.NODE_ENV === 'production');
    const pageRes = await fetchAPI<LandingPage>('/homepage', {
        locale: locale,
        populate: {
            title: '*',
            seo: { populate: '*' },
            heroHeader: {
                populate: {
                    image: '*',
                    buttons: { populate: '*' },
                    highlights: { populate: '*' },
                },
            },
            content: {
                populate: {
                    title: '*',
                    image: '*',
                    highlights: { populate: '*' },
                    logos: { populate: '*' },
                    testimonials: {
                        populate: {
                            logo: { populate: ['logoDark', 'logoLight', 'logoColor'] },
                            photo: { populate: true },
                        },
                    },
                    articles: {
                        populate: {
                            image: true,
                            categories: { populate: true },
                            author: {
                                populate: ['photo'],
                            },
                        },
                    },
                },
            },
        },
    });

    return {
        props: {
            page: pageRes.data,
            translation,
        },
        revalidate: 60,
    };
};
const Home = ({ page }: { page: LandingPage }) => {
    const content = page.attributes.content;
    const heroHeader = page.attributes.heroHeader;
    return (
        <>
            <Header />
            <HeroHeaderHome title={heroHeader.title} text={heroHeader.text} image={heroHeader.image} />
            <LandingPageContent content={content} isHome={true} />
            <Footer />
        </>
    );
};

export default Home;
