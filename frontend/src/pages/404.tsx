import { Header } from '@/components/shared/Header';
import { Footer } from '@/components/shared/Footer';
import { GetStaticProps, NextPage } from 'next';
import { loadTranslation } from '@/utils/i18n';
import { Trans } from '@lingui/macro';
import { fetchPage } from '@/services/page';
import { Messages } from '@lingui/core/esm/i18n';
import { Page } from '@/types/Page';
import Link from 'next/link';
import { Button } from '@/components/shared/Button';

export const getStaticProps: GetStaticProps = async ({ locale }) => {
    const translation = await loadTranslation(locale!, process.env.NODE_ENV === 'production');
    const [page] = await Promise.all([fetchPage('404', locale)]);
    return {
        props: {
            page,
            translation,
        },
    };
};

interface PageProps {
    translation: Messages[];
    page: Page;
}

const NotFoundPage: NextPage<PageProps> = ({ page }) => {
    return (
        <>
            <Header />
            <section className="flex h-full items-center p-16 text-gray-500">
                <div className="container mx-auto my-8 flex flex-col items-center justify-center px-5">
                    <div className="max-w-md text-center">
                        <h2 className="mb-8 text-9xl font-extrabold text-gray-600">
                            <span className="sr-only">Error</span>404
                        </h2>
                        <p className="text-2xl font-semibold md:text-3xl">
                            <Trans>Sorry, we could not find this page.</Trans>
                        </p>
                        <p className="mt-4 mb-8 text-gray-400">
                            <Trans>But dont worry, you can find plenty of other things on our homepage.</Trans>
                        </p>
                        <Link href="/" passHref legacyBehavior>
                            <Button>
                                <Trans>Back to homepage</Trans>
                            </Button>
                        </Link>
                    </div>
                </div>
            </section>
            <Footer />
        </>
    );
};

export default NotFoundPage;
