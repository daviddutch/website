import { Header } from '@/components/shared/Header';
import { GetStaticProps, NextPage } from 'next';
import { loadTranslation } from '@/utils/i18n';
import { t } from '@lingui/macro';
import { Footer } from '@/components/shared/Footer';
import { FAQ } from '@/components/shared/FAQ';
import React from 'react';
import { CustomerLogos } from '@/components/shared/CustomerLogos';
import { Messages } from '@lingui/core/esm/i18n';
import { PageHeader } from '@/components/shared/PageHeader';
import { Privacy } from '@/components/security/Privacy';
import { SocCompliance } from '@/components/security/SocCompliance';
import { Highlight, SecurityHighlights } from '@/components/security/SecurityHighlights';
import { faq, highlights } from '@/state/securityData';
import { CallToAction } from '@/components/cta/CallToAction';
import { Page } from '@/types/Page';
import { Logo } from '@/types/Logo';
import { fetchPage } from '@/services/page';
import { fetchCustomerLogos } from '@/services/logos';
import { LogosSectionType } from '@/types/LandingPage';

interface PageProps {
    translation: Messages[];
    page: Page;
    logos: Logo[];
    faq: { question: string; answer: string }[];
    highlights: Highlight[];
}

const SecurityPage: NextPage<PageProps> = ({ page, logos, faq, highlights }) => {
    return (
        <>
            <Header />
            <div className="relative overflow-hidden">
                <div
                    className="h-full w-1/3 shadow-dotted-fade lg:absolute lg:right-0 lg:bg-dotted lg:bg-dotted-20 lg:bg-dotted-10"
                    aria-hidden="true"
                ></div>
                <PageHeader
                    title={t`Security`}
                    description={t`Your data security is our top priority. That’s why all your data are stored in Switzerland in ISO 27001 certified data centers.`}
                />
            </div>
            <SecurityHighlights highlights={highlights} />
            <SocCompliance
                title={t`WEDO is SOC 2 Type II Compliant`}
                subHeader={t`Security is an integral part of our DNA`}
            >
                {t`The SOC 2 Type II audit is an industry-recognized security certification for
                    software-as-a-service (SaaS) companies. It validates that your data is secure, safe, and controlled
                    with WEDO. The 6-month long audit involved a thorough analysis of our controls, the tests we perform
                    to assess their effectiveness, and the results of those tests.`}
            </SocCompliance>
            <CustomerLogos
                logos={logos}
                type={LogosSectionType.Grid}
                title={t`Hundreds of companies rely on WEDO to keep their data protected`}
            />
            <FAQ entries={faq} />
            <Privacy />
            <CallToAction />
            <Footer />
        </>
    );
};

export const getStaticProps: GetStaticProps = async ({ locale }) => {
    const translation = await loadTranslation(locale!, process.env.NODE_ENV === 'production');
    const [page, logosRes] = await Promise.all([
        fetchPage('security', locale),
        fetchCustomerLogos({ showSecurity: true }),
    ]);
    return {
        props: {
            translation,
            page,
            logos: logosRes.data,
            faq,
            highlights,
        },
    };
};

export default SecurityPage;
