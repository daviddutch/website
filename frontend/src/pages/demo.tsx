import { Header } from '@/components/shared/Header';
import { GetStaticProps, NextPage } from 'next';
import { loadTranslation } from '@/utils/i18n';
import { t } from '@lingui/macro';
import { Footer } from '@/components/shared/Footer';
import React from 'react';
import { fetchAPI } from '@/utils/api';
import { Messages } from '@lingui/core/esm/i18n';
import { PageHeader } from '@/components/shared/PageHeader';
import { CallToAction } from '@/components/cta/CallToAction';
import { SectionHeader } from '@/components/shared/SectionHeader';
import { Contacts } from '@/components/demo/Contacts';
import { Employee } from '@/types/Employee';
import { fetchPage } from '@/services/page';
import { InlineWidget } from 'react-calendly';

interface PageProps {
    translation: Messages[];
    page: any;
    salesPeople: Employee[];
}

const SecurityPage: NextPage<PageProps> = ({ salesPeople }) => {
    return (
        <>
            <Header />
            <PageHeader
                title={t`Schedule a demo`}
                description={t`Choose a date for a demo directly in our calendar.`}
            />
            <div className="grid h-[1100px] lg:h-[710px]">
                <InlineWidget
                    styles={{}}
                    url="https://calendly.com/wedo-team/demo-en?embed_domain=wedo.swiss&embed_type=Inline&text_color=00263a&primary_color=0072ce"
                />
            </div>
            <div className="mx-auto max-w-6xl py-16 px-4">
                <SectionHeader
                    sectionName={t`Contact us`}
                    title={t`Do you have questions?`}
                    description={t`Our team is here to help`}
                />
                <Contacts contacts={salesPeople} />
            </div>
            <CallToAction />
            <Footer />
        </>
    );
};

export const getStaticProps: GetStaticProps = async ({ locale }) => {
    const translation = await loadTranslation(locale!, process.env.NODE_ENV === 'production');
    const [page, employeesRes] = await Promise.all([
        fetchPage('demo', locale),
        fetchAPI('/employees', {
            locale: locale,
            filters: {
                isSales: true,
            },
            sort: ['order', 'firstName'],
            populate: { photo: true },
        }),
    ]);

    return {
        props: {
            translation,
            page,
            salesPeople: employeesRes.data,
        },
    };
};

export default SecurityPage;
