import { Header } from '@/components/shared/Header';
import { Footer } from '@/components/shared/Footer';
import { GetStaticProps } from 'next';
import { loadTranslation } from '@/utils/i18n';
import { LandingPage } from '@/types/LandingPage';
import { LandingPageContent } from '@/components/shared/LandingPageContent';
import { fetchLandingPage, fetchLandingPagePaths } from '@/services/landingPage';
import { HeroHeaderDiscover } from '@/components/shared/HeroHeaderDiscover';

const FeatureLandingPage = ({ page }: { page: LandingPage }) => {
    const content = page.attributes.content;
    const heroHeader = page.attributes.heroHeader;
    return (
        <>
            <Header />

            <HeroHeaderDiscover title={heroHeader.title} text={heroHeader.text} />

            <LandingPageContent content={content} />

            <Footer />
        </>
    );
};

export async function getStaticPaths({ locales }: { locales: string[] }) {
    return {
        paths: await fetchLandingPagePaths('feature', locales),
        fallback: false,
    };
}

export const getStaticProps: GetStaticProps = async ({ params, locale }) => {
    const translation = await loadTranslation(locale!, process.env.NODE_ENV === 'production');
    const page = await fetchLandingPage(params?.slug as string, locale!);

    return {
        props: { page: page, translation, locale },
        revalidate: 60,
    };
};

export default FeatureLandingPage;
