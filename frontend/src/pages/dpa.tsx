import { GetStaticProps, NextPage } from 'next';
import { loadTranslation } from '@/utils/i18n';
import React from 'react';
import { Messages } from '@lingui/core/esm/i18n';
import { menu } from '@/state/termsData';
import { fetchPage } from '@/services/page';
import { Page } from '@/types/Page';
import { TermsPage } from '@/components/terms/TermsPage';
import { TermsContent } from '@/components/terms/TermsContent';
import { Menu } from '@/types/Menu';
import { t } from '@lingui/macro';
import { DataProcessingAgreement } from '@/components/terms/DataProcessingAgreement';

interface PageProps {
    translation: Messages[];
    page: Page;
    menu: Menu[];
}

const DpaPage: NextPage<PageProps> = ({ menu }) => {
    return (
        <TermsPage menu={menu} currentSlug={'dpa'}>
            <TermsContent
                title={t`Data Processing Agreement`}
                sectionName={t`Terms`}
                description={t`Last Updated: June 8, 2021`}
            >
                <DataProcessingAgreement />
            </TermsContent>
        </TermsPage>
    );
};

export const getStaticProps: GetStaticProps = async ({ locale }) => {
    const translation = await loadTranslation(locale!, process.env.NODE_ENV === 'production');
    const [page] = await Promise.all([fetchPage('dpa', locale)]);

    return {
        props: {
            translation,
            menu,
            page,
        },
    };
};

export default DpaPage;
