import { Header } from '@/components/shared/Header';
import { CompanyContacts } from '@/components/contact/CompanyContacts';
import { ContactForm } from '@/components/contact/ContactForm';
import { CallToAction } from '@/components/cta/CallToAction';
import { Footer } from '@/components/shared/Footer';
import { GetStaticProps, NextPage } from 'next';
import { loadTranslation } from '@/utils/i18n';
import { fetchPage } from '@/services/page';
import { GoogleReCaptchaProvider } from 'react-google-recaptcha-v3';
import { fetchAPI } from '@/utils/api';
import { SectionHeader } from '@/components/shared/SectionHeader';
import { t } from '@lingui/macro';
import React from 'react';
import { Messages } from '@lingui/core/esm/i18n';
import { Employee } from '@/types/Employee';
import { Contacts } from '@/components/demo/Contacts';

interface PageProps {
    translation: Messages[];
    page: any;
    salesPeople: Employee[];
}

const Contact: NextPage<PageProps> = ({ salesPeople }) => {
    return (
        <GoogleReCaptchaProvider
            reCaptchaKey={process.env.NEXT_PUBLIC_RECAPTHA_SITE_KEY || ''}
            scriptProps={{
                async: false,
                defer: true,
                appendTo: 'body',
                nonce: undefined,
            }}
            container={{
                parameters: {
                    badge: 'bottomleft',
                },
            }}
        >
            <Header />
            <ContactForm />
            <CompanyContacts />
            <div className="mx-auto max-w-6xl py-16 px-4">
                <SectionHeader
                    sectionName={t`Do you want to discover WEDO?`}
                    title={t`Our sales team is here to help`}
                />
                <Contacts contacts={salesPeople} />
            </div>
            <CallToAction />
            <Footer />
        </GoogleReCaptchaProvider>
    );
};

export const getStaticProps: GetStaticProps = async ({ locale }) => {
    const translation = await loadTranslation(locale!, process.env.NODE_ENV === 'production');

    const [page, employeesRes] = await Promise.all([
        fetchPage('contact', locale),
        fetchAPI('/employees', {
            locale: locale,
            filters: {
                isSales: true,
            },
            sort: ['order', 'firstName'],
            populate: { photo: true },
        }),
    ]);

    return {
        props: {
            page,
            translation,
            salesPeople: employeesRes.data,
        },
    };
};

export default Contact;
