import { Header } from '@/components/shared/Header';
import { SectionHeader } from '@/components/shared/SectionHeader';
import { Footer } from '@/components/shared/Footer';
import { fetchAPI } from '@/utils/api';
import { GetStaticProps } from 'next';
import { loadTranslation } from '@/utils/i18n';
import { Event } from '@/types/Event';
import { EventContent } from '@/components/events/EventContent';
import { EventInfo } from '@/components/events/EventInfo';
import Link from 'next/link';
import { CallToAction } from '@/components/cta/CallToAction';

const EventPage = ({ event }: { event: Event }) => {
    return (
        <>
            <Header />
            <SectionHeader
                title={event.attributes.title}
                description={event.attributes.startDate}
                sectionName={'Event'}
            >
                <Link
                    href={event.attributes.registrationUrl}
                    className={
                        'mt-4 inline-flex items-center rounded-md border border-transparent bg-blue-500 px-6 py-2 text-xl font-medium text-white shadow-sm hover:bg-blue-600 focus:outline-none'
                    }
                >
                    {event.attributes.registrationButtonText}
                </Link>
            </SectionHeader>

            <div className="mx-auto mt-3 flex flex-col lg:max-w-7xl lg:flex-row">
                <div className="lg:w-2/3">
                    <EventContent event={event} />
                </div>
                <div className="lg:w-1/3">
                    <EventInfo event={event} />
                </div>
            </div>
            <CallToAction />
            <Footer />
        </>
    );
};

export const getStaticPaths = async ({ locales }: { locales: string[] }) => {
    const fetchEvents = async (locale: string) => {
        return await fetchAPI<Event[]>('/events', {
            fields: ['slug'],
            locale: locale,
        });
    };
    const paths = await Promise.all(
        locales.map(async (locale) =>
            fetchEvents(locale).then((pages) =>
                pages.data.map((page) => ({
                    params: { slug: page.attributes.slug },
                    locale,
                }))
            )
        )
    ).then((values) => values.flat());

    return {
        paths: paths,
        fallback: false,
    };
};

export const getStaticProps: GetStaticProps = async ({ params, locale }) => {
    const translation = await loadTranslation(locale!, process.env.NODE_ENV === 'production');
    const pagesRes = await fetchAPI<Event[]>('/events', {
        locale: locale,
        filters: {
            slug: params?.slug,
        },
        populate: {
            hosts: {
                populate: {
                    fields: ['*'],
                    photo: '*',
                },
            },
        },
    });

    return {
        props: { event: pagesRes.data[0], translation },
        revalidate: 60,
    };
};

export default EventPage;
