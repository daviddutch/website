import { Header } from '@/components/shared/Header';
import { CallToAction } from '@/components/cta/CallToAction';
import { Footer } from '@/components/shared/Footer';
import { fetchAPI } from '@/utils/api';
import { GetStaticProps } from 'next';
import { loadTranslation } from '@/utils/i18n';
import { EventSection } from '@/components/events/EventSection';
import { Event } from '@/types/Event';
import { fetchPage } from '@/services/page';

type Props = {
    events: Event[];
};

const Index = ({ events }: Props) => {
    return (
        <>
            <Header />
            <EventSection events={events} />
            <CallToAction />
            <Footer />
        </>
    );
};

export const getStaticProps: GetStaticProps = async ({ locale }) => {
    const translation = await loadTranslation(locale!, process.env.NODE_ENV === 'production');
    // Run API calls in parallel
    const [page, eventsRes] = await Promise.all([
        fetchPage('events', locale),
        fetchAPI('/events', { locale: locale, populate: ['image', 'hosts'] }),
    ]);

    return {
        props: {
            page,
            events: eventsRes.data,
            translation,
        },
        revalidate: 60,
    };
};

export default Index;
