import { Header } from '@/components/shared/Header';
import { Footer } from '@/components/shared/Footer';
import { GetStaticProps } from 'next';
import { loadTranslation } from '@/utils/i18n';
import { LandingPage } from '@/types/LandingPage';
import { LandingPageContent } from '@/components/shared/LandingPageContent';
import { fetchLandingPage, fetchLandingPagePaths } from '@/services/landingPage';
import { HeroHeaderRounded } from '@/components/shared/HeroHeaderRounded';

const FeatureLandingPage = ({ page }: { page: LandingPage }) => {
    const content = page.attributes.content;
    const heroHeader = page.attributes.heroHeader;
    return (
        <>
            <Header />

            <HeroHeaderRounded title={heroHeader.title} text={heroHeader.text} image={heroHeader.image} />

            <LandingPageContent content={content} />

            <Footer />
        </>
    );
};

export async function getStaticPaths({ locales }: { locales: string[] }) {
    return {
        paths: await fetchLandingPagePaths('use-case', locales),
        fallback: false,
    };
}

export const getStaticProps: GetStaticProps = async ({ params, locale }) => {
    const translation = await loadTranslation(locale!, process.env.NODE_ENV === 'production');
    const page = await fetchLandingPage(params?.slug as string, locale!);

    return {
        props: { page: page, translation, locale },
        revalidate: 60,
    };
};

export default FeatureLandingPage;
