import { Header } from '@/components/shared/Header';
import { GetStaticProps, NextPage } from 'next';
import { loadTranslation } from '@/utils/i18n';
import { t } from '@lingui/macro';
import { Footer } from '@/components/shared/Footer';
import React from 'react';
import { fetchAPI } from '@/utils/api';
import { Messages } from '@lingui/core/esm/i18n';
import { PageHeader } from '@/components/shared/PageHeader';
import { CallToAction } from '@/components/cta/CallToAction';
import { OurValues } from '@/components/about/OurValues';
import { Team } from '@/components/about/Team';
import { fetchPage } from '@/services/page';
import { Page } from '@/types/Page';
import { Employee } from '@/types/Employee';
import { Outings } from '@/components/jobs/Outings';

export const getStaticProps: GetStaticProps = async ({ locale }) => {
    const translation = await loadTranslation(locale!, process.env.NODE_ENV === 'production');
    const [page, employeesRes] = await Promise.all([
        fetchPage('about', locale),
        fetchAPI('/employees', {
            sort: ['order', 'lastName'],
            populate: { photo: true },
            locale: locale,
        }),
    ]);

    return {
        props: {
            translation,
            page,
            people: employeesRes.data,
        },
    };
};

interface PageProps {
    translation: Messages[];
    page: Page;
    people: Employee[];
}

const AboutPage: NextPage<PageProps> = ({ page, people }) => {
    return (
        <>
            <Header />
            <PageHeader
                title={t`Meet the team that is on the mission to help you collaborate with ease`}
                description={t`Find out what the dream team at WEDO likes in addition to ping-pong, intuitive SaaS and excellent customer service here.`}
            />
            <OurValues />
            <Team people={people} />
            <Outings />
            <CallToAction />
            <Footer />
        </>
    );
};

export default AboutPage;
