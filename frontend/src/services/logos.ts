import { fetchAPI } from '@/utils/api';

type Filters = {
    showSecurity?: boolean;
    showPricing?: boolean;
};

export const fetchCustomerLogos = async (filters: Filters) =>
    await fetchAPI('/customer-logos', {
        filters,
        populate: {
            logoDark: '*',
            logoLight: '*',
            logoColor: '*',
        },
    });
