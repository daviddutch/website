import { fetchAPI } from '@/utils/api';
import { Page } from '@/types/Page';

export async function fetchPage(slug: string, locale: string | undefined) {
    let pageRes = await fetchAPI<Page[]>('/pages', {
        filters: {
            slug: slug,
        },
        locale: locale,
        populate: {
            seo: { populate: { shareImage: '*' } },
        },
    });
    return pageRes.data[0];
}
