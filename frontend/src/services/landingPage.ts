import { fetchAPI } from '@/utils/api';
import { LandingPage } from '@/types/LandingPage';

export async function fetchLandingPage(slug: string, locale: string | undefined) {
    const pagesRes = await fetchAPI<LandingPage[]>('/landing-pages', {
        filters: {
            slug: slug,
        },
        locale: locale,
        populate: {
            title: '*',
            seo: { populate: '*' },
            heroHeader: {
                populate: {
                    image: '*',
                    buttons: { populate: '*' },
                    highlights: { populate: '*' },
                },
            },
            content: {
                populate: {
                    title: '*',
                    image: '*',
                    highlights: { populate: '*' },
                    logos: { populate: '*' },
                    testimonials: {
                        populate: {
                            logo: { populate: ['logoDark', 'logoLight', 'logoColor'] },
                            photo: { populate: true },
                        },
                    },
                    articles: {
                        populate: {
                            image: true,
                            categories: { populate: true },
                            author: {
                                populate: ['photo'],
                            },
                        },
                    },
                },
            },
        },
    });
    return pagesRes.data[0];
}

export async function fetchLandingPagePaths(category: string, locales: string[]) {
    const fetchLandingPages = async (locale: string) => {
        return await fetchAPI<LandingPage[]>('/landing-pages', {
            fields: ['slug'],
            locale: locale,
            filters: {
                category: category,
            },
        });
    };
    return await Promise.all(
        locales.map(async (locale) =>
            fetchLandingPages(locale).then((pages) =>
                pages.data.map((page) => ({
                    params: { slug: page.attributes.slug },
                    locale,
                }))
            )
        )
    ).then((values) => values.flat());
}
