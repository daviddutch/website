import { fetchAPI } from '@/utils/api';
import { Post } from '@/types/Post';

export const countArticles = async (
    locale: string,
    cursor = 0,
    total = 0,
    filters: object | undefined = undefined
): Promise<number> => {
    const response = await fetchAPI<Post[]>('/articles', {
        fields: ['slug'],
        filters: filters,
        locale: locale,
        pagination: {
            start: cursor,
            limit: 100,
        },
    });
    if (response.data.length < 100) {
        return total + response.data.length;
    }
    return countArticles(locale, cursor + 100, total + response.data.length);
};
