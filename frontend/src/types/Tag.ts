import { SeoMeta } from '@/types/SeoMeta';

export type Tag = {
    id: string;
    attributes: {
        name: string;
        slug: string;
        locale: string;
        seo: SeoMeta;
        localizations: {
            data: Tag[];
        };
    };
};
