import { StrapiMedia } from '@/utils/media';
import { Employee } from '@/types/Employee';

export type Event = {
    id: string;
    attributes: {
        title: string;
        type: 'webinar' | 'tradeShow';
        startDate: string;
        slug: string;
        duration: string;
        location: string;
        registrationUrl: string;
        registrationButtonText: string;
        language: string;
        description: string;
        content: string;
        image: StrapiMedia;
        hosts: {
            data: Employee[];
        };
    };
};
