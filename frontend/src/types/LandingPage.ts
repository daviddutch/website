import { SeoMeta } from '@/types/SeoMeta';
import { StrapiMedia } from '@/utils/media';
import { Logo } from '@/types/Logo';
import { Icon } from '@/types/Icon';
import { Post } from '@/types/Post';

export enum CtaType {
    Main = 'main',
    Job = 'job',
    Calendly = 'calendly',
    CalendlyEmbed = 'calendlyEmbed',
    Security = 'security',
}
export enum LogosSectionType {
    Simple = 'simple',
    Grid = 'grid',
}

export type Highlight = {
    id: string;
    title: string;
    description: string;
    icon: Icon;
    image: StrapiMedia;
};
export type Testimonial = {
    id: string;
    attributes: {
        author: string;
        title: string;
        photo: StrapiMedia;
        company: string;
        logo: {
            data: Logo;
        };
        content: string;
    };
};
export type BlogSection = {
    id: string;
    title: string;
    description: string;
    articles: {
        data: Post[];
    };
};
export type CtaSection = {
    id: string;
    type: CtaType;
    calendlyUrl: string;
};
export type LogosSection = {
    title: string;
    type: LogosSectionType;
    logos: {
        data: Logo[];
    };
};
export type TestimonialsSection = {
    testimonials: {
        data: Testimonial[];
    };
};
export type GridSection = {
    title: string;
    description: string;
    highlights: {
        data: HighlightTemplate[];
    };
};
export type CardSection = {
    highlights: {
        data: HighlightTemplate[];
    };
};
export type SingleSection = {
    title: string;
    description: string;
    image: StrapiMedia;
};
export type LeftRightSection = {
    title: string;
    description: string;
    image: StrapiMedia;
    position: 'left' | 'right';
};
export type HighlightTemplate = {
    id: string;
    attributes: {
        title: string;
        description: string;
        icon: Icon;
        image: StrapiMedia;
    };
};

export enum SectionComponentType {
    Grid = 'shared.grid',
    Card = 'shared.card',
    LeftRight = 'shared.left-right',
    Single = 'shared.single',
    LogosSection = 'shared.logos',
    CtaSection = 'shared.shared-cta',
    TestimonialsSection = 'shared.shared-testimonials',
    BlogSection = 'shared.blog-section',
}

type BaseLandingPageSection = {
    id: string;
    __component: SectionComponentType;
    title: string;
};
export type LandingPageSection = BaseLandingPageSection &
    (
        | CtaSection
        | LogosSection
        | TestimonialsSection
        | BlogSection
        | GridSection
        | LeftRightSection
        | SingleSection
        | CardSection
    );

export type LandingPage = {
    id: string;
    attributes: {
        title: string;
        slug: string;
        category: 'feature' | 'use-case' | 'industry';
        seo: SeoMeta;
        heroHeader: {
            id: string;
            title: string;
            text: string;
            image: StrapiMedia;
        };
        content: LandingPageSection[];
    };
};
