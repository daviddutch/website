import { StrapiMedia } from '@/utils/media';

export type Employee = {
    id: string;
    attributes: {
        slug: string;
        firstName: string;
        lastName: string;
        title: string;
        photo: StrapiMedia;
        phone: string;
        email: string;
        languages: string;
        imageUrl: string;
        facebookUrl: string;
        instagramUrl: string;
        twitterUrl: string;
        linkedinUrl: string;
        githubUrl: string;

        calendlyGlobal: string;
        calendlyDemo: string;
        calendlyPhone: string;
    };
};
