import { SeoMeta } from '@/types/SeoMeta';

export type Category = {
    id: string;
    attributes: {
        name: string;
        slug: string;
        locale: string;
        description: string;
        seo: SeoMeta;
        localizations: {
            data: Category[];
        };
    };
};
