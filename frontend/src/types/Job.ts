export type Job = {
    id: string;
    attributes: {
        title: string;
        slug: string;
        applyUrl: string;
        applyButtonText: string;
        content: string;
        workload: string;
        location: string;
        languages: string;
        department: string;
    };
};
