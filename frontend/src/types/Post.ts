import { StrapiMedia } from '@/utils/media';
import { Category } from '@/types/Category';
import { Employee } from '@/types/Employee';
import { SeoMeta } from '@/types/SeoMeta';
import { Tag } from '@/types/Tag';

export type Post = {
    id: string;
    attributes: {
        title: string;
        description: string;
        content: string;
        slug: string;
        locale: string;
        image: StrapiMedia;
        publicationDate: string;
        categories: {
            data: Category[];
        };
        tags: {
            data: Tag[];
        };
        seo: SeoMeta;
        author: {
            data: Employee;
        };
        localizations: {
            data: Post[];
        };
    };
};
