export type Menu = {
    slug: string;
    name: string;
};
