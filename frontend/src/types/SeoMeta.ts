import { StrapiMedia } from '@/utils/media';

export type SeoMeta = {
    metaTitle: string;
    metaDescription: string;
    shareImage?: StrapiMedia;
    article: boolean;
};
