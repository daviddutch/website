import { StrapiMedia } from '@/utils/media';

export type Logo = {
    id: string;
    attributes: {
        name: string;
        logoColor: StrapiMedia;
        logoLight: StrapiMedia;
        logoDark: StrapiMedia;
        showSecurity: boolean;
    };
};
