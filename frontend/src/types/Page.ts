import { SeoMeta } from '@/types/SeoMeta';

export type Page = {
    id: string;
    attributes: {
        name: string;
        seo: SeoMeta;
    };
};
