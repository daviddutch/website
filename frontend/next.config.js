/** @type {import('next').NextConfig} */
const nextConfig = {
    reactStrictMode: true,
    swcMinify: true,
    i18n: {
        locales: ['en', 'fr', 'de'],
        defaultLocale: 'en',
    },
    images: {
        domains: ['localhost', 'wedo-assets.sos-ch-gva-2.exo.io'],
        dangerouslyAllowSVG: true,
    },
    async redirects() {
        return [
            {
                source: '/meetings',
                destination: '/features/meetings',
                permanent: true,
            },
            {
                source: '/tasks',
                destination: '/features/tasks',
                permanent: true,
            },
            {
                source: '/checklists',
                destination: '/features/checklists',
                permanent: true,
            },
            {
                source: '/project-management',
                destination: '/features/project-management',
                permanent: true,
            },
            {
                source: '/team-meetings',
                destination: '/use-cases/team-meetings',
                permanent: true,
            },
            {
                source: '/one-on-ones',
                destination: '/use-cases/one-on-ones',
                permanent: true,
            },
            {
                source: '/executive-committees',
                destination: '/use-cases/executive-committees',
                permanent: true,
            },
            {
                source: '/board-meetings',
                destination: '/use-cases/board-meetings',
                permanent: true,
            },
            {
                source: '/remote-teams',
                destination: '/use-cases/remote-teams',
                permanent: true,
            },
            {
                source: '/executive-assistant',
                destination: '/use-cases/executive-assistant',
                permanent: true,
            },
            {
                source: '/your-company',
                destination: '/industries/sme',
                permanent: true,
            },
            {
                source: '/banks-insurance',
                destination: '/industries/banks-insurance',
                permanent: true,
            },
            {
                source: '/municipal-administrations',
                destination: '/industries/municipal-administrations',
                permanent: true,
            },
            {
                source: '/nursing-homes',
                destination: '/industries/nursing-homes',
                permanent: true,
            },
            {
                source: '/foundations-and-associations',
                destination: '/industries/foundations-and-associations',
                permanent: true,
            },
            {
                source: '/schools-and-faculties',
                destination: '/industries/schools-and-faculties',
                permanent: true,
            },
        ];
    },
};

module.exports = nextConfig;
