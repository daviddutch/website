module.exports = {
    locales: ['en', 'fr', 'de'],
    sourceLocale: 'en',
    fallbackLocales: {
        default: 'en',
    },
    catalogs: [
        {
            path: 'src/translations/locales/{locale}/messages',
            include: ['src/pages', 'src/components', 'src/state'],
        },
    ],
    format: 'po',
    service: {
        name: 'TranslationIO',
        apiKey: 'f6c6d736f702419d9b52a1b7f37928c9',
    },
};
